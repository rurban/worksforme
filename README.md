# WorksForMe Project

## Synopsis

This project repository is intended to contain all my crazy stuff and, if
possible, share to someone that might be interested in any of these things.

There is no order, no documetation, nothing, it is just a bunch of stuff that
I develop and somehow have helped me, thus, I share here.

The codes/scripts or files that this repo contains may or may not have comments,
and I won't say sorry for that because I might not consider legibility sometimes.
What I consider important, I usually try to put comments and explain the flow.

If you need explanations about anything you may think to be useful, send me an
e-mail, I may or may not answer you.

## Platform

All codes here where used in:

- Ubuntu LTS versions

## Contributors

Cassiano Campes <cassianocampes@gmail.com>

## License

The underlying source code is licensed under the [MIT](LICENSE.md)
