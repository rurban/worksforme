#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

typedef struct container {
	uint64_t a;
	uint64_t b;
	uint64_t c;
	uint64_t d;
} container;

void print_structure(container *c)
{
	printf("container->a=%lu\n", c->a);
	printf("container->b=%lu\n", c->b);
	printf("container->c=%lu\n", c->c);
	printf("container->d=%lu\n", c->d);
}

void init_with_base(int base, container *c)
{
	c->a = base + 1;
	c->b = base + 2;
	c->c = base + 3;
	c->d = base + 4;
}

int main(int argc, char **argv)
{
	container *values = calloc(1, sizeof(*values));

	printf("sizeof(*values) = %lu\n", sizeof(*values));

	switch (argc) {
		container *new_values = calloc(1, sizeof(*new_values));

		case 1:
			init_with_base(10, values);
			print_structure(values);
			break;
		case 2:
			init_with_base(45, new_values);
			print_structure(new_values);
			break;
		default:
			printf("Default case\n");
	}
	return 0;
}
