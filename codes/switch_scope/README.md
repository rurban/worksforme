## Switch scope

Another day I got this problem, and I took some time until I discover it.

The switch statement will not allocate any memory in between cases, as
shown in [this StackOverflow question](https://stackoverflow.com/questions/53732341/using-switch-case-with-malloc).

Well, because I took time to discover this, it worth it to write about this
problem, so I can remember and I (probably) won't repeat this.
