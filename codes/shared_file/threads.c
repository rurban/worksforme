#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <string.h>

#define FILENAME "./final_file.md"
#define WRITESIZE 10
#define NUMTHREADS 8

struct thread_cb {
    int id;
    char wrt;
};

void* thread_func(void *args)
{
    int fd, bytes, row, i, ret;
    char buf[10];
    cpu_set_t mask;
    struct thread_cb *ptr = (struct thread_cb*) args;


    CPU_ZERO(&mask);
    CPU_SET(row = ptr->id, &mask);
    sched_setaffinity(0, sizeof(mask), &mask);

    fd = open(FILENAME, O_WRONLY);

    if (fd < 0) {
        printf("[%d] ERRROR opening file\n", ptr->id);
        printf("err %s\n", strerror(errno));
        return NULL;
    }
    printf("!!!![%d] SUCCESS opening file\n", ptr->id);
    lseek(fd, row * WRITESIZE, SEEK_SET);
    for (i = 0; i < WRITESIZE; i++) {
        ret = write(fd, &ptr->wrt, 1);
        if (ret != 1) {
            printf("Error while writing [%d]\n", ptr->id);
            close(fd);
            return NULL;
        }
    }
    close(fd);
    return NULL;
}

int main(int argc, char **argv)
{
    struct thread_cb params[NUMTHREADS];
    int i;
    pthread_t threads[NUMTHREADS];

    for (i = 0; i < NUMTHREADS; i++) {
        params[i].id = i;
        params[i].wrt = 'a' + i;
        pthread_create(&threads[i], NULL, thread_func, &params[i]);
    }
    for (i = 0; i < 1; i++) {
        pthread_join(threads[i], (void*) NULL);
    }

    return 0;
}
