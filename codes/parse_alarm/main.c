#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
	uint8_t *ptr, *anchor;
	char buf[100] = { 0 };
	char *str;

	if (argc != 2)
		return -1;

	str = argv[1];

	ptr = (uint8_t*) strstr(str, "Active Alarm Count: ");
	ptr += strlen("Active Alarm Count: ");

	uint32_t val = strtoul (ptr, NULL, 10);

	printf ("val = %d\n", val);

	if (ptr == NULL)
		return -1;

	anchor = strstr (ptr, "\\n");
	*anchor++ = '\0';

	strncpy(buf, ptr, 100);

	ptr = ++anchor;

	printf("buf = %s\n", buf);

	return 0;
}
