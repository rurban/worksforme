#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main(int argc, char **argv)
{
	uint32_t x = 0x00112233;
	printf ("%d\n", (uint32_t) (((uint8_t*)&x)[0]));
	printf ("%d\n", (uint32_t) (((uint8_t*)&x)[1]));
	printf ("%d\n", (uint32_t) (((uint8_t*)&x)[2]));
	printf ("%d\n", (uint32_t) (((uint8_t*)&x)[3]));
	return 0;
}
