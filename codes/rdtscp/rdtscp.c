#define _GNU_SOURCE

//XXX: REFERENCE https://stackoverflow.com/questions/10921210/cpu-tsc-fetch-operation-especially-in-multicore-multi-processor-environment

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <stdint.h>

#include <sched.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <math.h>

#define ERR_EXIT(msg) \
    do { \
        printf("%s\n", msg); \
        exit(-1); \
    } while (0)

typedef struct thread_args {
    int cpu_id;
    int chip, core;
    int thread_id;
    unsigned long *ptr;
} thread_args_t;

unsigned long tacc_rdtscp(int *chip, int *core)
{
    unsigned a, d, c;

    /*
     * RDTSCP => Read 64-bit time-stamp counter and
     *           IA32_TSC_AU value into EDX:EAX and ECX
     * */
    __asm__ volatile("rdtscp" : "=a" (a), "=d" (d), "=c" (c));

    *chip = (c & 0xFFF000)>>12;
    *core = c & 0xFFF;

    return ((unsigned long)a) | (((unsigned long)d) << 32);
}

void *thread_func(void *arg)
{
    cpu_set_t set;

    unsigned long counter = 0;
    int chip = 0, core = 0;
    thread_args_t *ta = arg;
    int cpu = ta->cpu_id;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);

    if(sched_setaffinity(0, sizeof(set), &set) == -1) {
        ERR_EXIT("sched_setaffinity");
    }

    *ta->ptr = tacc_rdtscp(&ta->chip, &ta->core);
    //printf("%d, %d,\t%lu\n", ta->chip, ta->core, *ta->ptr);
}

void sort(uint64_t *number, int size, uint32_t cache_alignment);

int main() {
    static const uint32_t cache_alignment = 64;

    uint64_t counter[100 * cache_alignment];
    int i = 0;

    unsigned long diff[100];
    unsigned long acc = 0;
    unsigned long min = ~0, max = 0;
    int min_pos = 0, max_pos = 0;
    unsigned long sd = 0, var = 0;

#if 1
    pthread_t *threads;
    thread_args_t *thread_args;

    long num_cores = sysconf(_SC_NPROCESSORS_ONLN);

    threads = (pthread_t*) calloc(num_cores, sizeof(pthread_t));
    thread_args = (thread_args_t*) calloc(num_cores, sizeof(thread_args_t));

    for (i = 0; i < num_cores; i++) {
        thread_args[i].cpu_id = i;
        thread_args[i].thread_id = i;
        thread_args[i].ptr = &counter[i * cache_alignment];
        pthread_create(&threads[i], NULL, thread_func, &thread_args[i]);
    }

    for(i = 0; i < num_cores; i++) {
        pthread_join(threads[i], NULL);
    }

    free(threads);
    free(thread_args);

    return 0;
    for(i = 0; i < num_cores; i++) {
        acc += counter[i * cache_alignment];
        if (counter[i * cache_alignment] > max) {
            max = counter[i * cache_alignment];
            max_pos = i;
        }
        if (counter[i * cache_alignment] < min) {
            min = counter[i * cache_alignment];
            min_pos = i;
        }
        //printf("[%d], %d, %d,\t%lu\n", i, thread_args[i].chip, thread_args[i].core, counter[i]);
        printf("%lu\n", counter[i * cache_alignment]);
    }
    acc = acc / num_cores;

    printf("Average\t%lu\n", acc);
    printf("Min\t[%d]%lu\n", min_pos, min);
    printf("Max\t[%d]%lu\n", max_pos, max);
    printf("Diff min-max\t%lu\n", (max-min));


    for(i = 0; i < num_cores; i++) {
        sd = sd + abs(counter[i * cache_alignment] - acc)*abs(counter[i * cache_alignment] - acc);
    }
    sd = sd / num_cores;
    var = sd / (num_cores -1);
    sd = sqrt(sd);

    printf("Standard deviation\t%lu\n", sd);
    printf("Variance\t%lu\n", var);

    sort(counter, num_cores, cache_alignment);
    printf("\n\n\n");
    for (i = 0; i < num_cores; i++) {
        printf("%lu\n", counter[i * cache_alignment]);
    }

    for(i = 0; i < num_cores - 1; i++) {
        diff[i] = abs(counter[i * cache_alignment] - counter[i * cache_alignment + 1]);
        printf("%lu\n", diff[i]);
    }


#else
    for(int i = 0; i < 100; i++) {
        counter[i] = tacc_rdtscp(&chip, &core);
    }
    for(int i = 0; i < 100; i++) {
        printf("%lu\n", counter[i]);
    }

    for(int i = 0; i < 99; i++) {
       diff[i] = abs(counter[i] - counter[i+1]);
       printf("%lu\n", diff[i]);
    }
#endif
    return 0;
}


void sort(uint64_t *number, int size, uint32_t cache_alignment)
{
    uint64_t  i, j, a;

    for(i = 0; i < size; ++i) {
        for(j = i + 1; j < size; ++j) {
            if (number[i * cache_alignment] > number[j * cache_alignment]) {
                a = number[i * cache_alignment];
                number[i * cache_alignment] = number[j * cache_alignment];
                number[j * cache_alignment] = a;
            }
        }
    }
}
