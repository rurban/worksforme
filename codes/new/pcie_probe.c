#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Cassiano Campes");

static int __init pcie_init(void)
{

	int bus, dev;

	printk(KERN_INFO "Hello world[%s]\n", __func__);

	for (bus = 0; bus < 256; bus++) {
		for (dev = 0; dev < 32; dev++) {
			unsigned int recvp, sendp = 0xf8000000;//(bus << 16) | (dev << 11) | ((unsigned int)0x80000000);
			outl(sendp, 0x0CF8);
			recvp = inl(0x0CFC);

			if (recvp != 0xFFFFFFFF)
				printk(KERN_ERR "%d/%d/%d: vendor=%d:device=%d\n", bus, dev, 0, ((0xFFFF0000 & recvp) >> 16),  0xFFFF & recvp);
		}
	}

	return 0;
}

static void __exit pcie_exit(void) {
	printk(KERN_INFO "Goodbye world[%s]\n", __func__);
}

module_init(pcie_init);
module_exit(pcie_exit);
