

class Graph:
    def __init__(self, row, col, g):
        self.ROW = row
        self.COL = col
        self.graph = g

    def isSafe(self, row, col, visited):
        return (row >= 0 and row < self.ROW and \
            col >= 0 and col < self.COL and \
            not visited[row][col] and self.graph[row][col])

    def DFS(self, row, col, visited):
        rowNR = [-1,-1-1,0,0,1,1,1]
        colNR = [-1,0,1,-1,0,1,-1,0,1]

        visited[row][col] = True

        for k in range(0, len(rowNR)):
            if self.isSafe(row + rowNR[k], col + colNR[k], visited):
                self.DFS(row + rowNR[k], col + colNR[k], visited)

    def countIslands(self):
        count = 0
        visited = [[False for j in range(self.COL)]for i in range(self.ROW)] 
        for row in range(0, self.ROW):
            for col in range(0, self.COL):
                if not visited[row][col] and self.graph[row][col]:
                    self.DFS(row, col, visited)
                    count += 1
        return count


if __name__ == "__main__":
    graph = [[1, 1, 0, 0, 0], 
        [0, 1, 0, 0, 1], 
        [1, 0, 0, 1, 1], 
        [0, 0, 0, 0, 0], 
        [1, 0, 1, 0, 1]] 
  
  
    row = len(graph) 
    col = len(graph[0]) 
  
    g= Graph(row, col, graph) 
    print(g.countIslands())

