
def runFizzBuzz(n, *args):
    multiples = {3:'fizz',
                 5:'fuzz'}

    for i in range (*args):
        output = ''
        for multiple in multiples:
            if not i % multiple:
                output += multiples[multiple] 
        if output == '':
            output = str(i)
        print(output)
n = 100
if __name__ == "__main__":
    runFizzBuzz(n, 1,101)