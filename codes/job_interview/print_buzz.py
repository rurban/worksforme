n1 = 3
n2 = 5

def printBuzzEnhanced(multiples, *args):
    for i in range(*args):
        output = ''
        for multiple in multiples:
            if not i % multiple:
                output+= multiples[multiple]
            if output == '':
                output = str(i)
        print(output)

def printBuzz(n):
    for i in range (0, n+1):
        if not i % n1 and not i % n2:
            print("FizzBuzz")
        elif not i % n1:
            print("Fizz")
        elif not i % n2:
            print("Buzz")
        else:
            print(i)
n = 100
multiples = {3:'Fizz', 5:'Buzz'}
if __name__ == "__main__":
    print(printBuzzEnhanced(multiples, 1,101))