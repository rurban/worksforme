class LinkedList():
    def __init__(self):
        self.head = None

    def add(self, data):
        if not self.head:
            self.head = Node(data)
        node = self.head
        while(node.next):
            node = node.next
        node.next = Node(data)

    def midpoint(self):
        slow = self.head
        fast = slow

        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
        return slow

class Node:
    def __init__(self, data = None):
        self.data = data
        self.next = None

if __name__ == "__main__":
    lis = LinkedList()
    lis.add(1)
    lis.add(2)
    lis.add(3)
    lis.add(4)
    lis.add(5)
    lis.add(6)
    lis.add(7)
    lis.add(8)
    lis.add(9)
    lis.add(9)

    print(lis.midpoint().data)