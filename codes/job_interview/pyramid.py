def printPyramidIteract(n):
  anchor = n-1
  for row in range(0, n):
    for col in range(0, 2*n):
      if col >= anchor-row and col <= anchor+row:
        print('#', end='')
      else:
        print(' ', end='')
    print()

n = 4
if __name__ == "__main__":
    printPyramidIteract(n)