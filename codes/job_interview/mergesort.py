import math

def mergeSort(array):
  if len(array) == 1:
    return array

  center = math.floor(len(array)/2)
  left = array[:center]
  right = array[center:]

  return merge(mergeSort(left), mergeSort(right))



def merge(left, right):
  results = []
  while len(left) and len(right):
    if left[0] < right[0]:
      results.append(left[0])
      del left[0]
    else:
      results.append(right[0])
      del right[0]

  for i in range (0, len(left)):
    results.append(left[i])
  for i in range (0, len(right)):
    results.append(right[i])

  return results
arr1 = [1,5,6, 7,8]
arr2 = [-1,5,2,9,11,6]
if __name__ == "__main__":
  #res = merge(arr1, arr2)
  #print(res)

  print(mergeSort(arr2))
