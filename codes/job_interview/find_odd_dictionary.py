elements = {1, 2, 3, 4, 5, 6, 6, 5, 4, 3}

def findOdd(elements):
    oddList = {}
    for el in elements:
        if oddList[el]:
            oddList.popitem(el)
        else:
            oddList[el] = True
    return oddList

if __name__ == "__main__":
    oddList = findOdd(elements)

    for i in oddList:
        print(i)
