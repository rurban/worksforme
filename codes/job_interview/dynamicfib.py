



def fib(n, memo={}):
  if n in memo:
    return memo[n]
  if n == 1 or n ==2:
    return 1
  else:
    result = fib(n-1) + fib(n-2)
  memo[n] = result
  return result

if __name__ == "__main__":
    print(fib(1000))