# Create a Queue from 2 Stack

class Stack:
    def __init__(self, max_size=10):
        self.data = []
        self.max_size = max_size
        self.size = 0

    def push(self, val):
        if self.max_size == self.size:
            return
        self.size += 1
        self.data.append(val)

    def pop(self):
        if self.size == 0:
            return None
        self.size-= 1
        return self.data[self.size]

    def peek(self):
        if self.size == 0:
            return None
        return self.data[-1]

class Queue:
    def __init__(self, max_size=10):
        self.enqueue = Stack(max_size=max_size)
        self.dequeue = Stack(max_size=max_size)

    def put(self, val):
        self.enqueue.push(val)

    def get(self):
        while self.enqueue.peek():
            self.dequeue.push(self.enqueue.pop())
        ret = self.dequeue.pop()
        while self.dequeue.peek():
            self.enqueue.push(self.dequeue.pop())
        return ret
    def peek(self):
        while self.enqueue.peek():
            self.dequeue.push(self.enqueue.pop())
        peek = self.dequeue.peek()
        while self.dequeue.peek():
            self.enqueue.push(self.dequeue.pop())
        return peek

if __name__ == "__main__":
    q = Queue()
    q.put(10)
    q.put(12)
    q.put(14)
    while q.peek():
        print(q.get())
    print("@")
    q = Stack()
    q.push(10)
    q.push(12)
    q.push(14)
    while q.peek():
        print(q.pop())
