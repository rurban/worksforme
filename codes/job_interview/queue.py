
class Queue:
    def __init__(self, max_size=10):
        self.max_size = max_size
        self.size = 0
        self.data = []

    def enqueue(self, val):
        self.data = [val] + self.data
        self.size += 1

    def dequeue(self):
        if self.size:
            self.size -= 1
            return self.data.pop()

    def peek(self):
        if not self.data:
            return None
        return self.data[0]

    def empty(self):
        return self.size == 0

def weave(q1, q2):
    q3 = Queue()
    while not q1.empty() or not q2.empty():
        if not q1.empty():
            q3.enqueue(q1.dequeue())
        if not q2.empty():
            q3.enqueue(q2.dequeue())
    return q3

if __name__ == "__main__":
    q = Queue()
    q.enqueue(1)
    q.enqueue(2)

    r = Queue()
    r.enqueue(3)
    r.enqueue(4)

    a = weave(q, r)
    
    while not a.empty():
        print(a.dequeue())

    


