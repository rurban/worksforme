
elements = [1,2,3,4,5,6,7,8,9,10,11]

def reverseList(elements):
    last = len(elements)-1
    first = 0

    if last == first:
        return

    while first < last:
        tmp = elements[first]
        elements[first] = elements[last]
        elements[last] = tmp
        first += 1
        last -= 1
    return elements

if __name__ == "__main__":
    res = reverseList(elements)
    print(res)
    