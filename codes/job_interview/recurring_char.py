def RecurringChar(input):
    j = 1
    visited = {}
    for i in range(len(input)):
        if input[i] in visited:
            return input[i]
        visited[input[i]] = True
        
input = ['a','b','c','d','e','e'] 
if __name__ == "__main__":
    print(RecurringChar(input))
