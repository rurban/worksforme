import re

def __cleanupSpaces(arrayA, arrayB):
    arrayA = re.sub('[^a-zA-Z]', '', arrayA).lower()
    arrayB = re.sub('[^a-zA-Z]', '', arrayB).lower()
    return arrayA, arrayB

def checkAnagramEnhanced(arrayA, arrayB):
    arrayA, arrayB = __cleanupSpaces(arrayA, arrayB)

    if len(arrayA) is not len(arrayB):
        return False
    arrayA = sorted(arrayA)
    arrayB = sorted(arrayB)

    for i in range(0, len(arrayA)):
        if arrayA[i] != arrayB[i]:
            return False
    return True


def checkAnagram(arrayA, arrayB):
    arrayA, arrayB = __cleanupSpaces(arrayA, arrayB)

    if len(arrayA) is not len(arrayB):
        return False

    freqA = {}
    freqB = {}

    for i in range(0, len(arrayA)):
        freqA[arrayA[i]] = 1 if i not in freqA else freqA[i] + 1
        freqB[arrayB[i]] = 1 if i not in freqB else freqB[i] + 1
    
    return True if freqA == freqB else False

arrayA = "Ca123s"
arrayB = "cas"

if __name__ == "__main__":
    print(checkAnagram(arrayA, arrayB))
    print(checkAnagramEnhanced(arrayA, arrayB))

