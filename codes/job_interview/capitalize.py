import string as String

def capitalize(string):

    splitted = [word[0].upper() + word[1:] for word in string.split()]
    
    return " ".join(splitted)
    
string = "hello world!"
if __name__ == "__main__":
    print(capitalize(string))