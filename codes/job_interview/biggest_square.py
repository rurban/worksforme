
def findBiggestSquare(array, cache):
    row = 0
    col = 0
    result = 0
    for row in range(len(array)):
        for col in range(len(array[0])):
            if row == 0 or col == 0:
                continue
            elif array[row][col]:
                cache[row][col] = 1 + min(cache[row][col-1],
                                           cache[row-1][col],
                                           cache[row-1][col-1])
            if cache[row][col] > result:
                result = cache[row][col]
    return result

array = [[1,1,0,0,0],
         [1,0,0,1,0],
         [1,0,1,1,0],
         [1,1,1,0,1]]
cache = array

if __name__ == "__main__":
    res = findBiggestSquare(array, cache)
    print(res)