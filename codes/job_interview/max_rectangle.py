import queue as Queue

def peekStack(stack):
    if stack.empty():
        return None
    peek = stack.get()
    stack.put(peek)
    return peek

def maxRectangle(histogram):
    stack = Queue.LifoQueue()
    maxArea = 0
    i = 0

    while i < len(histogram):
        if stack.empty() or histogram[peekStack(stack)] <= histogram[i]:
            stack.put(i)
            i += 1
        else:
            curMax = stack.get()
            area = histogram[curMax] * (i - 1) if stack.empty() else (i - 1 - peekStack(stack))
            if area > maxArea:
                maxArea = area
    while peekStack(stack):
        curMax = stack.get()
        area = histogram[curMax] * (i - 1) if stack.empty() else (i -1 - peekStack(stack))
        if area >= maxArea:
            maxArea = area
    return maxArea

histogram = [1,1,1]

if __name__ == "__main__":
    print(maxRectangle(histogram))