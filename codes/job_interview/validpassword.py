import re

def validatePassword(password, required=3):
    if password is None or len(password) < required:
        return False
    if re.search('[0-9]', password) is None:
        print('1')
        return False
    if re.search('[A-Z]', password) is None:
        print('2')
        return False
    if re.search('[!@#$%^&+=]', password) is None:
        print('3')
        return False
    else:
        return True

if __name__ == "__main__":
    password = input("Enter password: ")
    print(validatePassword(password))