elements = [1,2,6,7,9]


def findList(elements):
    longest = count = cur = 0
    new_set = {}

    for i in elements:
        new_set[i] = True

    for k in new_set:
        if k-1 not in new_set:
            cur = k
            count = 1
            while (cur+1) in new_set:
                count +=1
                cur +=1
            longest = max(longest, count)
    return longest

if __name__ == "__main__":
    res = findList(elements)
    print(res)
    