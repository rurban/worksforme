import queue

class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None
        self.level = None

class BST:
    def __init__(self):
        self.root = None
        self.queue = queue.Queue(maxsize=10)

    def __setRoot(self, val):
        self.root = Node(val)

    def insert(self, val):
        if self.root is None:
            self.__setRoot(val)
        else:
            self.__insertNode(self.root, val)
    def find(self, val):
        if self.root is None:
            return False
        return self.__findNode(self.root, val)

    def __insertNode(self, node, val):
        if val == node.val:
            return
        if val < node.val:
            if node.left is not None:
                self.__insertNode(node.left, val)
            else:
                node.left = Node(val)
        else:
            if node.right:
                self.__insertNode(node.right, val)
            else:
                node.right = Node(val)

    def __findNode(self, node, val):
        if node is None:
            return False
        elif val == node.val:
            return True
        elif val < node.val:
            return self.__findNode(node.left, val)
        else:
            return self.__findNode(node.right, val)

    def print(self):
        if self.root is None:
            print("No nodes!")
        else:
            print("------------")
            self.root.level = 0
            self.queue.put(self.root)
            self.__recursePrint(0)

    def __recursePrint(self, curLevel):
        if self.queue.empty():
            return
        node = self.queue.get()

        if curLevel < node.level:
            curLevel += 1
            print()
        print(str(node.val) + " ", end='')

        if node.left is not None:
            node.left.level = curLevel + 1
            self.queue.put(node.left)

        if node.right is not None:
            node.right.level = curLevel + 1
            self.queue.put(node.right)

        if not self.queue.empty():
            return self.__recursePrint(curLevel)

values_to_add = [30,15,10,18,45,35,33,37,36,11,50,55,60]

def run():
    bst = BST()
    for i in values_to_add:
        bst.insert(i)

    a = bst.find(4)
    print(a)
    a = bst.find(15)
    print(a)

    bst.print()

if __name__ == "__main__":
    run()