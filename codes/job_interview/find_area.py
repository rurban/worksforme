A = {'bX':1,'bY':2,'tX':6,'tY':6}
B = {'bX':2,'bY':1,'tX':4,'tY':5}
C = {'bX':5,'bY':1,'tX':6,'tY':5}
D = {'bX':-2,'bY':-2,'tX':0,'tY':0}
E = {'bX':-4,'bY':-4,'tX':0,'tY':0}
F = {'bX':-4,'bY':-2,'tX':0,'tY':-2}

def boxIntersect(A,B):
    posXb = max(A['bX'], B['bX'])
    posXt = min(A['tX'], B['tX'])
    
    valX = posXt - posXb
    if valX <= 0:
        return {'area':0}
    
    posYb = max(A['bY'], B['bY'])
    posYt = min(A['tY'], B['tY'])

    valY = posYt - posYb
    if valY <= 0:
        return {'area':0}

    print(valX * valY)
    return {'area':valX * valY}

if __name__ == "__main__":
    x = lambda a: a['area'] if a['area']  else False
    answer = boxIntersect(A,B)
    print(x(answer))
    answer = boxIntersect(A,C)
    print(x(answer))
    answer = boxIntersect(D,E)
    print(x(answer))
    answer = boxIntersect(E,F)
    print(x(answer))
    answer = boxIntersect(F, A)
    print(x(answer))