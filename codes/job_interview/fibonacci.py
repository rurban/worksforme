
def fibonacci(n):
  if n < 2:
    return n
  return fibonacci(n -1) + fibonacci(n -2)

def fibonacciCache(cache, n):
  if n < 2:
    return n
  if n not in cache:
    cache[n] = n
  else:
    return cache[n]
  return fibonacciCache(cache, n - 1) + fibonacciCache(cache, n - 2)

n = 40

if __name__ == "__main__":
  #print(fibonacci(n))
  cache = {}
  print(fibonacciCache(cache, n))