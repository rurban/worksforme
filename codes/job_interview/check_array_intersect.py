a = [1,2,3,5,7]
b = [1,2,3,5,7]
c = [1,4,7,8,9,10,11]

def OOB(x,y,z,a,b,c):
    if x >= len(a) or y >= len(b) or z >= len(c):
        return True
    return False

def findIntersect(a, b, c):
    result = []
    x = y = z = 0
    while not OOB(x,y,z,a,b,c):
        if a[x] == b[y] and b[y] == c[z]:
            result.append(a[x])
            x += 1
            y += 1
            z += 1
        elif (a[x] < b[y]):
            x += 1
        elif (b[y] < c[z]):
            y += 1
        else:
            z += 1
    return result

if __name__ == "__main__":
    print (findIntersect(a, b, c))