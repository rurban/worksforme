
def findFrequencies(elements):
    frequencies = {}
    for i in elements:
        if i in frequencies:
            frequencies[i] += 1
        else:
            frequencies[i] = 1
    return frequencies

def findKMF(elements, k):
    bucket = dict()
    frequencies = findFrequencies(elements)
    for i in frequencies:
        if frequencies[i] not in bucket:
            bucket[i] = [ i ]
        else:
            bucket[frequencies[i]].append(i)
    result = []

    for i in reversed(range(len(frequencies)+1)):
        if i in bucket:
            for j in bucket[i]:
                if k:
                    k -= 1
                    result.append(j)
                else:
                    break

    return result

elements = [1,2,3,3,4,4,4,5,5,5,6,6,6]
k = 64

if __name__ == "__main__":
    kel = findKMF(elements, k)
    print(kel)