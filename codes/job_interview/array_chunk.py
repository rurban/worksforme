
def breakChunkEnhanced(elements, n):
    if n >= len(elements):
        return elements
    answer = []

    for i in range(0, len(elements), n):
        answer.append(elements[i:i+n])
    return answer
    


def breakChunk(elements, n):
    if n >= len(elements):
        return elements
    chunk = []
    answer = []
    for i in range(0, len(elements)):
        chunk.append(elements[i])
        if len(chunk) >= n:
            answer.append(chunk)
            chunk = []
    if len(chunk):
        answer.append(chunk) 
    return answer
            

elements = [1,2,3,4,5,6,7,8,9,10]
n = 4

if __name__ == "__main__":
    print(elements)
    print(breakChunk(elements, n))
    print(breakChunkEnhanced(elements, n))


