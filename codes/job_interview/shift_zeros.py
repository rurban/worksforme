
def fnz(elements, pos):
    for i in range(pos, len(elements)):
        if elements[i] == 0:
            continue
        return i
    return None

def shiftZeroes(elements):
    for i in range(0, len(elements)-1):
        if elements[i] == 0:
            next = fnz(elements, i)
            if next is None:
                break
            elements[i] = elements[next]
            elements[next] = 0

    return elements

elements = [0, 0, 3, 4, 0, 5, 6]
if __name__ == "__main__":
    print(elements)
    print(shiftZeroes(elements))