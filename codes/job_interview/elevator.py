import queue

def calculateFloor(persons_weight, persons_floors, max_weight=200, max_persons=2, num_floors=5):
    elevator = {}

    cur_weight = 0

    person = 0
    trigger_trip = False

    num_trips = 0

    while person < len(persons_weight):
        if persons_weight[person] > max_weight:
            person += 1
        elif persons_weight[person] + cur_weight <= max_weight and len(elevator.keys()) < max_persons:
            elevator[persons_floors[person]] = persons_floors[person]
            cur_weight += persons_weight[person]
            person += 1
        else:
            trigger_trip = True

        if trigger_trip:
            num_trips += len(elevator.keys())
            elevator.clear()
            cur_weight = 0

            trigger_trip = False

        continue
    return num_trips+len(elevator.keys())

persons_weight = [190,156,50,1900]
persons_floors = [3,1,1,1]

if __name__ == "__main__":
   
    print(calculateFloor(persons_weight, persons_floors))
