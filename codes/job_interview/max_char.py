def maxChar(input):
    frequency = {}
    for i in input:
        frequency[i] = 1 if i not in frequency else frequency[i] + 1
    key_max = None
    max = 0
    for i in frequency:
        if max < frequency[i]:
            key_max = i
            max = frequency[i]
    print(key_max)

input = ['a','b','c','d','c']

if __name__ == "__main__":
    print(maxChar(input))