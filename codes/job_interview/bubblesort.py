def bubbleSort(array):
  for i in range(0, len(array)):
    for  j in range (0, len(array) -i-1):
      if array[j] > array[j+1]:
        array[j] ^= array[j+1]
        array[j+1] ^= array[j]
        array[j] ^= array[j+1]

array = [10,-30, 97, 0, 5]
if __name__ == "__main__":
  bubbleSort(array)
  print(array)