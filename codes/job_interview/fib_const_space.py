def fib(pos):
    if pos < 2:
        return pos
    
    last = 1
    secondlast = 0
    cur = 2

    while cur <= pos:
        temp = last
        last = last + secondlast
        secondlast = temp
        cur += 1

    return last

pos = 10

if __name__ == "__main__":
    ret = fib(pos)

    print(ret)