pos = 8

def f(arr, pos):
    if pos in arr:
        return arr[pos]
    if pos < 2:
        arr[pos] = pos
    else:
        arr[pos] = f(arr, pos-1) + f(arr, pos-2)
    return arr[pos]

def fib(pos):
    arr = {}
    return f(arr, pos)

if __name__ == "__main__":
    print(fib(pos))