import queue as Queue

def findParenthesis(elements):
    isOpen = { '(' : ')',
               '{' : '}',
               '[' : ']' }
    isClosed = { ')':'(',
                '}':'{', 
                ']':'[' }

    queue = Queue.LifoQueue(maxsize=20) 
    for i in elements:
        if i in isOpen:
            queue.put(i)
        elif i in isClosed:
            val = queue.get()
            if i != isOpen[val]:
                return False
    if queue.empty():
        return True
    return False

elements = ["{","(",")","}","(",")"]

if __name__ == "__main__":
    print(findParenthesis(elements))