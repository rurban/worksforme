import queue as Queue
class Node:
  def __init__(self, data):
    self.data = data
    self.left = None
    self.right = None

  def insert(self, data):
    if data < self.data and self.left:
      self.left.insert(data)
    elif data < self.data:
      self.left = Node(data)
    elif data > self.data and self.right:
      self.right.insert(data)
    elif data > (self.data):
      self.right = Node(data)

  def contains(data):
    if self.data == data:
      return self
    if self.data < data and self.right:
      return self.right.contains(data)
    elif self.data > data and self.left:
      return self.left.contains(data)
    return None

def Validade(node, min = None, max = None):
  if max not None and node.data > max:
    return False
  if min not None and node.data < min:
    return False
  
  if node.left and not Validate(node.left, min, max):
    return False
  if node.right and not Validate(node.right, min, max):
    return False

  return True



def BFS(node):
  if not node:
    return
  queue = Queue.Queue()
  queue.enqueue(node)

  while not queue.empty():
    deq = queue.dequeue()

  
def DFS(node):
  if not node:
    return
  
  stack = Queue.LifoQueue()

if __name__ == "__main__":
  n = Node(10)
  n.insert(15)
  n.insert(5)
  n.insert(6)
  n.insert(4)
  n.insert(20)