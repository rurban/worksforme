elements = [1,2,3,3,3,1,5,5,5,5,5,5,5,5,5,5,5]
k = 16

def runKMFU(elements, k):
    frequency = {}
    for i in elements:
        if i not in frequency:
            frequency[i] = 1
        else:
            frequency[i] += 1
    
    bucket = dict()

    for i in frequency:
        if frequency[i] not in bucket:
            bucket[i] = [ i ]
        else:
            bucket[frequency[i]].append(i)

    result = []
    for i in reversed(range(len(elements)+1)):
        if i in bucket:
            for j in bucket[i]:
                if k:
                    k -= 1
                    result.append(j)
                else:
                    break
    return result

if __name__ == "__main__":
    values = runKMFU(elements, k)

    print (values)