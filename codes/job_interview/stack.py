class Stack:
    def __init__(self, max_size = 10):
        self.data = []
        self.max_size = 10
        self.size = 0

    def push(self, val):
        if self.size == self.max_size:
            return
        self.size += 1
        self.data.append(val)
        print(self.data)

    def pop(self):
        if self.size == 0:
            return None
        return self.data.pop()

    def peek(self):
        if self.size == 0:
            return None
        print(self.size)
        print(self.data)#[self.size-1])

    def empty(self):
        return self.size == 0

    def getsize(self):
        return self.size

if __name__ == "__main__":
    s = Stack()

    s.push(10)
    s.pop()

    s.peek()

    
