def visit(node, connections, visited):
    visited.append(node)
    print(node)

    for child in connections[node]:
        if child in visited:
            continue
        visit(child, connections, visited)

def DFS(vertices, connections):
    visited = []
    for node in vertices:
        if node not in visited:
            visit(node, connections, visited)

vertices = ['A','B','C','D','E','F']
connections = {'A':['B','C','G'],
               'B':['C','D'],
               'C':['B','D','A'],
               'D':['B','C','H'],
               'E':['F'],
               'F':['E'],
               'G':['A','H'],
               'H':['G','D']}

if __name__ == "__main__":
    DFS(vertices, connections)