

def findCycle(elements):
    p = q = 0

    while True:
        if p < 0 or p >= len(elements) or q < 0 or q >= len(elements):
            return False

        p = elements[p]
        if p == q:
            return True
        if (p < 0 or p >= len(elements)):
            return False

        p = elements[p]
        if p == q:
            return True

        q = elements[q]
        if p == q:
            return True
    return True

elements = [1,2,3,4]
if __name__ == "__main__":
    print(findCycle(elements))