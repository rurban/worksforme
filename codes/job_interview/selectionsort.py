
def selectionSort(array):
  indexOfMin = len(array)

  for i in range(0, len(array)):
    indexOfMin = i

    for j in range(i+1, len(array)):
      if array[j] < array[indexOfMin]:
        indexOfMin = j

    if indexOfMin != i:
      lesser = array[indexOfMin]
      array[indexOfMin] = array[i]
      array[i] = lesser


array = [10,-30, 97, 0, 5]

if __name__ == "__main__":
  selectionSort(array)
  print(array)