import numpy as np

def walkSpiral(n):
  startCol = 0
  startRow = 0
  endCol = n-1
  endRow = n -1
  counter = 1
  results = np.zeros((n,n))

  while startCol <= endCol and startRow <= endRow:
    # First row
    for i in range(startCol, endCol+1):
      results[startRow][i] = counter
      counter += 1
    startRow += 1
    # Last Column
    for i in range(startRow, endRow+1):
      results[i][endCol] = counter
      counter += 1
    endCol -= 1
    # Last Row
    for i in range(endCol, startCol-1,-1):
      results[endRow][i] = counter
      counter += 1
    endRow -= 1
    # First column
    for i in range(endRow, startRow-1,-1):
      results[i][startCol] = counter
      counter += 1
    startCol += 1
  return results

n = 4
if __name__ == "__main__":
    print(walkSpiral(n))