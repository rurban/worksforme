
elements = [9,9]

def add_one(elements):
    carry = 1
    for i in range(len(elements)-1, -1, -1):
        sum = elements[i] + carry
        carry = 1 if sum >= 10 else 0
        elements[i] = sum % 10
    if carry:
        elements = [ 0 ] * (len(elements) + 1)
        elements[0] = carry
    return  elements

if __name__ == "__main__":
    print(elements)
    print()
    print(add_one(elements))