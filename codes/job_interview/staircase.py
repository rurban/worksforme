#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the staircase function below.
def staircase(n):
    for row in range(0, n):
      for col in range(1, n+1):
        print('#',end='') if col > n-1-row else print(' ',end='')
      print()
    

if __name__ == '__main__':
    #n = int(input())
    n = 4
    staircase(n)
