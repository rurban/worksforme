import re

def countChars(input, map = ['a','e','i','o','u']):
  counter = 0
  for i in range(0, len(input)):
    if input[i].lower() in map:
      counter += 1
  return counter

def countCharsRegx(input, map = ['a','e','i','o','u']):
  regex = re.compile('[^'+''.join(map)+']')
  input = regex.sub('', input.lower())
  return len(input)

input = 'HelloO!'
if __name__ == "__main__":
    print(countChars(input))
    print(countCharsRegx(input))

