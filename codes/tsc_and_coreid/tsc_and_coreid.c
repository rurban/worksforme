#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linux/kernel.h>

uint64_t rdtscp(uint64_t *chip, uint64_t *core)
{
    uint32_t a, d, c;

    __asm__ volatile("rdtscp" : "=a" (a),"=d" (d), "=c" (c));
    *chip = (c & 0xFFF000) >> 12;
    *core = c & 0xFFF;

    return ((uint64_t) a) | (((uint64_t) d) << 32);
}

#define COMBINE_TSC_AND_COREID(tsc, core) ((uint64_t)(((tsc) << 8) | ((core) & 0xFF )))
#define GET_COREID_FROM_COMBTSC(cmbtsc) ((uint64_t) ((cmbtsc) & 0xFF))
#define GET_TSC_ONLY(tsc) ((uint64_t)(tsc) >> 8)

static uint64_t inline compare_greater_equal_and_swap(uint64_t *reg, uint64_t new)
{
    uint64_t old;
    do {
        old = *reg;

        if(old >= new)
            break;
    } while(!__sync_bool_compare_and_swap(&reg, old, new));
}

int main(int argc, char **argv)
{
    uint64_t core, chip, tsc, tsc_cmb;

    tsc = rdtscp(&chip, &core);
    printf("TSC clear = %lu\n", tsc);
    tsc_cmb = COMBINE_TSC_AND_COREID(tsc, core);
    printf("Core cmb  = %lu\n", (tsc_cmb ) >> 8);
    printf("tsccmb p  = %lu\n", tsc_cmb);
    printf("Core cle  = %lu\n", GET_COREID_FROM_COMBTSC(tsc_cmb));
    printf("Core cmb  = %lu\n", tsc_cmb & 0xFF);
    return 0;
}
