#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct str {
	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint32_t d;
} str;

int main(int argc, char **argv)
{
	str *a;
	int i, n = 5;
	uint64_t val = (10ULL << 32 | 10ULL );

	a = calloc(1, sizeof(*a)*n);

	if (a == NULL)
		return -1;

	for (i = 0; i < n; i++)
		*((uint64_t*)&a[i].b) = val;

	for (i = 0; i < n; i++) {
		printf("[%d] a=%d,b=%d,c=%d,d=%d\n", i,
		a[i].a, a[i].b, a[i].c, a[i].d);
	}

	return 0;
}
