# Redundant assignment after calloc

In this example, we show a common mistake after using calloc.

Calloc is in charge of erasing the allocated memory back to 0, so,
any new assignment after that back to 0 is useless.

Also, depending on the write size, we can use 64-bits assignment to
fill up two 32-bits assignement.

In this example, we reduced from 121 lines of assembly code down to
104 lines of code, this is about 14% improvement.
