/**
Source Tutorial: https://www.toptal.com/linux/separation-anxiety-isolating-your-system-with-linux-namespaces
*/

#define _GNU_SOURCE

#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

static char child_stack[1048576];

static void print_nodename()
{
    struct utsname utsname;
    uname(&utsname);
    printf("%s\n", utsname.nodename);
}

static int child_fn()
{
    printf("New UTS namespace nodename: ");
    print_nodename();
    printf("Changing nodename inside new UTS namespace\n");
    sethostname("GLaDOS", 6);

    printf("New UTS namespace nodename: ");
    print_nodename();

    printf("PID:%ld\n", (long)getpid());
    printf("Parent PID:%ld\n", (long)getppid());
    //system("ip link");
    return 0;
}

int main()
{
    printf("Original\n");
    //system("ip link");
    printf("\n\n");
    pid_t child_pid = clone(child_fn, child_stack+1048576, CLONE_NEWPID | SIGCHLD | CLONE_NEWNET | CLONE_NEWNS, NULL);
    if (child_pid < 0) {
        printf("ERR: %s\n", strerror(errno));

    }
    sleep(1);
    printf("Original UTS namespace nodename: ");
    print_nodename();

    printf("clone() = %ld\n", (long)child_pid);

    waitpid(child_pid, NULL, 0);
    return 0;
}
