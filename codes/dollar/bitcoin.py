import requests, json, notify2
from time import sleep

def fetch_bitcoin():
    URL = 'https://www.bitstamp.net/api/ticker/'
    try:
        r = requests.get(URL)
        priceFloat = float(json.loads(r.text)['last'])
        return priceFloat
    except requests.ConnectionError:
        print ("Error querying Bitstamp API")

def notify():
    icon_path = "~/Desktop/btc.png"

    notify2.init("Cryptocurrency rates notifier")

    n = notify2.Notification("Crypto", icon=icon_path)

    n.set_urgency(notify2.URGENCY_NORMAL)
    n.timeout = 1000
    n.show()
    sleep(5)
    n.close()


while True:
	#print("Bitstamp last price: $") + str(getBitcoinPrice()) + "/BTC"
	print(str(fetch_bitcoin()*3.81))
	notify()
	sleep(15)


# https://www.ccn.com/bitcoin-price/
