import requests
import time

def check_dollar():

    url = "http://br.investing.com/currencies/usd-brl"
    r = requests.get(url)

    fname = "cassiano"

    lines = []
    for line in r.content.splitlines():
        if "alertValuePrice" in line:
            lines = line
            break
    value = lines.split('=')[-1].split('"')[1].split(',')
    print time.strftime("%H:%M:%S", time.localtime()) + ", " + value[0] + "." + value[1]

def main():
    while 1:
        check_dollar()
        time.sleep(10)

if __name__ == "__main__":main()
