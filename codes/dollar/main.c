#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#define die(e) do { fprintf(stderr, "%s\n", e); exit(EXIT_FAILURE); } while (0);

int main(int argc, char **argv) {
  int link[2];
  pid_t pid;
  char foo[8] = { 0 };

  if (pipe(link)==-1)
    die("pipe");

  if ((pid = fork()) == -1)
    die("fork");

  if(pid == 0) {

    dup2 (link[1], STDOUT_FILENO);
    close(link[0]);
    close(link[1]);
    execl("./dollar_while.sh", "./dollar_while.sh", "-1", (char *)0);
    die("execl");

  } else {

    close(link[1]);
	while (1) {
		unsigned long int value;
		double v;
		char *end;
		char *tok;
		char virg = ',';
		int nbytes = read(link[0], foo, sizeof(foo));

		tok = &foo[0];

		for (tok = foo; tok != NULL; tok++) {
			printf("%c", *tok);
			if (*tok = ',') {
				*tok = '.';
				printf("hehehe\n");
				break;
			}
		}

		value = strtoul(foo, &end, 10);
		v = strtof(foo, NULL);
		printf("value = %lu\n", value);
		printf("valuef = %4.4f\n", v);
		printf("Output: %.*s", nbytes, foo);
	}
    wait(NULL);

  }
  return 0;
}
