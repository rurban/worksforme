#ifndef __NVME_PARSER_H__
#define __NVME_PARSER_H__

#pragma once

#include <stdint.h>

/* Helpers to print the hex value or not */
#define PRINT_HEX (1)
#define NOT_PRINT_HEX (0)
/*
 * Given the @buf containing the raw hex values of the NVMe command,
 * we use this function to parse and print the commands read from @buf.
 *
 * [Information]
 * 1. The output will be a human-readable view of the NVMe command.
 * [Paramenters]
 * @buf: buffer array that holds the 64 bytes of the NVMe command
 * @size: size of the NVMe command (which reflects the @buf size as well)
 * @rep : representation of the command PRINT_HEX, or NOT_PRINT_HEX
 * */
void parser_print_full_cmd(const uint8_t * const buf, const size_t size, int rep);
/*
 * It prints the given DWORD in hex format
 *
 * [Parameters]
 * @cmd: this is a cast from nvme_rw_command_t* to uint32_t*.
 *		 Basically, we print the given memory position as an integer
 * @dword: which DWORD do you want to print from the command? (0-63)
 */
extern void parser_print_cmd_dword(const uint8_t * const cmd, const int dword);

/*
 * Prints the full DWORD command attributes with all its fields in human-readable
 *
 * [Parameters]
 * @cmd: this is a cast nvme_rw_command_t* to uint8_t
 * @dword: dword to print the command
 */
extern void parser_print_cmd_dword_info(const uint8_t * const cmd, const int dword);

/*
 * Used to format the integer value to hex value and store in a buffer
 *
 * [Parameters]
 * @cmd: this is a cast from nvme_rw_command_t* to uint32_t.
 *		 Basically, we use the given memory position as an integer
 * @dword: the DWORD (0-15) to be formmated
 * @dst: destination of the formmatted string
 * @size: sise of @dst
 */
extern void parser_format_cmd_dword(uint32_t const * const cmd, const int dword,
								unsigned char const * const dst,
								const int size);
/*
 * This function prints the n-bits of the given a 8-bits variable.
 * It also prints some helper markers, such as the 'b' and new-line '\n'
 * at the end of the print.
 *
 * [Waring] This prints in little-endianness order!
 *
 * Example:
 * - printnbits(buf, 0, 7, 1, 1) would print this string:
 *   "01010010b\n"
 * - printnbits(buf, 0, 7, 0, 0) would print this string:
 *   "01010010"
 *
 * [Parameters]
 * @buf: the byte to be used to print
 * @start: start offset (between 0-7)
 * @end: end offset (between 0-7)
 * @add_b: add the 'b' char at the ending of the print?
 * @nl: add new-line at the end? */
void printnbits(const uint8_t * const buf, const size_t start, size_t end,
					const int add_b, const int nl);

/* Print a given binary buffer into its hexadecimal representation
 *
 * [Parameters]
 * @buf: buffer to be used to print the hex vals
 * @size: size of the @buf
 * @offset: prints the offset together with the hexdump
 *
 * [Return]
 * Returns the number of converted bytes */
int parser_dump2hex(const uint8_t *buf, size_t size, int put_offset);

#endif // __NVME_PARSER_H__
