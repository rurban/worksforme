#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "nvme_parser.h"
#include "nvme_commands.h"
#include "nvme_messages.h"

void parser_print_full_cmd(const uint8_t * const buf, const size_t size, int rep)
{
	int i;

	assert(size == 64);

	for (i = 0; i < size / sizeof(uint32_t); i++) {
		if (rep == PRINT_HEX) {
			printf("| ");
			parser_print_cmd_dword(buf, i);
			printf(" ");
		}
		parser_print_cmd_dword_info(buf, i);
	}
}

static void __parse_dw0(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	int command = cmd->OPCODE & 0x7F;
	int stdcmd = (cmd->OPCODE >> 7)& 0x1;
	int fuse = cmd->FLAGS & 0x3;
	int rsvd = (cmd->FLAGS >> 2) & 0xf;
	int psdt = (cmd->FLAGS >> 6) & 0x3;
	uint16_t cmdid = cmd->command_id;

	if (command >= nvme_cmd_end)
		command = nvme_cmd_end;
	if (stdcmd >= 2)
		stdcmd = 2;
	if (fuse >= fuse_end)
		fuse = fuse_end;
	if (psdt >= psdt_end)
		psdt = psdt_end;

	if (rsvd)
		printf("\n[!] RSVD should be all zeroes\n");

	printf("| %s | %s | %s | %d | %s | CMID %u\n",
								 nvme_opcode_msg[command],
								 nvme_standard_cmd_msg[stdcmd],
								 nvme_fuse_msg[fuse],
								 0,
								 nvme_psdt_msg[psdt],
								 cmdid);
}

static void __parse_dw1(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;

	uint32_t nsid = cmd->nsid;
	printf("| NSID: %u\n", nsid);
}

static void __parse_dw2(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint64_t reserved2 = cmd->DW2;

	printf("| RSVD: %s\n", (reserved2) ? "*not-zero*" : "0");
}

static void __parse_dw4(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint64_t val = cmd->DW4;

	printf("| META: %lu\n", val);
}

static void __parse_dw6(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint64_t val = cmd->DW6;

	printf("| PRP1: %lu\n", val);
}

static void __parse_dw8(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint64_t val = cmd->DW8;

	printf("| PRP2: %lu\n", val);
}

static void __parse_dw10(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint64_t val = cmd->DW10;

	printf("| SLBA: %lu\n", val);
}

static void __parse_dw12(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;

	uint32_t DW12_indian = (uint32_t)cmd->DW12;
	uint32_t length = DW12_indian & 0xFFFF;
	uint32_t reserved1 = (DW12_indian >> 16) & 0x3FF;
	uint32_t protectinfo = (DW12_indian >> 26) & 0x0F;
	uint8_t fua = (DW12_indian >> 30) & 0x01;
	uint8_t limitretry = (DW12_indian >> 31) & 0b1;

	if (reserved1)
		printf("\n[!] bits 16:25 in DW12 should be reserved (all zeroes)\n");

	printf("| LEN: %u | RSVD: %u | PROT: %u | FUA: %d | LIMIT: %d\n", length, reserved1, protectinfo, fua, limitretry);
}

static void __parse_dw13(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;

	uint32_t DW13_indian = (uint32_t)cmd->DW13;
	uint32_t dsm = DW13_indian & 0xff;
	uint32_t reserved2 = (DW13_indian >> 8) & 0xff;

	printf("| DSM %u | RSVD: %s\n", dsm, (!reserved2) ? "0" : "*not-zero*");
}

static void __parse_dw14(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint32_t eilbrt = (uint32_t)cmd->DW14;

	printf("| EILBRT: %u\n", eilbrt);
}

static void __parse_dw15(const uint8_t * const buf)
{
	nvme_rw_command_t *cmd = (nvme_rw_command_t*) buf;
	uint32_t eilbat = (uint32_t)cmd->DW15;

	printf("| EILBAT: %u\n", eilbat);
}

inline void parser_print_cmd_dword_info(const uint8_t * const cmd, const int dword)
{
	switch (dword) {
		case 0: __parse_dw0(cmd);   break;
		case 1: __parse_dw1(cmd);   break;
		case 2: __parse_dw2(cmd);   break;
		case 4: __parse_dw4(cmd);   break;
		case 6: __parse_dw6(cmd);   break;
		case 8: __parse_dw8(cmd);   break;
		case 10: __parse_dw10(cmd); break;
		case 12: __parse_dw12(cmd); break;
		case 13: __parse_dw13(cmd); break;
		case 14: __parse_dw14(cmd);	break;
		case 15: __parse_dw15(cmd); break;
		default:
			printf("|\n");
	}
}
inline void parser_print_cmd_dword(const uint8_t * const cmd, const int dword)
{
	int pos = dword * sizeof(uint32_t);

	printf("%02x %02x %02x %02x", cmd[pos+3] & 0xFF, cmd[pos+2] & 0xFF,
				cmd[pos+1] & 0xFF, cmd[pos+0] & 0xFF);
}

inline void parser_format_cmd_dword(uint32_t const * const cmd, const int dword,
									unsigned char const * const dst,
									const int size)
{
	uint32_t value = *(cmd+dword);
	snprintf((char*)dst, size, "%04x\n", value);
}

void parser_printnbits(const uint8_t * const buf, const size_t start, size_t end,
					const int add_b, const int nl)
{
	int i, j = 0;

	for (i = 0, j = end; i <= (end - start); i++, j--) {
		printf("%d", ((*buf) & (1 << j) ? 1: 0));
	}
	if (add_b)
		printf("b");
	if (nl)
		printf("\n");
}

int parser_dump2hex(const uint8_t * const buf, const size_t size,
						const int put_offset)
{
	unsigned int i, j, off = 0;
	unsigned char endianness[64] = { 0 };

	assert((size /4) == 16);

	memcpy(endianness, buf, size);

	for (i = 0, j = 0; i < size;) {
		if (j == 0) {
			if (put_offset) {
				printf("%02xh ", off);
				off += 8;
			}
			printf("|");
		}
		else if (j == 4)
			printf(" ");
		printf(" %02x", endianness[i]);
		if (j == 7) {
			printf(" | %02xh\n", off);
			j = -1;
		}
		i++, j++;
	}
	return i;
}
