#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "nvme_commands.h"
#include "nvme_messages.h"
#include "nvme_parser.h"
#include "file_operations.h"

int test_file_init(void)
{
	char *path = "./cmdfiles/cmd_test.txt";
	FILE *fp;

	if (file_init(path, &fp)) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	printf("[#Passed] %s:%d\n", __func__, __LINE__);

	return 0;
}

int is_big_endian_machine(void)
{
	volatile uint32_t i = 0x01234567;
	return (((uint8_t*)(&i))[0] ) == 0x67;
}
int test_file_convert_cmd_to_endianness(void)
{
	uint32_t cmd[65] = { 0 };
	uint32_t cmd_orig[65] = { 0 };
	int i, comp = 0;
	uint32_t val = 0x01234567;

	for (i = 0; i < 64 / 4; i++) {
		memcpy(cmd, &val, sizeof(uint32_t));
		memcpy(cmd_orig, cmd, sizeof(uint32_t));
	}

	file_convert_cmd_to_endianness(cmd, USE_BE);
	file_convert_cmd_to_endianness(cmd, USE_BE);
	cmd[65] = 0;
	if ((comp = memcmp(cmd, cmd_orig, 65)) != 0) {
		printf("[!failed] %d %s:%d\n", comp, __func__, __LINE__);
		return -1;
	}

	file_convert_cmd_to_endianness(cmd, USE_LE);
	file_convert_cmd_to_endianness(cmd, USE_LE);
	if (memcmp(cmd, cmd_orig, 65) != 0) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	file_convert_cmd_to_endianness(cmd, USE_LE);
	if ((comp = memcmp(cmd, cmd_orig, 65)) != 0) {
		printf("[!failed] %d %s:%d\n", comp, __func__, __LINE__);
		return -1;
	}
	printf("[#Passed] %s:%d\n", __func__, __LINE__);

	return 0;
}

int test_file_read(void)
{
	FILE *fp;
	uint8_t cmd[65] = { 0 };
	size_t size = 65;
	char *path = "./cmdfiles/cmd_test.txt";

	if (file_init(path, &fp)) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}

	if (file_read(fp, cmd, &size)) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	printf("[#Passed] %s:%d\n", __func__, __LINE__);

	return 0;
}

int test_file_convet_hex32_to_ascii(void)
{
	FILE *fp;
	uint8_t cmd[65] = { 0 };
	uint32_t val = (uint32_t) *cmd;
	uint8_t dst[1024] = { 0 };
	size_t size = 65;
	char *path = "./cmdfiles/cmd_test.txt";

	if (file_init(path, &fp)) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	if (file_read(fp, cmd, &size)) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	if (file_convert_hex32_to_ascii(val, dst, 65, 1) < 32) {
		printf("[!failed] %s:%d\n", __func__, __LINE__);
		return -1;
	}
	printf("[#Passed] %s:%d\n", __func__, __LINE__);
	return 0;
}

int main(int argc, char **argv)
{
	test_file_init();
	test_file_convert_cmd_to_endianness();
	test_file_read();
	test_file_convet_hex32_to_ascii();
}
