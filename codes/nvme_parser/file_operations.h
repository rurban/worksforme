#ifndef __FILE_OPERATIONS__
#define __FILE_OPERATIONS__
#include <stdio.h>
#include <stdint.h>

#define USE_LE (1) // use little-endian
#define USE_BE (0) // use big-endian

#define NBITS_IN_A_BYTE (8)

/* DWORD is composed of 4 bytes, thus, to represent each bit in a
 * 4 bytes word, we need 1 byte per-bit to convert to ASCII.
 * This way, we need 4-bytes * 8-bits/byte * 1-byte = 32 bytes. */
#define DWORD_TO_ASCII (sizeof(uint32_t)*NBITS_IN_A_BYTE)

#define BYTES_IN_DWORD (4)

#define ADD_NL (1)
#define NO_NL (0)

/*
 * It opens the file located at @path, and stores the FILE pointer at @fp
 *
 * [Parameters]
 * @path: path of the file to be opened
 * @fp: FILE pointer variable to store the file pointer
 *
 * [Return]
 * 0: on success, and @fp with the file pointer; -1 otherwise. and @fp may have
 *		a undefined value;
 *
 * [Information]
 * The file is open as a READY_ONLY flag.
 * */
int file_init(const char * const path, FILE **fp);

/*
 * Reads a given ASCII file and converts it to hex into the given buffer
 *
 * [Parameters]
 * @fp: an open file pointer
 * @out: output buffer
 * @size: size of out
 * @add_nl: add a NEW LINE after every 1-byte? 1, yes. 0, no.
 *
 * [Return]
 * 0: on succes; -1 otherwise.
 *
 * [Information]
 * The @size is also used as an output value, that is, the @size stores the
 * converted number of bytes. The converted number of bytes will not be bigger
 * than the @size (when it was the input). */
int file_read(FILE *fp, uint8_t *out, size_t *size);

/* Given the cmd (cast it to uint32_t*), we convert the command to
 * little-endian or big-endian according to the selected option.
 *
 * [INFO]
 * The entire 64 bytes of the NVMe command is converted
 *
 * @cmd: command to be converted (cast to uint32_t*)
 * @le: if 1, convert to little-endian, otherwise, big-endian
 */
void file_convert_cmd_to_endianness(uint32_t * const cmd, const int le);
/*
 * Given a uint32_t value, converts it to ASCII and store it at @dst
 *
 * [Parameters]
 * @value: uint32_t value to be converted to ASCII
 * @dst: destination to store the conversion
 * @size: maximum size of @dst. If convertion reaches this point, it returns
 *			thus avoiding buffer overflow.
 * @add_nl: if 1, it adds a new-line after every 8-bits converstion.
 *
 * [Return]
 * Returns the number of converted bits + the new-lines added.
 *
 * [INFO]
 * This does not consider endianness, it simply converts to ASCII
 */
size_t file_convert_hex32_to_ascii(const uint32_t * value, uint8_t * const dst,
						const size_t size);

#endif
