#ifndef __NVME_MESSAGES_H__
#define __NVME_MESSAGES_H__

static const char *nvme_opcode_msg[] = {
	"nvme_cmd_flush",
	"nvme_cmd_write",
	"nvme_cmd_read",
	"ERROR",
	"nvme_cmd_write_uncor",
	"nvme_cmd_compare",
	"ERROR",
	"ERROR",
	"nvme_cmd_write_zeroes",
	"nvme_cmd_dsm",
	"ERROR",
	"ERROR",
	"ERROR",
	"nvme_cmd_resv_register",
	"nvme_cmd_resv_report",
	"ERROR",
	"ERROR",
	"nvme_cmd_resv_acquire",
	"ERROR",
	"ERROR",
	"ERROR",
	"nvme_cmd_resv_release",
    "nvme_cmd_EOF",
};

static const char *nvme_standard_cmd_msg[] = {
    "nvme_standard_cmd",
    "nvme_non_std_cmd",
    "nvme_EOF"
};

static const char *nvme_fuse_msg[] = {
	"fuse_normal_op",
	"fuse_fused_op_first_cmd",
	"fuse_fused_op_secon_cmd",
	"fuse_reserved",
    "fuse_EOF",
};

static const char *nvme_psdt_msg[] = {
	"psdt_prp",
	"psdt_sgl",
	"psdt_mptr",
	"psdt_rsvd",
	"psdt_EOF",
};
#endif // __NVME_MESSAGES_H__
