#ifndef __CMD_CREATOR_H__
#define __CMD_CREATOR_H__

#include <stdio.h>
#include <stdint.h>

#include "nvme_commands.h"
#include "file_operations.h"

/*
 * Creates the NVMe command given the parameters
 *
 * [Information]
 * This function takes care of the endianness.
 *
 * [Parameters]
 * @opcode: Opcode of the command
 * @slba: starting logic block address
 * @DW12: D-Word 12 of the NVMe command.
 *
 * [Return]
 * Returns a pointer to the nvme_rw_command_t* structure on success,
 * otherwise, returns NULL.
 */
nvme_rw_command_t *cmd_create_command(uint8_t opcode, uint64_t slba,
					uint32_t DW12);

/*
 * Writes the command to a file.
 *
 * [Parameters]
 * @cmd: this is the nvme_command_t pointer with cast to uint32_t*
 * @path: path to the file write (if it is not create, it will be created)
 * @le: write in little-endianess? 1, yes, 0 no.
 * @div: what is the division of the DWORD, 4, 2, 1?
 *			(sizeof(uint32_t) / div) = number of bytes written per line
 *			in the file.
 *
 * [Return]
 * Returns the number of bytes written to the file.
 */
int cmd_write_cmd_to_file(uint32_t * const cmd, char * const path,
						const int div);


#endif // __CMD_CREATOR_H__
