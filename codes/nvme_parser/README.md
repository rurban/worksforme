# NVMe Parser

The `NVMe parser` is composed of two main applications:

1-  The `Command Parser`
2- The `Command Creator`

## `Command Parser`

It is responsible in reading a `.txt` file which has a NVMe command.
The file format must be 1-Byte per line as below, in binary form:

```
00100100
00010000
00011110
[...]
11000010
```

### Compiling the `Command Parser`

You can simply use Makefile to compile it.

To clean old files:

`make clean`

To compile:

`make`

### To run

You can follow the `help` printed by the code execution itself, but below is an example:

```
./main -file nvme_cmd.txt -end b
```

### Output

Once the `Command Parser` parses the `.txt` file, the output will be like this:
```
| 84 83 82 82 | tx_c2h | nvme_cmd_flush | nvme_non_std_cmd | fuse_fused_op_secon_cmd | 0 | psdt_mptr | CMID 33668
| 84 83 82 81 | NSID: 2172814212
| 84 83 82 81 | RSVD: *not-zero*
| 84 83 82 81 |
| 84 83 82 81 | META: 9332165982996824964
| 84 83 82 81 |
| 84 83 82 81 | PRP1: 9332165982996824964
| 84 83 82 81 |
| 84 83 82 81 | PRP2: 9332165980824010752
| 00 00 00 00 |
| 00 00 00 00 | SLBA: 132
| 84 00 00 00 |
| 84 83 82 81 | LEN: 132 | RSVD: 386 | PROT: 0 | FUA: 0 | LIMIT: 1
| 84 83 82 81 | DSM 132 | RSVD: *not-zero*
| 84 83 82 81 | EILBRT: 2172814212
| 84 83 82 81 | EILBAT: 2172814212

```

## `Command Creator`

The command creator allow us to create `.txt` files that is used by the `Command Parser`. 

### Compiling the `Command Creator`

To compile, use the following command:

```
gcc -g -Wall cmd_creator.c nvme_parser.c file_operations.c -o cmd
```

### To run

You can follow the `help` printed by the code execution itself, but below is an example:

```
./cmd -len 123 -div 4 -op write -slba 123 -fua 1
```

This will create a `write` command with `lba` 123 and `length` 123, with the `REQ_FUA` set. and the command `.txt` will be `sizeof(uint32_t) / 4 = 1` (-div 4), in other words, the `.txt` will be 1-byte per line. 

## To run the tests

_[!] The tests are not compiling at this time_

There is a test file to test the source code created here.

### Compiling the test code

`gcc test.c file_operations.c nvme_parser.c -o test`

### Running the tests

`./test`

## Example output

```

```

## Known Issues

- Tests are not compiling

## TODO

- _~~Check the indianness~~ [Fixed in 20 June 19]_
- _~~Create test files and see if the commands match~~ [Fixed in 20 June 19]_
