#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "file_operations.h"

int file_init(const char * const path, FILE **fp)
{
	FILE *f = NULL;
	if ((f = fopen(path, "r")) == NULL) {
		printf("[!] Error to open %s\n", path);
		return -1;
	}
	*fp = f;
	return 0;
}

void file_convert_cmd_to_endianness(uint32_t * const cmd, const int le)
{
	uint32_t endianness;
	int i;

	for (i = 0; i < 64 / sizeof(uint32_t); i++) {
		endianness = le ? htole32(cmd[i]) : htobe32(cmd[i]);
		cmd[i] = endianness;
	}
}

int file_read(FILE *fp, uint8_t *out, size_t *size)
{
	char *line = NULL, *token = NULL;
	size_t len = 0,  i = 0;
	unsigned int conv = 0;
	int ret = 0;

	while (getline(&line, &len, fp) != -1) {
		if (i > *size)
			goto exit;
		if ((token = strchr(line, '\n'))) {
			*token = 0;
			token = NULL;
		}
		if (line[0] == '\0')
			goto exit;

		if ((conv = strtoul(line, 0, 2)) & 0xFF00) {
			printf("[!] Conversion error at %d\n", __LINE__);
			ret = -1;
			goto exit;
		}
		*out++ = (0xFF) & conv;
		i++;
	}
	*out = 0;
exit:
	*size = i;
	return ret;
}

/*
 * Check the host's endianness
 *
 * [Return]
 * 1 if it is little endian, 0 if it is big endian
 */
static int is_little_endian(void)
{
	volatile uint32_t i = 0x1234567;
	return ((*((uint8_t*)(&i))) == 0x67);
}

size_t file_convert_hex32_to_ascii(const uint32_t  * value, uint8_t * const dst,
						const size_t size)
{
	int8_t i = (DWORD_TO_ASCII-1), j;

	if (is_little_endian()) {
		for (i = 0, j = 7; i < 32; ++i, j--) {
			dst[j + (8*(i/8))] = (*value >> i) & 1 ? '1' : '0';
			if (j <= 0)
				j = 8;
		}
	} else {
		for (i = j = 0; i < 32; i++, j++)
			dst[i] = (*value >> i) & 1 ? '1' : '0';
	}
	return j;
}

