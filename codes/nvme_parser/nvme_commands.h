#ifndef __NVME_COMMANDS_H__
#define __NVME_COMMANDS_H__

#pragma once

#include <linux/types.h>
#include <stdint.h>
#include <endian.h> // http://man7.org/linux/man-pages/man3/endian.3.html

// Offset 0x00 : bits (06:02)
/* Can find at page 182 @ spec 1.3d */
enum nvme_opcode {
	nvme_cmd_flush			= 0x00,
	nvme_cmd_write			= 0x01,
	nvme_cmd_read			= 0x02,
	_nvme1,
	nvme_cmd_write_uncor	= 0x04,
	nvme_cmd_compare		= 0x05,
	_nvme2,
	_nvme3,
	nvme_cmd_write_zeroes	= 0x08,
	nvme_cmd_dsm			= 0x09,
	_nvme4,
	_nvme5,
	_nvme6,
	nvme_cmd_resv_register	= 0x0d,
	nvme_cmd_resv_report	= 0x0e,
	_nvme7,
	_nvme8,
	nvme_cmd_resv_acquire	= 0x11,
	_nvme9,
	_nvme10,
	_nvme11,
	nvme_cmd_resv_release	= 0x15,
	nvme_cmd_end,
};

static const unsigned char fuse_bitmask = 0x03;

/* Can find at page 56 @ spec 1.3d */
enum nvme_fuse {
	fuse_normal_op			= 0x00,
	fuse_fused_op_first_cmd = 0x01,
	fuse_fused_op_secon_cmd = 0x02,
	fuse_reserved			= 0x03,
	fuse_end,
};

/* Can find at page 56 @ spec 1.3d */
enum nvme_psdt {
	psdt_prp				= 0x00,
	psdt_sgl				= 0x01,
	psdt_mptr				= 0x02,
	psdt_rsvd				= 0x03,
	psdt_end,
};

/******************************************************************************/
/* This is the NVMe command structure										  */
/******************************************************************************/
typedef struct nvme_rw_command {
	//DW0
	union {
		__u8 OPCODE;
		struct {
			__u8 datatransfer		: 2;
			__u8 function			: 5;
			__u8 stdcmd				: 1;
		};
	};
	union {
		__u8 FLAGS;
		struct {
			__u8 FUSE : 2;
			__u8 reserved0 : 4;
			__u8 psdt : 2;
		};
	};
	__le16		command_id;
	//DW1
	union {
		__le32 DW1;
		struct {
			__le32		nsid;
		};
	};
	//DW2
	union {
		__le64 DW2;
		struct {
			__u64		rsvd2;
		};
	};
	//DW4
	union {
		__le64 DW4;
		struct {
			__le64		metadata;
		};
	};
	//DW6
	union {
		__le64 DW6;
		struct {
			__le64	prp1;
		};
	};
	//DW8
	union {
		__le64 DW8;
		struct {
			__le64	prp2;
		};
	};
	//DW10
	union {
		__le64 DW10;
		struct {
			__le64		slba;
		};
	};
	//DW12
	union {
		__le32	DW12;
		struct {
			__le32		length		: 16;
			__le32		reserved1	: 10;
			__le32		protectinfo	: 4;
			__le32		FUA			: 1;
			__le32		limitretry	: 1;
		};
	};
	//DW13
	union {
		__le32	DW13;
		struct {
			__le32		dsm			: 8;
			__le32		reserved2	: 24;
		};
	};
	//DW14
	union {
		__le32	DW14;
		struct {
			__le32		eilbrt;
		};
	};
	//DW15
	union {
		__le32		DW15;
		struct {
			__le32		elbat		: 16;
			__le32		elbatm		: 16;
		};
	};
} nvme_rw_command_t;

/******************************************************************************/
/* Command Completion Queue Entry (16-bytes long)							  */
/******************************************************************************/
typedef struct completion_queue_entry {
	uint32_t command_specific;
	uint32_t reserved;
	struct {
		uint32_t SQHD : 16;
		uint32_t SQID : 16;
	};
	struct {
		uint32_t CID : 16;
		uint32_t P : 1;
		uint32_t SF : 15;
	};
} completion_queue_entry_t;

/******************************************************************************/
/* This is the PCIe Registers based on the specification					  */
/******************************************************************************/
typedef struct pcie {
	struct {
		uint32_t VID : 16;
		uint32_t DID : 16;
	};
	//0x04
	struct {
		uint16_t IOSE : 1;
		uint16_t MSE : 1;
		uint16_t BME : 1;
		uint16_t SCE : 1;
		uint16_t MWIE : 1;
		uint16_t VGA : 1;
		uint16_t PEE : 1;
		uint16_t ZERO : 1;
		uint16_t SEE : 1;
		uint16_t FBE : 1;
		uint16_t ID : 1;
		uint16_t RSVD1 : 5;
	};
	//0x06
	struct {
		uint16_t RSVD2 : 3;
		uint16_t IS : 1;
		uint16_t CL : 1;
		uint16_t C66 : 1;
		uint16_t RSVD3 : 1;
		uint16_t FBC : 1;
		uint16_t DPD : 1;
		uint16_t DEVT : 2;
		uint16_t STA : 1;
		uint16_t RTA : 1;
		uint16_t RMA : 1;
		uint16_t SSE : 1;
		uint16_t DPE : 1;
	};
	//0x08
	uint8_t RID;
	//0x09
	uint8_t PI;
	uint8_t SCC;
	uint8_t BCC;
	//0xc
	uint8_t CLS;
	uint8_t MLT;
	struct {
		uint8_t HL : 7;
		uint8_t MFD : 1;
	};
	//0x0f
	struct {
		uint8_t CC : 4;
		uint8_t RSVD4 : 2;
		uint8_t SB : 1;
		uint8_t BC : 1;
	};
	//0x10
	struct {
		uint32_t RTE : 1;
		uint32_t TP : 2;
		uint32_t PF : 1;
		uint32_t RSVD5 : 10;
		uint32_t BA : 18;
	};
	//0x14
	uint32_t BAR1;
	//0x18
	uint32_t BAR2;
	//0x1c
	uint32_t BAR3;
	//0x20
	uint32_t BAR4;
	//0x24
	uint32_t BAR5;
	//0x28
	uint32_t CCPTR;
	//0x2c
	struct {
		uint32_t SSVID : 16;
		uint32_t SSID : 16;
	};
	//0x30
	uint32_t EROM;
	//0x34: indicates the first capability pointer offset
	uint32_t CAP;
	uint32_t RSVD6;
	//0x3c
	uint8_t ILINE;
	uint8_t IPIN;
	//0x3e
	uint8_t GNT;
	//0x3f
	uint8_t LAT;
} pcie_t;

#endif // __NVME_COMMANDS_H__
