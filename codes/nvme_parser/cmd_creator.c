#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <endian.h>

#include "cmd_creator.h"

static void args_error(char **argv)
{
	printf( "Usage: %s -C\n"													\
			"-op : Operation of this NVMe command\n"							\
			"\t\tread : create a read NVMe command\n"							\
			"\t\twrite: create a write NVMe command\n"							\
			"-slba : Starting Logib Block Address\n"							\
			"-len : Number of blocks\n"											\
			"-fua : Force Unit Access\n"										\
			"\t\t1 : request a FUA access\n"									\
			"\t\t0 : no FUA\n"													\
			"-file : filename to the file to be created\n"						\
			"[-div] : How to divide the DWORDS in the output file?\n"			\
			"\t\t4 : Divide DW to 4 = 1 byte per line\n"						\
			"\t\t2 : Divide DW to 2 = 2 bytes per line\n"						\
			"\t\t(default = 4) : Divide DW to 1 = 4 bytes per line\n"			\
			"[-end] : Endianess to write to the file\n"							\
			"\t\t1: Little-endian\n"											\
			"\t\t0: Big-endian\n"												\
			"\t\t(default = 1)\n"												\
			, argv[0]);
	exit(-1);
}

/*
 * Argument parser
 *
 * [Parameters]
 * @argc: the traditional argc
 * @argv: the traditional argv
 * @opcode: pointer to where the opcode is going to be stored
 * @dst_slba: pointer to where the slba is going to be written
 * @DW12: pointer to where DW12 is going to be stored
 * @div: pointer where the divisor is going to be stored
 * @path: pointer to store the path of the file
 * @endian: pointer to store the endianness
 *
 * [Return]
 * Returns an integer with bits set according to what the perser has find
 */
static int ARGS(int argc, char **argv, uint8_t *opcode, uint64_t *dst_slba,
						uint32_t *DW12, int *div, char **path, int *end)
{
	unsigned int arg_iter = 0, i;
	uint32_t op_rw = 0x01; // default is read
	uint64_t slba = 0;
	uint32_t length = 0, fua = 0;
	char *ptr;

	// 11 - because we have 5 mandatory fields, so, total is 10 with the args
	// of the commands + the appliacation name
	if (argc < 11 || argc > 15) {
		args_error(argv);
		return -1;
	}
	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "-op")) {
			arg_iter |= 0x01;
			if (!strcmp(argv[++i], "read"))
				op_rw = 0x02;
			else if (!strcmp(argv[i], "write"))
				op_rw = 0x01;
		} else if (!strcmp(argv[i], "-slba")) {
			arg_iter |= 0x01 << 1;
			slba = strtoull(argv[++i], &ptr, 10);
		} else if (!strcmp(argv[i], "-len")) {
			arg_iter |= 0x1 << 2;
			length = strtoul(argv[++i], &ptr, 10);
			if (length > 0xFFFF) {
				printf("[!] Warning, length is %d > 65535\n"\
						"Using 65535 instead\n", length);
				length = 0xFFFF;
			}
		} else if (!strcmp(argv[i], "-fua") ||
						!strcmp(argv[i], "-FUA")) {
			arg_iter |= 0x01 << 3;
			fua = strtoul(argv[++i], &ptr, 10);
		} else if (!strcmp(argv[i], "-file")) {
			arg_iter |= 0x01 << 4;
			*path = argv[++i];
		} else if (!strcmp(argv[i], "-end")) {
			arg_iter |= 0x01 << 5;
			*end = strtoul(argv[++i], NULL, 10) == 0 ? 0 : 1;
		} else if (!strcmp(argv[i], "-div")) {
			arg_iter |= 0x01 << 6;
			*div = strtoul(argv[++i], &ptr, 10);
			switch (*div) {
				case 1:
				case 2:
				case 4:
					break;
				default:
					*div = 4;
			}
		}
	}
	*opcode = op_rw;
	*dst_slba = slba;
	*DW12 =  (fua << 30) | (length & 0xFFFF);

	if ((arg_iter == 0x1F && argc != 11) ||
		(arg_iter == 0x3F && argc != 13) ||
		(arg_iter == 0x7F && argc != 15)) {
		args_error(argv);
	}

	return arg_iter;
}

nvme_rw_command_t *cmd_create_command(uint8_t opcode, uint64_t slba
							, uint32_t DW12)
{
	nvme_rw_command_t *cmd = calloc(1, 64);

	if (!cmd)
		return NULL;

	/* Here we just need to fill these fields in the cmd */
	cmd->OPCODE = opcode;
	cmd->DW10 = htole64(slba);
	cmd->DW12 = htole32(DW12);
	return cmd;
}

#define NVME_NUM_DWORDS (64/sizeof(uint32_t))

int cmd_write_cmd_to_file(uint32_t * const cmd, char *const path, const int div)
{
	FILE *fp = NULL;
	int8_t dwiter;
	int32_t bufiter = 0;
	uint8_t buffer[1024] = { 0 };
	int i;
	size_t nbytes = 0;
	char nl = '\n';

	for (dwiter = bufiter = 0; dwiter < NVME_NUM_DWORDS; dwiter++, bufiter = (dwiter*32)) {
		file_convert_hex32_to_ascii(&(cmd[dwiter]), &buffer[bufiter], (DWORD_TO_ASCII+1));
	}

	if ((fp = fopen(path, "w+")) == NULL) {
		printf("[!] Could not create %s\n", path);
		return -1;
	}

	for (i = 0; i < 4 * NVME_NUM_DWORDS * NBITS_IN_A_BYTE; i+= (4 / div) * NBITS_IN_A_BYTE) {
		nbytes += fwrite(&buffer[i], (sizeof(uint32_t) / div) * NBITS_IN_A_BYTE, 1, fp);
		nbytes += fwrite(&nl, 1, 1, fp);
	}
	fclose(fp);
	return nbytes;
}

int main(int argc, char **argv)
{
	uint8_t opcode = 0;
	uint64_t slba = 0;
	uint32_t DW12 = 0;
	char *path;
	int end = 1;
	nvme_rw_command_t *cmd;
	int div = 4;

	ARGS(argc, argv, &opcode, &slba, &DW12, &div, &path, &end);
	cmd = cmd_create_command(opcode, slba, DW12);
	file_convert_cmd_to_endianness((uint32_t*)cmd, end);
	if(cmd_write_cmd_to_file((uint32_t*)cmd, path, div) != -1)
		printf("Command created successfully.\nWritten to => %s\n", path);
	else
		printf("Could not write to => %s\n", path);

	free(cmd);
	return 0;
}
