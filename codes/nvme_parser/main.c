#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "nvme_parser.h"
#include "file_operations.h"
#include "nvme_commands.h"

static void args_error(char **argv)
{
	printf("Usage: %s\n"													 \
			"-file  : File to parse\n"										 \
			"[-repr] : representation of the output\n"						 \
			"\t\t1 : print the HEX before the command parsed\n"				 \
			"\t\t0 : do not print HEX before the command parsed\n"			 \
			"\t\t(default = 0)\n"											 \
			"\n[INFO] The command.txt file must be 1-byte (ASCII) per line.\n" \
			"\tByte order are from DW0 - DW15. Also, the byte reading is from\n"\
			"\tbyte-0, byte-1, byte-2, byte-3, and so on.\n\n"				 \
			, argv[0]);
	exit(-1);
}
static void ARGS(int argc, char **argv, char ** path, int *rep)
{
	unsigned int arg_iter = 0;
	int i;

	if (argc < 3 || argc > 5) {
		args_error(argv);
	}
	for (i = 0; i < argc; ++i) {
		if (!strcmp(argv[i], "-file")) {
			arg_iter |= 0x01;
			*path = argv[++i];
		} else if (!strcmp(argv[i], "-repr")) {
			arg_iter |= 0x01 << 1;
			*rep = strtoul(argv[++i], NULL, 10) == 0 ? 0 : 1;
		}
	}
	if ((arg_iter == 0x01 && argc != 3) ||
		(arg_iter == 0x03 && argc != 5) ||
		(arg_iter == 0x02 && argc != 3) ||
		(arg_iter != 0x01 && arg_iter != 0x03))
		args_error(argv);
}

int main(int argc, char **argv)
{
	size_t size = 1024;
	FILE *fp = NULL;
	char *path = NULL;
	unsigned char dst[1024] = { 0 };
	int rep = 0;

	assert(sizeof(pcie_t) == 64);
	assert(sizeof(nvme_rw_command_t) == 64);
	assert(sizeof(completion_queue_entry_t) == 16);

	ARGS(argc, argv, &path, &rep);

	if (file_init(path, &fp)) {
		printf("[!] Error at %d\n", __LINE__);
		return -1;
	} else if (file_read(fp, &dst[0], &size)) {
		printf("[!] Error at %d\n", __LINE__);
		return -1;
	}

	parser_print_full_cmd(dst, 64, rep);

	return 0;

	/* If you want to hexdump, uncomment this */
	/*
	parser_dump2hex(dst, size, 1, USE_LE);
	printf("\n");
	parser_dump2hex(dst, size, 1, USE_BE);
	return 0;
	*/
}

