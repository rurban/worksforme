#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv)
{
	uint64_t val = 123456789;
	char *ptr = (char*) &val;

	int i = 0, j = 0;
	//int valor = 5;
	//ptr = (char*)&valor;
	//
	printf("ptr = %p\n", ptr);
	printf("ptr* = ");
		for (i = 7; i >= 0; i--)
			printf("%d", (*ptr >> i ) & 1);

	printf("\nval = %p\n", &val);
	printf("ptr = %p\n", (ptr + 1));
	for (j = 0; j < 4; j++) {
		for (i = 7; i >= 0; i--)
			printf("%d", (*(ptr+j) >> i ) & 1);
		printf("-");
	}
	return 0;
}
