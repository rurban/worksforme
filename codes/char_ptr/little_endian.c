#include <stdio.h>
#include <stdint.h>

int main(int argc, char **argv)
{
    uint32_t valor = 0x44332255;
    char *ptr = (char*) &valor;

    if (*((char*) &valor) == 0x55)
        printf("little endian = %d\n", *ptr);
    else 
        printf("big endian = %d\n", *ptr);



    return 0;
}