#!/bin/bash

a=1

echo "RUNNING TIMELAPSE"
while [ 1 ]
do
    new=$(printf "img%06d.jpeg" "$a")
    ffmpeg -f video4linux2 -s 1920x1080 -i /dev/video0 -vframes 1 $new
    #streamer -q -c /dev/video0 -o $new
    let a=a+1
    sleep 5
done



ffmpeg -r 30 -f image2 -s 1920x1080 -i img%06d.jpeg -vcodec libx264 -crf 15  -pix_fmt yuv420p test.mp4
ffmpeg -r 60 -i img%06d.jpeg -vb 20M -vcodec mpeg4 tst.avi
