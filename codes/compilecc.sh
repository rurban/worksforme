# Some MUST HAVE GCC flags
CFLAGS += -Wall -Wextra -Wpedantic \
          -Wformat=2 -Wno-unused-parameter -Wshadow \
          -Wwrite-strings -Wstrict-prototypes -Wold-style-definition \
          -Wredundant-decls -Wnested-externs -Wmissing-include-dirs

# Optimization can also help to detect errors
CFLAGS += O2

# Debug flags for GDB
CFLAGS += -g

# Extra flags that works only for GCC
CFLAGS += -Wjump-misses-init -Wlogical-op

# Using the most up-to-date standard
CFLAGS += -std=c11

# Warn when strings (char[]) are assigned to pointers (char *)
CFLAGS += -Wwrite-strings
