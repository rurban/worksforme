#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/timer.h>

typedef struct z_data {
    struct timer_list z_timer;
    wait_queue_head_t z_wait_commit;
    struct task_struct *z_task;
} z_data_t;

z_data_t my_thread_info_s;
z_data_t *my_thread_info = &my_thread_info_s;

static void z_timer_fn(struct timer_list *tmr)
{
    struct task_struct *t = (struct task_struct *) tmr;

    printk(KERN_ERR "TIMER[%d]\n", __LINE__);
    wake_up_process(t);
}

int thread_fn(void *args)
{
    z_data_t *t = args;

    t->z_timer.expires = 100;//= (unsigned long) current;
    t->z_timer.data = 100;//= (unsigned long) current;
    timer_setup(&t->z_timer, z_timer_fn, 0);
    t->z_task = current;

    printk(KERN_ERR "RUNNING THREAD\n");

    wake_up(&t->z_wait_commit);
    printk(KERN_ERR "[%d]\n", __LINE__);

    return 0;
}

int thread_init(void)
{
    struct task_struct *t;
    char our_thread[8] = "thread1";
    printk(KERN_ERR "in init\n");

    init_waitqueue_head(&my_thread_info->z_wait_commit);
    t = kthread_create(thread_fn, my_thread_info, our_thread);
    if (!IS_ERR(t))
        wake_up_process(t);
    else
        return PTR_ERR(t);

    printk(KERN_ERR "[%d]\n", __LINE__);
    wait_event(my_thread_info->z_wait_commit, my_thread_info->z_task != NULL);
    printk(KERN_ERR "[%d]\n", __LINE__);
    return 0;
}

void thread_cleanup(void)
{
    int ret;

    return;
    ret = kthread_stop(my_thread_info->z_task);
    if (!ret)
        printk(KERN_ERR "Thread stopped\n");
}
MODULE_LICENSE("GPL");
module_init(thread_init);
module_exit(thread_cleanup);
