import requests
import time

f_from = "POA"
#f_from = "GIG"
#f_from = "GRU"

def check_flight(in_date="2017-07-21", out_date="2017-12-16"):

    url = "http://br.investing.com/currencies/usd-brl"
    url = "http://www.e-agencias.com.br/api/flights/flights?site=BR&from="+f_from+"&to=ICN&departureDate="+in_date+ "&adults=1&children=0&infants=0&cabinType=&facets.stops=&facets.airlines=&markup=&specialPassenger=&returnDate="+out_date+"&agency=paylesstur"

    r = requests.get(url)

    lines = []

    text = r.text

    i = text.find("adults_subtotal")
    j = i+17
    val = text[j:j+4]

    print "{0}, {1}".format(str(out_date), str(val))
    return

def main():

    for i in range(20,32):
        print "2017-07-{0} ----------------------------[{1}]".format(str(i), f_from)
        for j in range(1,30):
            check_flight("2017-07-"+str(i).zfill(2), "2018-01-"+str(j).zfill(2))
            time.sleep(1)

if __name__ == "__main__":main()
