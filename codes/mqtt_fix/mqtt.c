/******************************************************************************

 Copyright (C) 2020 THALES DIS AIS Deutschland GmbH

 This software is protected by international intellectual property laws and
 treaties. Customer shall not reverse engineer, decompile or disassemble the
 source code and shall only use the source code for the purpose of evaluation,
 analysis and to provide feedback thereon to Gemalto. Any right, title and
 interest in and to the source code, other than those expressly granted to the
 customer under this agreement, shall remain vested with Gemalto. This
 license may be terminated by Gemalto at any time in writing. Customer
 undertakes not to provide third parties with the source code. In particular,
 Customer is not allowed to sell, to lend or to license the source code or to
 make it available to the public.

 The information contained in this document is considered the CONFIDENTIAL and
 PROPRIETARY information of Gemalto M2M GmbH and may not be
 disclosed or discussed with anyone who is not employed by Gemalto M2M
 GmbH, unless the individual company:

 i)  has an express need to know such information, and

 ii) disclosure of information is subject to the terms of a duly executed
 Confidentiality  and Non-Disclosure Agreement between Gemalto M2M GmbH
 and the individual company.

 THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 GEMALTO, ITS LEGAL REPRESENTATIVES AND VICARIOUS AGENTS SHALL - IRRESPECTIVE
 OF THE LEGAL GROUND - ONLY BE LIABLE FOR DAMAGES IF THE DAMAGE WAS CAUSED
 THROUGH CULPABLE BREACH OF A MAJOR CONTRACTUAL OBLIGATION (CARDINAL DUTY),
 I.E. A DUTY THE FULFILMENT OF WHICH ALLOWS THE PROPER EXECUTION OF THE
 RESPECTIVE AGREEMENT IN THE FIRST PLACE OR THE BREACH OF WHICH PUTS THE
 ACHIEVEMENT OF THE PURPOSE OF THE AGREEMENT AT STAKE, RESPECTIVELY, AND ON
 THE FULFILMENT OF WHICH THE RECIPIENT THEREFORE MAY RELY ON OR WAS CAUSED BY
 GROSS NEGLIGENCE OR INTENTIONALLY. ANY FURTHER LIABILITY FOR DAMAGES SHALL -
 IRRESPECTIVE OF THE LEGAL GROUND - BE EXCLUDED. IN THE EVENT THAT GEMALTO
 IS LIABLE FOR THE VIOLATION OF A MAJOR CONTRACTUAL OBLIGATION IN THE ABSENCE
 OF GROSS NEGLIGENCE OR WILFUL CONDUCT, SUCH LIABILITY FOR DAMAGE SHALL BE
 LIMITED TO AN EXTENT WHICH, AT THE TIME WHEN THE RESPECTIVE AGREEMENT IS
 CONCLUDED, GEMALTO SHOULD NORMALLY EXPECT TO ARISE DUE TO CIRCUMSTANCES THAT
 THE PARTIES HAD KNOWLEDGE OF AT SUCH POINT IN TIME. GEMALTO SHALL IN NO
 EVENT BE LIABLE FOR INDIRECT AND CONSEQUENTIAL DAMAGES OR LOSS OF PROFIT.
 GEMALTO SHALL IN NO EVENT BE LIABLE FOR AN AMOUNT EXCEEDING EUR 20,000.00
 PER EVENT OF  DAMAGE. WITHIN THE BUSINESS RELATIONSHIP THE OVERALL LIABILITY
 SHALL BE  LIMITED TO A TOTAL OF EUR 100,000.00. CLAIMS FOR DAMAGES SHALL
 BECOME TIME-BARRED AFTER ONE YEAR AS OF THE BEGINNING OF THE STATUTORY
 LIMITATION PERIOD. IRRESPECTIVE OF THE LICENSEE'S KNOWLEDGE OR GROSS NEGLIGENT
 LACK OF  KNOWLEDGE OF THE CIRCUMSTANCES GIVING RISE FOR A LIABILITY ANY CLAIMS
 SHALL BECOME TIME-BARRED AFTER FIVE YEARS AS OF THE LIABILITY AROSE. THE
 AFOREMENTIONED LIMITATION OR EXCLUSION OF LIABILITY SHALL NOT APPLY IN THE
 CASE OF CULPABLE INJURY TO LIFE, BODY OR HEALTH, IN CASE OF INTENTIONAL ACTS,
 UNDER THE LIABILITY PROVISIONS OF THE GERMAN PRODUCT LIABILITY ACT
 (PRODUKTHAFTUNGSGESETZ) OR IN CASE OF A CONTRACTUALLY AGREED OBLIGATION TO
 ASSUME LIABILITY IRRESPECTIVE OF ANY FAULT (GUARANTEE).

 IN THE EVENT OF A CONFLICT BETWEEN THE PROVISIONS OF THIS AGREEMENT AND
 ANOTHER AGREEMENT REGARDING THE SOURCE CODE (EXCEPT THE GENERAL TERMS AND
 CONDITIONS OF GEMALTO) THE OTHER AGREEMENT SHALL PREVAIL.

 All rights created by patent grant or registration of a utility model or
 design patent are reserved.
******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>

#include "qapi_fs_types.h"
#include "qapi_status.h"
#include "qapi_socket.h"
#include "qapi_dss.h"
#include "qapi_netservices.h"

#include "qapi_uart.h"
#include "qapi_timer.h"
#include "qapi_diag.h"
#include "qapi_ns_utils.h"

#include "qapi_ssl.h"
#include "qapi_fs.h"
#include "qapi_fs_types.h"

#include "qapi_mqtt.h"

#include "gina.h"

/**
    ************** TEST SETUP **************

    This test is intended to show
     *  a generic mqtt(s) handling
     *  some memory pool handling
     *  some event handling

    Every setup value is compiled into the application.

    Preconditions:
      - PIN unlocked
      - module successfully attached to the network
      - used PDP context not yet established from
        host, IP Services or another DAM application

    To do this request, a valid PDP context has to be
    established before, this is done by this application:
      - Please change apn, apn_username, apn_passwd
        to valid values of your your used network
      - The contexct has to exist in CGDCONT database:
        Please add this context (once, persitent) with
          AT+CGDCONT=<ProfileIdx>,<IpVer>,<APN>
        with the correct values before using it.
      - this application will use the default APN @index 1

    For mqtts, the used certificates from the TLS demo can be resued, to
    be found in ../tls/certs
    To use them, the *.pem files have to be copied to FFS root.

    mqtts demo will read its used certs always from
      /demo_certificate.pem       --> client cert
      /demo_certificate_key.pem   --> client cert private key
      /demo_rootca.pem            --> a validation rootca
    Please keep the name unchanged, otherwise please update the names in following codes.

    As in every example program, flash lifetime is not optimized.
    Global certificate loading/removing functions like
      qapi_Net_SSL_Cert_Convert_And_Store
      qapi_Net_SSL_Cert_delete
    work on the secured file system by adding/removing certificates
    with a module unique key. This is very secure, but
    should not called on every application start or stop.
    Like every continuous flash write access, this would
    decrease flash lifetime.
*/

// TESTNET; PLEASE CHANGE TO YOUR NETWORK PROVIDER FOR A VALID NETWORK CONNECTION!!!
static char apn[] = "YOUR_APN_HERE";
static char apn_username[] = "USERNAME";
static char apn_passwd[] = "PASSWORD";

//mqtt configuration
static char will_topic_ptr[] = "mqttwill";
static char will_message_ptr[] = "This is a will message from willTopic";
static char username_ptr[] = "mqtt_user1";
static char password_ptr[] = "mqtt_user1_pw";

/* Enable MQTT WILL Management */
#define ENABLE_MQTT_WILL

/*Choose which to support, ipv4 or ipv6. Then update the mqtt_server_ip address*/
#define ENABLE_MQTT_IPV4
//#define ENABLE_MQTT_IPV6
static char mqtt_server_ip[] = "123.56.73.138"; // Bln MQTT Test Server Address

/*Disabled by default. Uncomment this if mqtts is used*/
#define ENABLE_SSL_MQTT

#ifdef ENABLE_SSL_MQTT
static char mqtts_server_ip[] = "123.56.73.138"; // Bei MQTTS Test Server Address
#define CERT_FILE           "crt.bin"
#define ROOT_CA_File        "ca_list.bin"
#endif

/*===========================================================================
                           Global variable
===========================================================================*/

typedef enum
{
    NETCTRL_DSSLIB_STATE_CLOSED,
    NETCTRL_DSSLIB_STATE_FAILED,
    NETCTRL_DSSLIB_STATE_OPENED
} DSS_Lib_Status_e;

typedef enum
{
    NETCTRL_SIG_EVT_INVALID_EVENT     = 0x0001,
    NETCTRL_SIG_EVT_NOCONN_EVENT      = 0x0002,
    NETCTRL_SIG_EVT_CONNECTED_EVENT   = 0x0004,
    NETCTRL_SIG_EVT_DISCONNECTED_EVENT= 0x0010,
    NETCTRL_SIG_EVT_EXIT_EVENT        = 0x0020
} DSS_Signal_Evt_e;

static signed char netctrl_lib_status = NETCTRL_DSSLIB_STATE_CLOSED;
static qapi_DSS_Net_Evt_t netctrl_pdp_status = QAPI_DSS_EVT_INVALID_E;
static qapi_DSS_Hndl_t netctrl_dss_handle = NULL;

static TX_EVENT_FLAGS_GROUP *main_signal_hndl;


static qapi_Net_MQTT_Hndl_t    _mqtt_handle = NULL;
static qapi_Net_MQTT_Config_t  _mqtt_config;

#define _htons(x) ((unsigned short)((((unsigned short)(x) & 0x00ff) << 8) | (((unsigned short)(x) & 0xff00) >> 8)))
#define _ntohs(x) ((unsigned short)((((unsigned short)(x) & 0x00ff) << 8) | (((unsigned short)(x) & 0xff00) >> 8)))


//-------------------------DEBUG-----------------------
#define DSS_ADDR_SIZE       16
#define DSS_ADDR_INFO_SIZE  5
#define GET_ADDR_INFO_MIN(a, b) ((a) > (b) ? (b) : (a))

/*===========================================================================
                               FUNCTION
===========================================================================*/


#define TEST_BYTE_POOL_SIZE 30720*8

static TX_BYTE_POOL *byte_pool_test;
static uint32 free_memory_test[TEST_BYTE_POOL_SIZE/4];

static int netctrl_stop(void);
static qapi_Status_t dam_byte_pool_deinit(void);


/* cleanup used resources */
static void cleanup(void * data)
{
  if (main_signal_hndl != NULL) {
    tx_event_flags_delete(main_signal_hndl);
    txm_module_object_deallocate(main_signal_hndl);
  }

  dam_byte_pool_deinit();
  gina_cleanup();
  GINA_UWLOG_CRITICAL("cleanup: %s", (gchar *)data);
}


/*!
    @brief
    Create byte pool for DAM application.
    @return
    None
*/
/*=========================================================================*/
static qapi_Status_t dam_byte_pool_init(void)
{
    int ret;

    /* Allocate byte_pool_dam (memory heap) */
    ret = txm_module_object_allocate(&byte_pool_test, sizeof(TX_BYTE_POOL));
    if(ret != TX_SUCCESS)
    {
      gina_uwlog_printf("Allocate byte_pool_dam fail");
      return ret;
    }

    /* Create byte_pool_dam */
    ret = tx_byte_pool_create(byte_pool_test,
                              "Test pool",
                              free_memory_test,
                              TEST_BYTE_POOL_SIZE);
    if(ret != TX_SUCCESS)
    {
      gina_uwlog_printf("Create byte_pool_dam fail");
      return ret;
    }

    return ret;
}


static qapi_Status_t dam_byte_pool_deinit(void)
{
    int ret;

    ret = tx_byte_pool_delete(byte_pool_test);
    if(ret != TX_SUCCESS) {
        GINA_UWLOG_INFO("delete byte_pool fail");
        return ret;
    }

    /* Allocate byte_pool_dam (memory heap) */
    ret = txm_module_object_deallocate(byte_pool_test);
    if(ret != TX_SUCCESS) {
        GINA_UWLOG_INFO("Deallocate byte_pool fail");
    }
    return ret;
}


static void netctrl_event_cb
(
  qapi_DSS_Hndl_t         hndl,
  void                    *user_data,
  qapi_DSS_Net_Evt_t      evt,
  qapi_DSS_Evt_Payload_t  *payload_ptr
)
{
  /*****************************
   ATTENTION: DO NOT CALL ANY
   GINA/QAPI FUNCTIONS CAUSE THEY
   USUALLY RUN INTO A SYSCALL
   NOT POSSIBLE HERE
   *****************************/

    qapi_Status_t status = QAPI_OK;

    switch (evt)
    {
        case QAPI_DSS_EVT_NET_IS_CONN_E:
            //** datacall connected */;
            /* Signal main task */
            netctrl_pdp_status = QAPI_DSS_EVT_NET_IS_CONN_E;
            tx_event_flags_set(main_signal_hndl,
                                NETCTRL_SIG_EVT_CONNECTED_EVENT, TX_OR);

			gina_uwlog_printf("%s) QAPI_DSS_EVT_NET_IS_CONN_E", __func__);
            break;

        case QAPI_DSS_EVT_NET_NO_NET_E:
            if (QAPI_DSS_EVT_NET_IS_CONN_E == netctrl_pdp_status)
            {
                tx_event_flags_set(main_signal_hndl,
                                    NETCTRL_SIG_EVT_EXIT_EVENT, TX_OR);
				gina_uwlog_printf("%s) QAPI_DSS_EVT_NET_IS_CONN_E", __func__);
            }
            else
            {
                /* DSS status maybe QAPI_DSS_EVT_NET_NO_NET_E
                    before data call establishment */
                tx_event_flags_set(main_signal_hndl,
                                    NETCTRL_SIG_EVT_NOCONN_EVENT, TX_OR);
				gina_uwlog_printf("%s) ! QAPI_DSS_EVT_NET_IS_CONN_E", __func__);
            }
			gina_uwlog_printf("%s) QAPI_DSS_EVT_NET_NO_NET_E", __func__);
            break;
        default:
            /* Signal main task */
            tx_event_flags_set(main_signal_hndl,
                              NETCTRL_SIG_EVT_INVALID_EVENT, TX_OR);
			gina_uwlog_printf("%s) NETCTRL_SIG_EVT_INVALID_EVENT", __func__);
            break;
    }
}


static int netctrl_SetPDPParams(int IpVer)
{
    qapi_Status_t rc;
    qapi_DSS_Call_Param_Value_t param_info;

    if (NULL == netctrl_dss_handle) {

        gina_uwlog_printf("Dss handler is NULL!!!");
        return -1;
    }

    /* set data call param */
    param_info.buf_val = NULL;
    param_info.num_val = QAPI_DSS_RADIO_TECH_UNKNOWN;
    rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                      QAPI_DSS_CALL_INFO_TECH_PREF_E,
                                      &param_info);
    GINA_UWLOG_INFO("Set TECH_PREF to AUTO, rc=%d", rc);

    /* set apn */
    param_info.buf_val = apn;
    param_info.num_val = strlen(apn);
    rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                      QAPI_DSS_CALL_INFO_APN_NAME_E,
                                      &param_info);
    gina_uwlog_printf("Set APN=%s, rc=%d", apn, rc);

    if (strlen(apn_username)>0)
    {
        /* set apn username */
        param_info.buf_val = apn_username;
        param_info.num_val = strlen(apn_username);
        rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                          QAPI_DSS_CALL_INFO_USERNAME_E,
                                          &param_info);
        gina_uwlog_printf("Set APN_USER=%s, rc=%d", apn_username, rc);

        /* set apn password */
        param_info.buf_val = apn_passwd;
        param_info.num_val = strlen(apn_passwd);
        rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                          QAPI_DSS_CALL_INFO_PASSWORD_E,
                                          &param_info);
        gina_uwlog_printf("Set APN_PASSWORD=%s, rc=%d", apn_passwd, rc);
    }

    /* set IP version(NON-IP IPv4 or IPv6) */
    param_info.buf_val = NULL;
    param_info.num_val = IpVer;
    rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                      QAPI_DSS_CALL_INFO_IP_VERSION_E,
                                      &param_info);
    gina_uwlog_printf("Set IP_family=IPv_%d (2:NonIP 4:v4 6:v6 10:v4_6)", param_info.num_val, rc);

    /**
        depending from the used network, not every +CGDCONT position is usable
        from DAM application.

        Often, 1-4 is used internally by the modem, and the
        customer has to use 5++

        Sometimes (i.e. Verizon) the usage of a dedicated APN index + name is
        dedicated.

        PLEASE NOTE:
        The contexct has to exist!
        Please add this context (once, persitent) with
          AT+CGDCONT=<ProfileIdx>,<IpVer>,<APN>
        with the correct values before using it.

        The MQTT example use the default context on Idx 1 for a connection.
    */
    param_info.buf_val = NULL;
    param_info.num_val = 1;
    rc = qapi_DSS_Set_Data_Call_Param(netctrl_dss_handle,
                                      QAPI_DSS_CALL_INFO_UMTS_PROFILE_IDX_E,
                                      &param_info);
    gina_uwlog_printf("Set UMTS_PROFILE_IDX=%d, rc=%d", param_info.num_val, rc);

    /** we currently ignore any error; for a
        product, this is not the best idea */
    return 0;
}



/* Initializes the DSS netctrl library for the specified operating mode */
static int netctrl_init(void)
{
    int ret_val = 0;
    qapi_Status_t status = QAPI_OK;

    /* Initializes the DSS netctrl library */
    if (QAPI_OK == qapi_DSS_Init(QAPI_DSS_MODE_GENERAL))
    {
        netctrl_lib_status = NETCTRL_DSSLIB_STATE_OPENED;
        gina_uwlog_printf("qapi_DSS_Init() success");
    }
    else
    {
        /* @Note: netctrl library has been initialized */
        netctrl_lib_status = NETCTRL_DSSLIB_STATE_FAILED;
        gina_uwlog_printf("qapi_DSS_Init() failed;"
                          " continue anyway (already initialized?)");
    }

    /* Registering callback */
    do
    {
        gina_uwlog_printf("Registering Callback dss handle");

        status = qapi_DSS_Get_Data_Srvc_Hndl(netctrl_event_cb, NULL,
                                              &netctrl_dss_handle);
        gina_uwlog_printf("dss handle=%p, status=%d, %s",
                          netctrl_dss_handle,
                          status,
                          NULL==netctrl_dss_handle?"FAIL, RETRY":"SUCCESS");

        if (NULL != netctrl_dss_handle)
        {
          gina_uwlog_printf("Registed netctrl_dss_handler success");
          break;
        }

        /* retry each 100 ms if failed*/
        qapi_Timer_Sleep(100, QAPI_TIMER_UNIT_MSEC, true);
    } while(1);

    return ret_val;
}



static int netctrl_start(void)
{
    int rc = 0;

    if (0 == netctrl_init())
    {
        /* Get valid DSS handler and set the data call parameter */
        #ifdef ENABLE_MQTT_IPV4
        netctrl_SetPDPParams(QAPI_DSS_IP_VERSION_4);
        #endif

        #ifdef ENABLE_MQTT_IPV6
        netctrl_SetPDPParams(QAPI_DSS_IP_VERSION_6);
        #endif

        gina_uwlog_printf("qapi_DSS_Start_Data_Call start");
        if (QAPI_OK == qapi_DSS_Start_Data_Call(netctrl_dss_handle))
        {
            gina_uwlog_printf("Start Data service success");
        }
        else
        {
            rc = -1;
        }
    }
    else
    {
        gina_uwlog_printf("dss_init fail.");
        rc = -1;
    }
    return rc;
}



static int netctrl_stop(void)
{
    qapi_Status_t status;

    if (netctrl_dss_handle)
    {
        status = qapi_DSS_Stop_Data_Call(netctrl_dss_handle);
        gina_uwlog_printf("Stop data call, rc=%d", status);

        status = qapi_DSS_Rel_Data_Srvc_Hndl(netctrl_dss_handle);
        gina_uwlog_printf("Release dss handle, rc=%d", status);
    }

    status = qapi_DSS_Release(QAPI_DSS_MODE_GENERAL);
    gina_uwlog_printf("netctrl_stop: qapi_DSS_Release(), rc=%d", status);
    return 0;
}

#ifdef ENABLE_SSL_MQTT

void *util_data_malloc(uint32_t size)
{
  void *data = NULL;
  uint32_t status;

  if (0 == size) {
    return NULL;
  }

  status = tx_byte_allocate(byte_pool_test, (VOID **)&data, size, TX_NO_WAIT);

  if (TX_SUCCESS != status) {
    GINA_UWLOG_INFO("Failed to allocate memory with %d", status);
    return NULL;
  }
  return data;
}

void util_data_free(void *data)
{
  uint32_t status = 0;

  if(NULL == data) {
    return;
  }
  status = tx_byte_release(data);

  if (TX_SUCCESS != status) {
    GINA_UWLOG_INFO("Failed to release memory with %d", status);
  }
  data = NULL;
}


/*=============================================================================

FUNCTION util_certstore_load

DESCRIPTION
  This utility function is to convert the certificate buffer
  to file in security place

DEPENDENCIES
  None.

RETURN VALUE
  QAPI_OK: convert/store success
  others:  convert/store failure

SIDE EFFECTS
  None
==============================================================================*/
qapi_Status_t util_certstore_load
(
  qapi_Net_SSL_Cert_Type_t cert_type,
  const char*              security_cert_name,
  uint8_t*                 cert_buf,
  uint32_t                 cert_buf_size,
  uint8_t*                 key_buf,
  uint32_t                 key_buf_size
)
{
  int to_alloc;
  qapi_Status_t ret = QAPI_OK;
  qapi_Net_SSL_Cert_Info_t cert_info;

  if(cert_buf == NULL || cert_buf_size == 0)
  {
    return QAPI_ERR_INVALID_PARAM;
  }

  /* Key need provided when for normal certifiate  */
  if(cert_type == QAPI_NET_SSL_CERTIFICATE_E &&
        (key_buf == NULL || key_buf_size == 0))
  {
    return QAPI_ERR_INVALID_PARAM;
  }

  memset(&cert_info, 0, sizeof(qapi_Net_SSL_Cert_Info_t));
  cert_info.cert_Type = cert_type;

  switch(cert_info.cert_Type)
  {
    case QAPI_NET_SSL_CA_LIST_E:

	  gina_uwlog_printf("%s:%d) QAPI_NET_SSL_CA_LIST_E", __func__, __LINE__);
      cert_info.info.ca_List.ca_Cnt = 1;
      to_alloc=sizeof(qapi_NET_SSL_CA_Info_t) * cert_info.info.ca_List.ca_Cnt;
      cert_info.info.ca_List.ca_Info[0] = util_data_malloc(to_alloc);
      if(cert_info.info.ca_List.ca_Info[0] == NULL) {
        ret = QAPI_ERROR;
      }
      else {
      cert_info.info.ca_List.ca_Info[0]->ca_Size = cert_buf_size;
      cert_info.info.ca_List.ca_Info[0]->ca_Buf  = cert_buf;
      }
      break;

    case QAPI_NET_SSL_CERTIFICATE_E:
	  gina_uwlog_printf("%s:%d) QAPI_NET_SSL_CERTIFICATE_E", __func__, __LINE__);
      cert_info.info.cert.cert_Size = cert_buf_size;
      cert_info.info.cert.cert_Buf  = cert_buf;
      cert_info.info.cert.key_Size  = key_buf_size;
      cert_info.info.cert.key_Buf   = key_buf;
      break;

    case QAPI_NET_SSL_DI_CERT_E:
	  gina_uwlog_printf("%s:%d) QAPI_NET_SSL_DI_CERT_E", __func__, __LINE__);
      cert_info.info.di_cert.di_Cert_Size = cert_buf_size;
      cert_info.info.di_cert.di_Cert_Buf  = cert_buf;
      break;

    case QAPI_NET_SSL_PSK_TABLE_E:
	  gina_uwlog_printf("%s:%d) QAPI_NET_SSL_PSK_TABLE_E", __func__, __LINE__);
      cert_info.info.psk_Tbl.psk_Size = cert_buf_size;
      cert_info.info.psk_Tbl.psk_Buf  = cert_buf;
      break;

    default:
	  gina_uwlog_printf("%s:%d) Unknown certificate type(%d)", __func__, __LINE__, cert_info.cert_Type);
      GINA_UWLOG_ERROR("Unknown certificate type(%d)", cert_info.cert_Type);
      ret = QAPI_ERROR;
      break;
  }

  if (ret == QAPI_OK) {
    ret = qapi_Net_SSL_Cert_Convert_And_Store(&cert_info, security_cert_name);
    GINA_UWLOG_INFO("convert/store for %s rc=%d", security_cert_name, ret);
    gina_uwlog_printf("convert/store for %s rc=%d", security_cert_name, ret);
  }

  if(cert_info.cert_Type == QAPI_NET_SSL_CA_LIST_E)
  {
    int i;
    for (i=0; i<cert_info.info.ca_List.ca_Cnt; i++) {
      if(cert_info.info.ca_List.ca_Info[i] != NULL) {
        util_data_free(cert_info.info.ca_List.ca_Info[i]);
      }
    }
  }

  return ret;
}


static int util_ReadFileToBuf(char *CertName, uint8 **Buf, size_t *BufSize)
{
  int fread=-1, fd = -1, rc=-1;
  struct qapi_FS_Stat_Type_s stat;
  stat.st_size = 0;

  do
  {
    if (!CertName || !Buf || !BufSize) {
        GINA_UWLOG_ERROR("%s: param error %p,%p,%p", __func__, CertName, Buf, BufSize);
        break;
    }

    if (qapi_FS_Open(CertName, QAPI_FS_O_RDONLY_E, &fd) < 0) {
        GINA_UWLOG_ERROR("could not open file=%s", CertName);
        break;
    }

    if (qapi_FS_Stat_With_Handle(fd, &stat) < 0) {
        GINA_UWLOG_ERROR("could not stat file=%s", CertName);
        break;
    }

    *Buf = (uint8 *) util_data_malloc(stat.st_size);
    if (*Buf == NULL) {
        GINA_UWLOG_ERROR("could not alloc %lu bytes for file=%s", stat.st_size, CertName);
        break;
    }

    qapi_FS_Read(fd, *Buf, stat.st_size, &fread);
    if (fread != stat.st_size) {
        util_data_free(*Buf);
        *Buf=NULL;
        GINA_UWLOG_ERROR("could not read %lu bytes from file=%s", stat.st_size, CertName);
        break;
    }

    *BufSize = stat.st_size;
    GINA_UWLOG_INFO("read %lu bytes from file=%s", stat.st_size, CertName);
    rc = 0;
  }
  while (0);

  if (fd != -1) {
    qapi_FS_Close(fd);
  }
  return rc;
}


/**
    Loading a certificate in PEM format from the generic file system
    to the secured certificate store (certstore)

    For this demo, this loading is always performed. For a real
    product, only load to the certstore when a certificate really
    needs an update (flash lifetime will be decreased with too much
    write access)
*/
static int util_CopyCertoToCertBuf
(
    qapi_Net_SSL_Cert_Type_t Type,
    char *CertName,
    char *KeyName,
    char *CertStoreName
)
{
  int rc=-1;
  uint8 *KeyBuf=NULL, *CertBuf=NULL;
  size_t KeyBufSize=-1, CertBufSize=-1;

  do
  {
    if (!CertName || !CertStoreName)
      break;

    int ret=util_ReadFileToBuf(CertName, &CertBuf, &CertBufSize);
    if (ret)
      break;

    if (KeyName != NULL) {
      ret=util_ReadFileToBuf(KeyName, &KeyBuf, &KeyBufSize);
      if (ret)
        break;
    }

    /* convert RootCA/certificate/PSK from blob to security place */
    if (QAPI_OK != util_certstore_load(Type, CertStoreName,
                                        CertBuf, CertBufSize,
                                        KeyBuf, KeyBufSize)) {
      break;
    }

    rc=0;
  }
  while (0);

  if (CertBuf)
    util_data_free(CertBuf);
  if (KeyBuf)
    util_data_free(KeyBuf);

  GINA_UWLOG_INFO("%s: Type=%d CertStoreName=%s file=%s key=%s rc=%d",
                  __func__,
                  Type,
                  CertStoreName?CertStoreName:"<empty>",
                  CertName?CertName:"<empty>",
                  KeyName?KeyName:"<empty>",
                  rc);
  return rc;
}

#endif

void dam_mqtt_config()
{
    char buff[32];
    static uint32_t port_client = 1234;
    struct sockaddr_in *sin4;
    struct sockaddr_in6 *sin6;

    uint32_t addrNum;

    uint32 retain = false;
    uint16 keepalive_duration = 60;
    uint16 connack_timeout = 120;
    boolean clean_session       = true;
    boolean non_blocking        = false;
    char  client_id[]           = "mqtt_client1";
    uint8 client_id_len         = strlen(client_id);
    uint8 will_qos = 0;

    gina_uwlog_printf("%s: start", __func__);

    memset(&_mqtt_config, 0, sizeof(_mqtt_config));

#ifdef ENABLE_MQTT_IPV4
    sin4 = (struct sockaddr_in *)&_mqtt_config.local;
    sin4->sin_family = AF_INET;
    sin4->sin_port = (uint16)_htons(port_client);
    inet_pton(AF_INET, "0.0.0.0", &(addrNum));
    sin4->sin_addr.s_addr = addrNum;

    sin4 = (struct sockaddr_in *)&_mqtt_config.remote;
    sin4->sin_family = AF_INET;

#ifdef ENABLE_SSL_MQTT
    sin4->sin_addr.s_addr = inet_addr(mqtts_server_ip);
    sin4->sin_port = _htons(CLI_MQTT_SECURE_PORT);
#else
    sin4->sin_addr.s_addr = inet_addr(mqtt_server_ip);
    sin4->sin_port = _htons(CLI_MQTT_PORT);
#endif /* ENABLE_SSL_MQTT */
    gina_uwlog_printf("MQTT Server IP: %s Port: %d", inet_ntop(AF_INET, &sin4->sin_addr.s_addr, buff, sizeof(buff)), _ntohs(sin4->sin_port));
#endif /* ENABLE_MQTT_IPV4 */

#ifdef ENABLE_MQTT_IPV6
    sin6 = (struct sockaddr_in6 *)&_mqtt_config.local;
    sin6->sin_family = AF_INET6;
    sin6->sin_port = (uint16)_htons(port_client);
    inet_pton(AF_INET6, "::", &sin6->sin_addr);

    sin6 = (struct sockaddr_in6 *)&_mqtt_config.remote;
    sin6->sin_family = AF_INET6;

#ifdef ENABLE_SSL_MQTT
    inet_pton(AF_INET6, mqtts_server_ip, &sin6->sin_addr);
    sin6->sin_port = _htons(CLI_MQTT_SECURE_PORT);
#else
    inet_pton(AF_INET6, mqtt_server_ip, &sin6->sin_addr);
    sin6->sin_port = _htons(CLI_MQTT_PORT);
#endif /* ENABLE_SSL_MQTT */
    gina_uwlog_printf("MQTT Server IP: %s Port: %d", inet_ntop(AF_INET6, &sin6->sin_addr.s_addr, buff, sizeof(buff)), _ntohs(sin6->sin_port));
#endif /* ENABLE_MQTT_IPV6 */

    strncpy((char *)_mqtt_config.client_id, (char *)client_id, QAPI_NET_MQTT_MAX_CLIENT_ID_LEN);
    _mqtt_config.client_id_len        = client_id_len;
    _mqtt_config.nonblocking_connect  = non_blocking;
    _mqtt_config.keepalive_duration   = keepalive_duration;
    _mqtt_config.clean_session        = clean_session;
    _mqtt_config.connack_timed_out_sec= connack_timeout;

#ifdef ENABLE_MQTT_WILL
    _mqtt_config.will_retained        = retain;
    _mqtt_config.will_qos             = will_qos;
    _mqtt_config.will_topic           = (uint8_t*)will_topic_ptr;
    _mqtt_config.will_topic_len       = strlen(will_topic_ptr);
    _mqtt_config.will_message         = (uint8_t*)will_message_ptr;
    _mqtt_config.will_message_len     = strlen(will_message_ptr);
#else
    _mqtt_config.will_retained        = retain;		//false`
    _mqtt_config.will_qos             = will_qos;   // =0
    _mqtt_config.will_topic           = NULL;
    _mqtt_config.will_topic_len       = 0;
    _mqtt_config.will_message         = NULL;
    _mqtt_config.will_message_len     = 0;
#endif /* ENABLE_MQTT_WILL */

	_mqtt_config.username             = (uint8_t*)username_ptr;
    _mqtt_config.username_len         = strlen(username_ptr);
    _mqtt_config.password             = (uint8_t*)password_ptr;
    _mqtt_config.password_len         = strlen(password_ptr);

    gina_uwlog_printf("MQTT Configuration: client id [%s], client id len=%d", _mqtt_config.client_id, _mqtt_config.client_id_len);
    gina_uwlog_printf("MQTT Configuration: username [%s], username len = %d", _mqtt_config.username, _mqtt_config.username_len);
    gina_uwlog_printf("MQTT Configuration: password [%s], password len = %d", _mqtt_config.password, _mqtt_config.password_len);
    gina_uwlog_printf("MQTT Configuration: will topic [%s], topic len = %d", _mqtt_config.will_topic, _mqtt_config.will_topic_len);
    gina_uwlog_printf("MQTT Configuration: connack timeout_sec = %d, keepalive duration = %d", _mqtt_config.connack_timed_out_sec, _mqtt_config.keepalive_duration);

#ifdef ENABLE_SSL_MQTT
    gina_uwlog_printf("MQTT SSL is enabled");
    _mqtt_config.ca_list = ROOT_CA_File;
    _mqtt_config.cert = CERT_FILE;

    /** protocol set to TLS 1.2 */
    _mqtt_config.ssl_cfg.protocol  = QAPI_NET_SSL_PROTOCOL_TLS_1_2;

    /** cipher suites */
    /**
    ###########################################################
    ### please remember QAPI_NET_SSL_CIPHERSUITE_LIST_DEPTH ###
    ### and activate not more than this, as this demo code  ###
    ### does not care for it                                ###
    ###########################################################
    */
	_mqtt_config.ssl_cfg.cipher[0] = QAPI_NET_TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384;
    _mqtt_config.ssl_cfg.cipher[1] = QAPI_NET_TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384;
    _mqtt_config.ssl_cfg.cipher[2] = QAPI_NET_TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256;
    _mqtt_config.ssl_cfg.cipher[3] = QAPI_NET_TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256;
    _mqtt_config.ssl_cfg.cipher[4] = QAPI_NET_TLS_ECDH_RSA_WITH_AES_256_CBC_SHA;
    _mqtt_config.ssl_cfg.cipher[5] = QAPI_NET_TLS_ECDH_RSA_WITH_AES_128_CBC_SHA;
    _mqtt_config.ssl_cfg.cipher[6] = QAPI_NET_TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384;
    _mqtt_config.ssl_cfg.cipher[7] = QAPI_NET_TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384;

	/* Fragment value will be taken only in 512, 1024, 2048, 4096 */
	_mqtt_config.ssl_cfg.max_Frag_Len = 4096;
	_mqtt_config.ssl_cfg.max_Frag_Len_Neg_Disable = 0;

      /** validate time for this demo*/
    _mqtt_config.ssl_cfg.verify.time_Validity = 0;

    /** alert */
    _mqtt_config.ssl_cfg.verify.send_Alert = 0;

    /** no domain */
    _mqtt_config.ssl_cfg.verify.domain = 0;
    _mqtt_config.ssl_cfg.verify.match_Name[0] = '\0';

    /** no sni */
    _mqtt_config.ssl_cfg.sni_Name_Size = 0;
    _mqtt_config.ssl_cfg.sni_Name = NULL;





    /*Prepare the CA File and CERT file*/
    (void) util_CopyCertoToCertBuf(QAPI_NET_SSL_CA_LIST_E,
            "/demo_rootca.pem",
            NULL,
            ROOT_CA_File);

    (void) util_CopyCertoToCertBuf(QAPI_NET_SSL_CERTIFICATE_E,
            "/demo_certificate.pem",
            "/demo_certificate_key.pem",
            CERT_FILE);
#endif /* ENABLE_SSL_MQTT */
}


int dam_mqtt_subscribe()
{
    int ret = QAPI_ERROR;

    char sub_topic[] = "mqtttest";
    int sub_qos = 1;

    gina_uwlog_printf("%s: start", __func__);

    if(NULL == _mqtt_handle)
    {
      gina_uwlog_printf("No MQTT Connection, Please do MQTT connection first \n");
      return QAPI_ERROR;
    }

    ret = qapi_Net_MQTT_Subscribe(_mqtt_handle, (uint8 *)sub_topic, sub_qos);

    gina_uwlog_printf("Topic Subscribe %s", ret?"Failed":"Success");
    return ret;
}


int dam_mqtt_publish(unsigned int count)
{
    char msg[50];
    int ret = QAPI_ERROR;

    gina_uwlog_printf("%s: start", __func__);

    char topic[] = "counter";
    sprintf(msg, "publish count = %d", count);
    int qos = 2;
    int retain = 0;

    if(NULL == _mqtt_handle)
    {
      gina_uwlog_printf("No MQTT Connection, Please do MQTT connection first \n");
      return QAPI_ERROR;
    }

    ret = qapi_Net_MQTT_Publish(_mqtt_handle, (uint8 *)topic, (uint8 *)msg, strlen(msg), qos, retain);

    gina_uwlog_printf("Topic Publish on %s is %s", topic,ret?"Failed":"Success");
    return ret;
}


int dam_mqtt_unsubscribe()
{
    int ret = QAPI_ERROR;
    char unsub_topic[] = "mqtttest";

    gina_uwlog_printf("%s: start", __func__);

    if(NULL == _mqtt_handle)
    {
      gina_uwlog_printf("No MQTT Connection, Please do MQTT connection first \n");
      return QAPI_ERROR;
    }

    ret = qapi_Net_MQTT_Unsubscribe(_mqtt_handle, (uint8 *)unsub_topic);

    gina_uwlog_printf("Topic Unubscribe %s", ret?"Failed":"Success");
    return ret;
}



int dam_mqtt_disconnect()
{
    int ret = QAPI_ERROR;

    gina_uwlog_printf("%s: start", __func__);
    ret = qapi_Net_MQTT_Disconnect(_mqtt_handle);
    if(ret)
    {
      gina_uwlog_printf("MQTT Disconnect Failed, Error type %d", ret);
    }
    else
    {
      gina_uwlog_printf("Disconnect Successfull");
    }

    return ret;
}


int dam_mqtt_destory()
{
    int ret = QAPI_ERROR;

    gina_uwlog_printf("%s: start", __func__);

    if(NULL == _mqtt_handle)
    {
      gina_uwlog_printf("No MQTT Connection, Please do MQTT connection first \n");
      return QAPI_ERROR;
    }

    ret = qapi_Net_MQTT_Destroy(_mqtt_handle);

    gina_uwlog_printf("MQTT Destroy %s", ret?"Failed":"Success");
    return ret;
}


void dam_mqtt_connect_callback(qapi_Net_MQTT_Hndl_t mqtt, int32 reason)
{
    switch (reason) {
        case QAPI_NET_MQTT_CONNECT_SUCCEEDED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_SUCCEEDED_E: MQTT connect succeed");
            dam_mqtt_subscribe();
            return;
        case QAPI_NET_MQTT_TCP_CONNECT_FAILED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_TCP_CONNECT_FAILED_E: TCP connect failed");
            break;
        case QAPI_NET_MQTT_SSL_CONNECT_FAILED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_SSL_CONNECT_FAILED_E: SSL connect failed");
            break;
        case QAPI_NET_MQTT_CONNECT_FAILED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_FAILED_E: MQTT connect failed");
            break;
        case QAPI_NET_MQTT_CONNECT_PROTOCOL_VER_ERROR_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_PROTOCOL_VER_ERROR_E: Connection refused -unacceptable");
            break;
        case QAPI_NET_MQTT_CONNECT_IDENTIFIER_ERROR_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_IDENTIFIER_ERROR_E: Connection refused - identifier rejected");
            break;
        case QAPI_NET_MQTT_CONNECT_SERVER_UNAVAILABLE_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_SERVER_UNAVAILABLE_E: Connection refused - server unavailable");
            break;
        case QAPI_NET_MQTT_CONNECT_MALFORMED_DATA_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_MALFORMED_DATA_E: connection refused - data in username or password malformed");
            break;
        case QAPI_NET_MQTT_CONNECT_UNAUTHORIZED_CLIENT_E:
            gina_uwlog_printf("QAPI_NET_MQTT_CONNECT_UNAUTHORIZED_CLIENT_E: connection refused - unauthorized client");
            break;
    }
    dam_mqtt_disconnect();
}



void dam_mqtt_subscribe_callback(qapi_Net_MQTT_Hndl_t mqtt,
                                 int32 reason,
                                 const uint8 *topic,
                                 int32 topic_length,
                                 int32 qos,
                                 const void *sid)
{
    switch (reason) {
        case QAPI_NET_MQTT_SUBSCRIBE_GRANTED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_SUBSCRIBE_GRANTED_E: Subscribe Callback: Granted");
            break;
        case QAPI_NET_MQTT_SUBSCRIBE_DENIED_E:
            gina_uwlog_printf("QAPI_NET_MQTT_SUBSCRIBE_DENIED_E: Subscribe Callback: Denied");
            break;
        case QAPI_NET_MQTT_SUBSCRIBE_MSG_E:
            gina_uwlog_printf("QAPI_NET_MQTT_SUBSCRIBE_MSG_E: Subscribe Callback: Message Received From Broker");
            break;
    }
}

void dam_mqtt_publish_callback(qapi_Net_MQTT_Hndl_t mqtt,
                                          enum QAPI_NET_MQTT_MSG_TYPES msgtype,
                                          int qos,
                                          uint16_t msg_id)
{
    gina_uwlog_printf("Publish Callback: msg_type=%d, qos=%d", msgtype, qos);
}


void dam_mqtt_message_callback(qapi_Net_MQTT_Hndl_t mqtt,
                               int32 reason,
                               const uint8 *topic,
                               int32 topic_length,
                               const uint8 *msg,
                               int32 msg_length,
                               int32 qos,
                               const void *sid)
{
    gina_uwlog_printf("Message Callback: qos=%d, topic=%s, msg=%s", qos, topic, msg);
}


int dam_mqtt_connect()
{
    int ret = QAPI_ERROR;
    int loop = 3;
	int connected = 0;

    gina_uwlog_printf("%s: start", __func__);

    qapi_Net_MQTT_Pass_Pool_Ptr (_mqtt_handle, byte_pool_test);

    ret = qapi_Net_MQTT_Set_Connect_Callback(_mqtt_handle, dam_mqtt_connect_callback);
    if (ret == QAPI_OK)
        gina_uwlog_printf("registered connect callback success");
    else
        gina_uwlog_printf("registered connect callback FAILED");
    qapi_Net_MQTT_Set_Subscribe_Callback(_mqtt_handle, dam_mqtt_subscribe_callback);
    if (ret == QAPI_OK)
        gina_uwlog_printf("registered subscribe callback success");
    else
        gina_uwlog_printf("registered subscribecallback FAILED");
    qapi_Net_MQTT_Set_Publish_Callback(_mqtt_handle, dam_mqtt_publish_callback);
    if (ret == QAPI_OK)
        gina_uwlog_printf("registered publish callback success");
    else
        gina_uwlog_printf("registered publish callback FAILED");

    qapi_Net_MQTT_Set_Message_Callback(_mqtt_handle, dam_mqtt_message_callback);
    if (ret == QAPI_OK)
        gina_uwlog_printf("registered message callback success");
    else
        gina_uwlog_printf("registered message callback FAILED");

    do{
        gina_uwlog_printf("%s: Connecting...", __func__);
        qapi_Timer_Sleep(5, QAPI_TIMER_UNIT_SEC, true); //Try Every 5 Seconds
        ret = qapi_Net_MQTT_Connect(_mqtt_handle, &_mqtt_config);
        if(ret != QAPI_OK)
        {
          gina_uwlog_printf("MQTT Connect Failed, Error type %d", ret);

          switch (ret) {
              case QAPI_NET_MQTT_ERR_TCP_BIND_FAILED:
                gina_uwlog_printf("QAPI_NET_MQTT_ERR_TCP_BIND_FAILED", ret);
                break;
              case QAPI_NET_MQTT_ERR_SSL_CONN_FAILURE:
                gina_uwlog_printf("QAPI_NET_MQTT_ERR_TCP_BIND_FAILED", ret);
                break;
              default:
                gina_uwlog_printf("DEFAULT: MQTT Connect Failed, Error type %d", ret);
                break;
          }
        }
        else
        {
          gina_uwlog_printf("MQTT Connected, rc= %d", ret);
		  connected = 1;
          break;
        }
        loop--;
    }while(loop);

	if (connected == 0)
		gina_uwlog_printf("MQTT IS NOT CONNECTED, rc= %d", ret);
	else
		gina_uwlog_printf("MQTT IS CONNECTED!!!!, rc= %d", ret);


    gina_uwlog_printf("%s: connect request sent", __func__);

    return ret;
}


int test_dam_mqtt_sample()
{
    int ret = QAPI_ERROR;
    unsigned int publish_count = 0;

    gina_uwlog_printf("%s: start", __func__);

    ret = qapi_Net_MQTT_New(&_mqtt_handle);

    if(ret != QAPI_OK)
    {
      gina_uwlog_printf("MQTT New Failed, Error type %d", ret);
      return ret;
    }
    else
    {
      gina_uwlog_printf("MQTT New Successfull, mqtt handle %p", _mqtt_handle);
    }

    dam_mqtt_config();

    dam_mqtt_connect();

    //dam_mqtt_subscribe();
    while (publish_count < 100){
        qapi_Timer_Sleep(10, QAPI_TIMER_UNIT_SEC, true);
        gina_uwlog_printf("Count = %d", publish_count);
        dam_mqtt_publish(publish_count);
        publish_count++;
    }

    dam_mqtt_unsubscribe();

    dam_mqtt_disconnect();

    dam_mqtt_destory();

    gina_uwlog_printf("%s: finish", __func__);
    return ret;
}


int _dam_main(void)
{
    ULONG dss_event = 0;
    int32 sig_mask;
    ULONG status;
    int ret = QAPI_ERROR;

    gina_init();

    qapi_Timer_Sleep(10, QAPI_TIMER_UNIT_SEC, true);
    /* register the cleanup function */
    gina_uwmod_set_cleanup(cleanup, "MY CLEANUP");

    gina_uwlog_printf("MQTT Demo Start");

    dam_byte_pool_init();

    /* Create event signal handle and clear signals */
    status = txm_module_object_allocate(&main_signal_hndl,
                                        sizeof(TX_EVENT_FLAGS_GROUP));
    gina_uwlog_printf("allocate event rc=%d", status);
    if (TX_SUCCESS != status)
        return -1;

    status = tx_event_flags_create(main_signal_hndl, "main_signal_hndl");
    gina_uwlog_printf("create event rc=%d", status);
    if (TX_SUCCESS != status)
        return -1;

    netctrl_start();

    sig_mask =  NETCTRL_SIG_EVT_INVALID_EVENT |
                NETCTRL_SIG_EVT_NOCONN_EVENT |
                NETCTRL_SIG_EVT_CONNECTED_EVENT |
                NETCTRL_SIG_EVT_EXIT_EVENT;

    while (1)
    {
        qapi_Timer_Sleep(5, QAPI_TIMER_UNIT_SEC, true);
        /* main signal process */
        gina_uwlog_printf("MAIN EVENT WAIT FOR 0x%x", sig_mask);
        status = tx_event_flags_get(main_signal_hndl, sig_mask, TX_OR,
                                    &dss_event, TX_WAIT_FOREVER);
        gina_uwlog_printf("SIGNAL EVENT IS [0x%x] status=%d",
                          dss_event, status);

        if (dss_event & NETCTRL_SIG_EVT_INVALID_EVENT) {
            gina_uwlog_printf("NETCTRL_SIG_EVT_INVALID_EVENT Signal");
            break;
        }
        if (dss_event & NETCTRL_SIG_EVT_NOCONN_EVENT) {
            gina_uwlog_printf("NETCTRL_SIG_EVT_NOCONN_EVENT Signal");
            break;
        }
        if (dss_event & NETCTRL_SIG_EVT_CONNECTED_EVENT) {
            gina_uwlog_printf("NETCTRL_SIG_EVT_CONNECTED_EVENT Signal");
            /* run the mqtt test */
            ret = test_dam_mqtt_sample();
            break;
        }
        if (dss_event & NETCTRL_SIG_EVT_EXIT_EVENT) {
            gina_uwlog_printf("NETCTRL_SIG_EVT_EXIT_EVENT Signal");
            break;
        }
    }

    qapi_Timer_Sleep(10, QAPI_TIMER_UNIT_SEC, true);

    netctrl_stop();

    gina_uwlog_printf("MQTT Demo Finish And %s", ret?"Failed":"Success");

    return 0;
}
