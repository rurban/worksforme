/*
 * Description:
 *      This is a deadline-based calendar where it shows
 *      how many days are left until the deadline.
 *      I like this kind because is better to visualize
 *      how much time I still have.
 *
 *      It reads a file that stores the deadlines.
 *      So, you keep that file updated with your
 *      appointments and stuff in the format:
 *      YYYY-MM-DD # {string with what you want}
 *
 *      Then you call the program passing the file name.
 *
 *      That is it, it will generate a calendar
 *      based on how many days still left to the deadline.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct item {
    char *ptr;
    int year;
    int month;
    int day;
    void *next;
} item_t;

typedef struct list {
    item_t *head, *tail;
    int size;
} list_t;

int print_row = 0;
void print_bar_size(int year, int month, int day, char *ptr);

    int main(int argc, char **argv) {

    char buffer[500];
    char four[5];
    char bmonth[3], bday[3];
    char *it;
    int offset;
    list_t list = { .head = NULL, .tail = NULL, .size = 0 };
    item_t *dummy;
    int ryear, rmonth, rday;

    if(argc != 2) {
        printf("Please pass the filepath\n");
        exit(1);
    }

    FILE *fp;
    fp = fopen(argv[1], "a+");
    if(!fp) {
        printf("Could not open the file\n");
        exit(1);
    }

    while(fgets(buffer, 500, (FILE*) fp)) {
        offset = strlen(buffer);
        if(offset < 12)
            continue;

        four[0] = buffer[0];
        four[1] = buffer[1];
        four[2] = buffer[2];
        four[3] = buffer[3];
        four[4] = 0;
        ryear = atoi(four);

        it = buffer;

        bmonth[0] = buffer[5];
        bmonth[1] = buffer[6];
        bmonth[2] = 0;
        rmonth = atoi(bmonth);

        bday[0] = buffer[8];
        bday[1] = buffer[9];
        bday[2] = 0;
        rday = atoi(bday);

        it = &buffer[12];

        item_t *tmp = malloc(sizeof(item_t));
        tmp->year = ryear;
        tmp->month = rmonth;
        tmp->day = rday;
        tmp->ptr = calloc((strlen(it)+1), sizeof(char));
        strncpy(tmp->ptr, it, strlen(it));

        if(list.head == NULL) {
            list.head = list.tail = tmp;
        } else {
            list.tail->next = tmp;
            list.tail = tmp;
        }
        list.size++;

        print_bar_size(tmp->year, tmp->month, tmp->day, tmp->ptr);
    }
    fclose(fp);


    return 0;
}

void print_bar_size(int year, int month, int day, char *ptr) {

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int i = 0, rem_days = 0, rem_days_year = 0;

    int month_days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, };

    if(month < tm.tm_mon + 1) {
        return;
    }

    for(i = tm.tm_mon + 1; i <= month; i++) {
        rem_days  += month_days[i];
    }
    rem_days = rem_days - (month_days[month] - day + tm.tm_mday);

    if(rem_days < 0) {
        printf("!Deadline missed!\n");
    }

    for(i = tm.tm_mon + 1; i <= 12; i++) {
        rem_days_year += month_days[i];
    }
    rem_days_year -= tm.tm_mday;

    if(print_row) {
        print_row = 0;
        for(i = 0; i < rem_days_year; i++)
            printf(".__");

        printf(".\n");

        for(i = 0; i < rem_days_year; i++)
            printf("|%2d", rem_days_year - i);
        printf("|\n\n");
    }

    // Here is a simple sched printed
    printf("[%d/%d/%d] (%2d) left for => %s\n", year, month, day, rem_days, ptr);

    return;

    // Here is a complete table, there are bugs, though
    for(i = 0; i <= rem_days; i++) {
        if(i == 0)
            printf(" %2d", rem_days);
        if(i != 0 && i != rem_days)
            printf("---");
        if(i == rem_days) {
            printf("-##");
            break;
        }
    }
    printf("[%d/%d/%d] %s\n", year, month, day, ptr);
}
