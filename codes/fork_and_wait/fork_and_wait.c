#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {

    pid_t child_pid, wpid, my_pid;
    int status = 0;
    int i;

    my_pid = getpid();

    if((child_pid = fork()) == 0) {
        // in child process


        printf("My child pid = %d\n", getpid());
        char args[] = {""};
        char *ptr = args;
        execv("./bin", &ptr );


    } else if (child_pid > 0) {
        printf("My parent pid = %d\n", getpid());

        wpid = wait(&status);

        printf("Returned exit status %d\n", status);

    }

    return 0;
}
