#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

enum {
	BIT_a = 1 << 0,
	BIT_b = 1 << 1,
	BIT_c = 1 << 2,
};
int main(int argc, char **argv)
{
	unsigned char bits = 0;
	int i;

	bits = (BIT_a | BIT_b | BIT_c);

	for (i = 7; i >= 0; i--) {
		printf("%d", bits & (1 << i) ? 1 : 0);
	}
	printf("\n");
	bits &= ~BIT_b;

	for (i = 7; i >= 0; i--) {
		printf("%d", bits & (1 << i) ? 1 : 0);
	}
	return 0;
}
