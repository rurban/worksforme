#include <stdio.h>

int main(int argc, char **argv) {
    int new_val, reg_val, old_val, read;

    printf("Register value = ");
    scanf("%d", &read);
    reg_val = read;
    printf("New value = ");
    scanf("%d", &read);

    new_val = read;
    do {
        old_val = reg_val;
        if(old_val >= new_val)
            break;
    } while(!__sync_bool_compare_and_swap(&reg_val, old_val, new_val));

    printf("new_val = %d, reg_val = %d\n", new_val, reg_val);

    return 0;
}
