#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/time.h>
#include <linux/timer.h>

static struct task_struct *thread1;

int thread_fn(void *args)
{
    unsigned long j0, j1;
    int delay = 60*HZ;
    j0 = jiffies;
    j1 = j0 + delay;

    printk(KERN_ERR "in thread1\n");

    while (time_before(jiffies, j1))
        schedule();

    return 0;
}

int thread_init(void)
{
    char our_thread[8] = "thread1";
    printk(KERN_ERR "in init\n");
    thread1 = kthread_create(thread_fn, NULL, our_thread);
    if (thread1) {
        printk(KERN_ERR "in if\n");
        wake_up_process(thread1);
    }
    return 0;
}

void thread_cleanup(void)
{
    int ret;
    ret = kthread_stop(thread1);
    if (!ret)
        printk(KERN_ERR "Thread stopped\n");
}
MODULE_LICENSE("GPL");
module_init(thread_init);
module_exit(thread_cleanup);
