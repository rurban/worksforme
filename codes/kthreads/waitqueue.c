#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/uaccess.h>

#ifndef __GFP_WAIT
#define __GFP_WAIT __GFP_RECLAIM
#endif

MODULE_LICENSE("GPL");
MODULE_AUTHOR("CASSIANO CAMPES");

#define ENTRY_NAME "counter"
#define PERMS 0644
#define PARENT NULL

static struct file_operations fops;
static struct task_struct *kthread;
static int counter;

static char* message;
static int read_p;

int counter_run(void *data) {
    while (!kthread_should_stop()) {
        ssleep(1);
        counter += 1;
    }
    printk(KERN_ERR "The counter thread has terminated\n");
    return counter;
}

int counter_proc_open(struct inode *sp_inode, struct file *sp_file) {
    printk(KERN_ERR "proc called open\n");

    message = kmalloc(sizeof(char)*30, __GFP_WAIT | __GFP_IO | __GFP_FS);
    if (message == NULL) {
        printk(KERN_ERR "[!] error counter_proc_open\n");
        return -ENOMEM;
    }
    sprintf(message, "The counter is now at %d\n", counter);
    return 0;
}

ssize_t counter_proc_read(struct file *sp_file, char __user *buf,
        size_t size, loff_t *offset) {
    int len = strlen(message);

    read_p = !read_p;
    if (read_p) {
        return 0;
    }

    printk(KERN_ERR "proc called read\n");
    copy_to_user(buf, message, len);
    return len;
}

int counter_proc_release(struct inode *sp_inode, struct file *sp_file) {
    printk(KERN_ERR "proc called release\n");
    kfree(message);
    return 0;
}

static int counter_init(void) {
    printk(KERN_ERR "/proc/%s create\n", ENTRY_NAME);

    kthread = kthread_run(counter_run, NULL, "counter");

    if (IS_ERR(kthread)) {
        printk(KERN_ERR "[!] error kthread_run\n");
        return PTR_ERR(kthread);
    }

    fops.open = counter_proc_open;
    fops.read = counter_proc_read;
    fops.release = counter_proc_release;

    if (!proc_create(ENTRY_NAME, PERMS, NULL, &fops)) {
        printk(KERN_ERR "[!] error proc_create\n");
        remove_proc_entry(ENTRY_NAME, NULL);
        return -ENOMEM;
    }
    return 0;
}

module_init(counter_init);

static void counter_exit(void) {
    int ret = kthread_stop(kthread);
    if (ret != -EINTR)
        printk(KERN_ERR "kthread stopped\n");
    remove_proc_entry(ENTRY_NAME, NULL);
    printk(KERN_ERR "Removing /proc/%s\n", ENTRY_NAME);
}
module_exit(counter_exit);
