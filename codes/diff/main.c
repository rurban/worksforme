#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char **argv)
{
	int32_t now, timestamp, diff;
#define MAX 100
	now = 10;
	timestamp = MAX - 20;

	int64_t q = INT64_MAX;
	if (now >= timestamp)
		diff = now - timestamp;
	else
		diff = (MAX - timestamp) + now;

	printf("Diff is = %d\n", diff);

	return 0;
}
