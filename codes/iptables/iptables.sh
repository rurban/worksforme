#!/bin/bash
#
# Autores: Cassiano Campes e Fábio Zummach
# Contato: cassianocampes@gmail.com; zummach@gmail.com
#
# Copyright (c) 2015, All Right Reserved.
#
# Este código visa realizar um trabalho técnico prático referente a disciplina
# de Sistemas Distribuídos e de tempo real (Còdigo disciplina: 070073) da Universidade
# do Vale do Rio dos Sinos - UNISINOS da turma do segundo semestre de 2015.
#
#######################################################################
#######################################################################
#######################################################################
# THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
# KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
# ACADEMIC PURPOSE.
#######################################################################
#######################################################################
#######################################################################
#
# Este script visa configurar as tabelas do IPTABLES de acordo com o cenário descrito
# na apresentação "NAT e FIREWALL NO LINUX"
# As tabelas e chains aqui utilizados são:
#
#       - FILTER TABLE (INPUT, FORWARD, OUTPUT)
#       - NAT TABLE (PREROUTING, POSTROUTING)
#       - MANGLE TABLE (POSTROUTING)
#
# Lembrando que os IP's aqui utilizados estão estáticos, pois foram os mesmos utilizados
# para descrever o cenário apresentado;
#
#######################################################################
 
FILE="dhcpd_status.conf"; # Este arquivo é criado para armazenar informações sobre a execução deste script
 
function FlushFilters()
{
  # Limpa todas as tabelas que serão utilizadas;
  iptables -t filter -F;
  iptables -t nat    -F;
  iptables -t mangle -F;
 
  # Zera os contadores das tabelas que serão utilizadas;
  iptables -t filter -Z;
  iptables -t nat    -Z;
  iptables -t mangle -Z;
 
  # Exclui chains definido pelo usuário;      
  iptables -t filter -X icmp-flood 2> /dev/null;
  iptables -t filter -X forward-filter 2> /dev/null;
 
  echo -e "All tables were flushed.\n";
}
 
function NATTableRules()
{
  ##############
  # PREROUTING #
  ##############
 
  # Os pacotes com destino ao SERVER:80 (HTTP) deverão ser traduzidos e transferidos para o HOST;
  iptables -t nat -A PREROUTING -p tcp -d 192.168.201.178 --dport 80 -j DNAT --to-destination 10.0.0.28;
 
  ###############
  # POSTROUTING #
  ###############
 
  # Os pacotes vindos da SUB_REDE com destino fora da rede devem ser traduzidos para o IP_SERVER;
  iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth1 -j SNAT --to 192.168.201.178;
 
  echo -e "NAT Table configured.\n";
}
 
function FILTERTableRules()
{
  #######################################################################
  # DEFAULT FILTER   ####################################################
  #######################################################################
 
  iptables -t filter -P INPUT DROP;
  iptables -t filter -P OUTPUT ACCEPT;
  iptables -t filter -P FORWARD DROP;
 
  ########################################################################
  #  INPUT           #####################################################
  ########################################################################
 
  # Interface LOOPBACK é PERMITIDO;
  iptables -t filter -A INPUT -i lo -j ACCEPT;
 
  # Chain "icmp-flood" definido pelo usuário;
  iptables -N icmp-flood 2> /dev/null;
 
  # Filtrando pacotes icmp, saltando para o chain definido pelo usuário "icmp-flood";
  iptables -t filter -A INPUT -p icmp -j icmp-flood;
 
  ##############################
  # SPECIAL CHAIN - ICMP-FLOOD #
  ##############################
                               
                                # Realiza a verificação do numero máximo de pkts ICMP (Echo),
                                # aceitando se estiver no limite (5/second);   
                                iptables -A icmp-flood -p icmp --icmp-type 8 -m limit --limit 5/second -j ACCEPT;
 
                                # Se ultrapassou o limite, logar no sistema em qual interface isso ocorreu;
                                iptables -A icmp-flood -i eth0 -j LOG --log-prefix "ICMP flood - internal";
                                iptables -A icmp-flood -i eth1 -j LOG --log-prefix "ICMP flood - external";
 
                                # Descartar os pacotes assim que forem logados;
                                iptables -A icmp-flood -j DROP;
       
        # Conexões relacionadas e já estabelecidas serão aceitas;
        iptables -t filter -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT;
 
        # Pacotes TCP com a flag SYN (início de conexão) com destino ao SERVER e com
        # porta destino 21 serão logadas no sistema, e aceitos;
        iptables -t filter -A INPUT -i eth0 -s 10.0.0.0/24 -p tcp --tcp-flags SYN SYN --dport 21 -j LOG --log-prefix "FTP Server ";
        iptables -t filter -A INPUT -i eth0 -s 10.0.0.0/24 -p tcp --dport 21 -j ACCEPT;
 
        # Pacotes TCP com a flag SYN (início de conn) com destino ao SERVER e com porta
        # destino 10776 serão logadas no sistema, e aceitos (serviço especalizado QuickSynergy);
        iptables -t filter -A INPUT -i eth0 -s 10.0.0.0/24 -p tcp --tcp-flags SYN SYN --dport 10776 -j LOG --log-prefix "Quicksynergy ";
        iptables -t filter -A INPUT -i eth0 -s 10.0.0.0/24 -p tcp --tcp-flags SYN SYN -j ACCEPT;
 
        #######################################################################
        # FORWARD #############################################################
        #######################################################################
       
        # Criação do chain definido pelo usuário "forward-filter";
        iptables -N forward-filter;
 
        # Verifica qual a direção dos pacotes, se de ETH0 -> ETH1 ou ETH1 <- ETH0, filtrando
        # pelas portas 80 (HTTP) e 443 (HTTPS), saltando para o chain do usuário;
        iptables -t filter -A FORWARD -i eth0 -o eth1 -p tcp -m multiport --dports 80,443 -j forward-filter;
        iptables -t filter -A FORWARD -i eth1 -o eth0 -p tcp -m multiport --sports 80,443 -j forward-filter;
 
                                ##################################
                                # SPECIAL CHAIN - FORWARD-FILTER #
                                ##################################
                               
                                # Filtra os pacotes por horário, caso esteja no horário, eles serão aceitos,
                                # senão, serão dropados pela política padrão;
                                iptables -t filter -A forward-filter -m time --timestart 09:00 --timestop 12:59 -j ACCEPT;
 
        # Conexões relacionadas e já estabelecidas serão aceitas;
        iptables -t filter -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT;
 
        # Pacotes com destino UDP porta 53 vindos no sentido ETH0 -> ETH1 deverão ser aceitos,
        # para resolução DNS da rede PARKS;
        iptables -t filter -A FORWARD -i eth0 -o eth1 -p udp --dport 53 -j ACCEPT;
 
        # Pacotes com destino TCP porta 21 vindos no sentido ETH0 -> ETH1 deverão ser aceitos,
        # para servidores externos de FTP;
        iptables -t filter -A FORWARD -i eth0 -o eth1 -p tcp --dport 21 -j ACCEPT;
 
        # Solicitações PING para hosts fora da rede devem ser aceitos;
        iptables -t filter -A FORWARD -i eth0 -o eth1 -p icmp --icmp-type 8 -j ACCEPT;
 
        #######################################################################
        # OUTPUT  #############################################################
        #######################################################################
       
        echo -e "FILTER Table configured.\n";
}
 
function MANGLETableRules()
{
        ####################
        #  DEFAULT MANGLE  #
        ####################
 
        iptables -t mangle -P PREROUTING ACCEPT;
        iptables -t mangle -P INPUT ACCEPT;
        iptables -t mangle -P FORWARD ACCEPT;
        iptables -t mangle -P OUTPUT ACCEPT;
        iptables -t mangle -P POSTROUTING ACCEPT;
 
        #######################################################################
        # POSTROUTING #########################################################
        #######################################################################
       
        # Marca os pacotes TCP com destino porta 21 como MAXIMO THROUGPUT, cabe aos roteadores
        # darem prioridade ou não a este serviço especializado;
        iptables -t mangle -A POSTROUTING -o eth1 -s 10.0.0.0/24 -p tcp --dport 21 -j DSCP --set-dscp 0x08;
       
        echo -e "MANGLE Table configured.\n";
}
 
# Chama as funções que inicializam as tabelas;
function StartAllFilters()
{
        NATTableRules;
        FILTERTableRules;
        MANGLETableRules;
}
 
# Função para tratar parâmetro START do console;
function StartFilters()
{
        echo "1" > /proc/sys/net/ipv4/ip_forward;
 
        if [ -s $FILE ]; then
                STATUS=`cat dhcpd_status.conf`;
               
                if [ $STATUS = 1 ]; then
                        echo -e "Service already running.\n";
                else
                        echo -e "Service not running.\n";
 
                        StartAllFilters;
 
                        `echo "1" > dhcpd_status.conf`;        
                       
                        echo -e "Service started.\n";  
                fi
        else
                `echo "1" > dhcpd_status.conf`;
       
                echo -e "Service not running.\n";
               
                StartAllFilters;
 
                echo -e "Service started.\n";
        fi
}
 
# Função para tratar parâmetro STOP do console;
function StopFilters()
{
        if [ -s $FILE ]; then
                STATUS=`cat dhcpd_status.conf`;
               
                if [ $STATUS = 1 ]; then
                        FlushFilters;
                        `echo "0" > dhcpd_status.conf`;
                        echo "Service stopped.";
                else
                        echo -e "Service already stopped\n";
                fi
        fi
}
 
# Função para tratar parâmetro STATUS do console;
function StatusFilter()
{
        if [ -s $FILE ]; then
                STATUS=`cat dhcpd_status.conf`;
               
                if [ $STATUS = 1 ]; then
 
                        echo -e "Service is running.\n";
                else
                        echo -e "Service is not running.\n";
                fi
        fi     
}
 
# Função para tratar parâmetro RESTART do console;
function RestartFilters()
{
        StopFilters;
        StartFilters;
       
        echo "Service restarted.";
}
 
if [ $# -lt 1 ]; then
        echo "No parameters, please use {start|stop|restart|status}";
 
else
        ROOT_UID=0
 
        if [ $UID != $ROOT_UID ]; then
                echo "You don't have sufficient privileges to run this script.";
                exit 1;
        else
                case "$1" in
                        "start")
                                StartFilters;;
 
                        "stop")
                                StopFilters;;
 
                        "restart")
                                RestartFilters;;
 
                        "status")
                                StatusFilter;;
                esac;
 
        fi
        exit 0;
fi
