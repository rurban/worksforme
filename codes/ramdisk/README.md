LL-SSD simulator using ramdisk
==============================

How to use /proc filesystem
---------------------------

The *rdlat.c* file is a C source code that explains how to use the */proc* file system.
It has its Makefile that generates the LKM (loadable kernel module), and the result
can be verified in */proc/rdlat*

Enabling RAM disk size
----------------------

Make sure to enable the RAM disk to use different sizes.
It is configured in the linux source code tree, by doing the commands

> cd  linux-4.4.32

> make menuconfig

> vim .config

Then, change the lines as you wish

Set RAM block number to 4, that is, we will find 4 ram devices in */dev/ram[0-3]*

> CONFIG_BLK_DEV_RAM_COUNT=4

Set RAM block size to 10GB

> CONFIG_BLK_DEV_RAM_SIZE=10485760

Compiling the brd module
------------------------

First patch the file 

After patching the *drivers/block/brd.c*, compile it.

> make drivers/block/brd.ko -j$(nproc)

Recompile the kernel and install it.

Compiling the example rdlat.c
-----------------------------

Just compile it using the makefile

> make

Then load the module

> insmod rdlat.ko

Source code
-----------

The *file_modif.c* is the original *drivers/block/brd.c* file with the necessary
modifications for the ramdisk.

The file *patch.diff* is the diff patch that can be applied directly to the kernel source code tree.

The *rdlat.c* is the mechanism that allows the userspace to control the delay in ramdisk.


References
----------
[1] - http://superuser.com/questions/255060/how-can-i-change-the-amount-and-size-of-linux-ramdisks-dev-ram0-dev-ram15
