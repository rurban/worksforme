/*************************************************************************************************/
/*
 * \brief Example of how to find a given character into the string, in this case, an white space
 *          or an '-'. When it is found, we want to manipulate the numbers that comes right
 *          after the token.
 */
/*************************************************************************************************/

#include <ctype.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

int main()
{
    char string[] = "string 123";
    char match[] = " -";
    char *token = NULL;
    int n = 0;
    int i = 0;
    uint32_t temp = 0;

    //if (sscanf(string, "%*s %" SCNu32, &temp) != 1) {
    if (sscanf(string, "%*s%" SCNu32 " %n", &temp, &n) != 1) {
        printf("No match in sscanf\n");
        return -1;
    }

    token = strpbrk(string, match);
    if (token == NULL) {
        return -1;
    }

    for (i = 0, ++token; token[i] != '\0'; i++) {
        if (!isdigit(token[i])) {
            return -1;
        }
        printf("%c", token[i]);
    }
    printf("\n");

    return 0;
}
