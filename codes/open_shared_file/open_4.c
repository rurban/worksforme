/**
 * This is a code example where threads access the shared
 * file to be written. The threads must acquire the mutex
 * before writing to it.
 *
 * This extends the previous one by using core affinity.
 *
 * @author: Cassiano Campes
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include <stdlib.h>

#define FILENAME ("/mnt/cassiano/my_file.txt") /* File to be opened */

FILE *fp;
pthread_t threads[4];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void * read_file(void* tmp)
{
    cpu_set_t mask;
    int i, ret;

    CPU_ZERO(&mask);
    if (pthread_self() == threads[0]) {
        CPU_SET(0, &mask);
        sched_setaffinity(0, sizeof(mask), &mask);
    } else if (pthread_self() == threads[1]) {
        CPU_SET(1, &mask);
        sched_setaffinity(0, sizeof(mask), &mask);
    } else if (pthread_self() == threads[2]) {
        CPU_SET(2, &mask);
        sched_setaffinity(0, sizeof(mask), &mask);
    } else {
        CPU_SET(3, &mask);
        sched_setaffinity(0, sizeof(mask), &mask);
    }
    for (i = 0; i < 300; i++) {
        if (pthread_self() == threads[0]) {
            pthread_mutex_lock(&mutex);
            ret = fputc('a', fp);
            pthread_mutex_unlock(&mutex);
        } else if (pthread_self() == threads[1]){
            pthread_mutex_lock(&mutex);
            ret = fputc('b', fp);
            pthread_mutex_unlock(&mutex);
	} else if (pthread_self() == threads[2]){
            pthread_mutex_lock(&mutex);
            ret = fputc('c', fp);
            pthread_mutex_unlock(&mutex);
	} else if (pthread_self() == threads[3]){
            pthread_mutex_lock(&mutex);
            ret = fputc('d', fp);
            pthread_mutex_unlock(&mutex);
        }
        if (ret < 0) {
            printf("Some error, closing\n");
            goto close;
        }
    }
    if(pthread_self() == threads[0]) {
        pthread_mutex_lock(&mutex);
        fputc('-', fp);
        pthread_mutex_unlock(&mutex);
    } else if(pthread_self() == threads[1]) {
        pthread_mutex_lock(&mutex);
        fputc('+', fp);
        pthread_mutex_unlock(&mutex);
    } else if(pthread_self() == threads[2]) {
        pthread_mutex_lock(&mutex);
        fputc('*', fp);
        pthread_mutex_unlock(&mutex);
    } else if(pthread_self() == threads[3]) {
        pthread_mutex_lock(&mutex);
        fputc('/', fp);
        pthread_mutex_unlock(&mutex);
    }
close:
    return NULL;
}

int main()
{
    fp = fopen(FILENAME, "a+");
    if (!fp) {
        printf("Could not open the file\n");
        return -1;
    }
    pthread_create(&threads[0], NULL, read_file, NULL);
    pthread_create(&threads[1], NULL, read_file, NULL);
    pthread_create(&threads[2], NULL, read_file, NULL);
    pthread_create(&threads[3], NULL, read_file, NULL);

    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);
    pthread_join(threads[2], NULL);
    pthread_join(threads[3], NULL);

    fclose(fp);
    return 0;
}

