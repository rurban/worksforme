/**
 * This is a code example where threads access the shared
 * file to be written. The threads must acquire the mutex
 * before writing to it.
 *
 * @author: Cassiano Campes
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define FILENAME ("my_file.txt") /* File to be opened */

FILE *fp;
pthread_t threads[2];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void * read_file(void* tmp)
{
    int i, ret;
    
    for (i = 0; i < 3000; i++) {
        if (pthread_self() == threads[1]) {
            pthread_mutex_lock(&mutex);
            ret = fputc('a', fp);
            pthread_mutex_unlock(&mutex);
        } else {
            pthread_mutex_lock(&mutex);
            ret = fputc('b', fp);
            pthread_mutex_unlock(&mutex);
        }
        
        if (ret < 0) {
            printf("Some error, closing\n");
            goto close;
        }
    }
    if(pthread_self() == threads[1]) {
        pthread_mutex_lock(&mutex);
        fputc('-', fp);
        pthread_mutex_unlock(&mutex);
    } else {
        pthread_mutex_lock(&mutex);
        fputc('+', fp);
        pthread_mutex_unlock(&mutex);
    }
close:
    return NULL;
}

int main()
{
    fp = fopen(FILENAME, "a+");
    if (!fp) {
        printf("Could not open the file\n");
        return -1;
    }
    pthread_create(&threads[1], NULL, read_file, NULL);
    pthread_create(&threads[2], NULL, read_file, NULL);

    pthread_join(threads[1], NULL);
    pthread_join(threads[2], NULL);

    fclose(fp);
    return 0;
}

