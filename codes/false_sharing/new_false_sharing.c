#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <sched.h>

typedef struct shared_struct {
    uint32_t coreid;
    uint32_t counter;
    /*8-bytes long */
} shared_t;
/* Because we are running at 8-cores, so we
 * gonna have in a single cache line all the
 * structures for the 8 threads. */

void* thread_cb(void *args)
{
    shared_t *tmp = (shared_t*) args;
    cpu_set_t cpuset;

    int repeat = 100000;

    CPU_ZERO(&cpuset);
    CPU_SET(tmp->coreid, &cpuset);
    sched_setaffinity(0, sizeof(cpuset), &cpuset);

    printf("Thread %u in %d\n", tmp->coreid, sched_getcpu());
    while(repeat--) {
        tmp->counter += 1;
        asm volatile("": : :"memory");
    }
    return NULL;
}

/* My processor is 64-bytes cache line long,
 * so that is why I can use the PADDING of 8 here,
 * because the shared_t is 8-bytes long. */
#define PADDING 8
int main(int argc, char **argv)
{
    shared_t shared_struct[sysconf(_SC_NPROCESSORS_ONLN) * PADDING];
    pthread_t threads[sysconf(_SC_NPROCESSORS_ONLN)];
    int i;

    printf("Sizeof = %lu\n", sizeof(shared_struct));
    printf("Running on %lu\n", sysconf(_SC_NPROCESSORS_ONLN));

    for (i = 0; i < sysconf(_SC_NPROCESSORS_ONLN); i++) {
        shared_struct[i * PADDING].coreid = shared_struct[i].counter = i;
        pthread_create(&threads[i], NULL, thread_cb, &shared_struct[i * PADDING]);
    }

    for (i = 0; i < sysconf(_SC_NPROCESSORS_ONLN); i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
