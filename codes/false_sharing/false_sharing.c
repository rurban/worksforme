#define _GNU_SOURCE
#include <stdio.h>
#include <sched.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM_THREADS (4)
#define CACHE_LINE_SIZE (64)

struct false_sharing_t {
  uint8_t padding[8];
};

struct without_sharing_t {
  uint8_t padding[8*CACHE_LINE_SIZE];
};

struct false_sharing_t global_false_sharing;
struct without_sharing_t global_without_sharing;

void *false_sharing_cb(void *vargs)
{
  cpu_set_t cpuset;
  int *val = (int*) vargs;
  int counter = 1000;

  CPU_ZERO(&cpuset);
  CPU_SET(*val, &cpuset);

  sched_setaffinity(0, sizeof(cpuset), &cpuset);

  /*printf("CPU %d\n", sched_getcpu());*/
  while (counter--)
    global_false_sharing.padding[*val] += 1;
}

void *without_sharing_cb(void *vargs)
{
  cpu_set_t cpuset;
  int *val = (int*) vargs;
  int counter = 100000;

  CPU_ZERO(&cpuset);
  CPU_SET(*val, &cpuset);

  sched_setaffinity(0, sizeof(cpuset), &cpuset);

  /*printf("CPU %d\n", sched_getcpu());*/
  while (counter--)
    global_without_sharing.padding[*val * CACHE_LINE_SIZE] += 1;
}

uint64_t rdtscp(void)
{
    uint32_t a, d, c;
    __asm__ volatile("rdtscp" : "=a" (a),"=d" (d), "=c" (c));
    return (((uint64_t) a) | (((uint64_t) d) << 32));
}

#define NUM_REPEAT (10000)

int main(int argc, char **argv)
{
  int i, repeat = NUM_REPEAT;
  uint64_t sum = 0;

  uint64_t begin, fini;
  pthread_t threads[NUM_THREADS];
  int tid[NUM_THREADS] = { 0 };

  if (argc != 2) {
    printf("Error, no parameters specified\n");
    return -1;
  }

  while (repeat--) {
    begin = rdtscp();
    for (i = 0; i < NUM_THREADS; i++) {
      tid[i] = i;
      if (atoi(argv[1]) == 1)
        pthread_create(&threads[i], NULL, false_sharing_cb, (void*) &tid[i]);
      else if (atoi(argv[1]) == 2)
        pthread_create(&threads[i], NULL, false_sharing_cb, (void*) &tid[i]);
      else
        printf("Error, no parameters specified\n");
    }
    for (i = 0; i < NUM_THREADS; i++)
        pthread_join(threads[i], NULL);
    fini = rdtscp();

    sum += fini - begin;
  }
  printf("Elapsed avg time = %lu\n", sum / NUM_REPEAT);
  return 0;
}