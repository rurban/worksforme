#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define BUFFER_SIZE 20

int main(void)
{
    char buffer[BUFFER_SIZE] = {0};

    fgets(buffer, BUFFER_SIZE, stdin);
    buffer[strcspn(buffer, "\r\n")] = '\0';

    if (tcflush(STDIN_FILENO, TCIFLUSH) == -1)
        return EXIT_FAILURE;

    printf("Read:  %s\n", buffer);

    return EXIT_SUCCESS;
}
