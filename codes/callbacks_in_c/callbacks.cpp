/*
 *  This is how to implement callbacks that calls
 *  C code in a CPP source code.
 */

#include <functional>
#include <iostream>
 
typedef void (*callback_t) (void);
 
class Communication
{
  public:
    Communication();
    ~Communication();
    static void serverFound();
    static void registerCallback(callback_t cb_ptr);
    static callback_t callback;
};
 
Communication::Communication()
{
}
 
Communication::~Communication()
{
}
 
void Communication::serverFound()
{
  callback();
}
callback_t Communication::callback;
void Communication::registerCallback(callback_t cb_ptr)
{
  callback = cb_ptr;
}
class Client
{
  public:
    Client();
    ~Client();
    static void serverFound1();
    void startClient();
  private:
    Communication *comm;
};
 
Client::Client()
{
}
Client::~Client()
{
}
 
void Client::serverFound1()
{
  printf("Client Server Found\n");
}
 
void Client::startClient()
{
  Communication communication;
  comm = &communication;
  comm->registerCallback(serverFound1);
  printf("start client\n");
 
  comm->serverFound();
}
 
int main()
{
  Client *client = new Client();
  client->startClient();
 
  delete client;
}SS
