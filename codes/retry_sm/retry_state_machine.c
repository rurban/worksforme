#include <stdio.h>
#include <stdbool.h>

int main(int argc, char **argv)
{
	enum {
		GET_PAGE,
		NO_FREE_PAGES,
		FORCE_GC,
		CHECK_GC,
		STATE_ERROR,
		FINISH,
	};
	int state = GET_PAGE;
	bool gc_tried = false, gc_forced = false;

	while (1) {
		switch (state) {
			case GET_PAGE:
				state = CHECK_GC;
				printf("get_page\n");
				if (argc == 2)
					state = NO_FREE_PAGES;
				if (gc_forced && gc_tried)
					state = STATE_ERROR;
				break;
			case NO_FREE_PAGES:
				printf("no_free_pages\n");
				gc_forced = true;
			case CHECK_GC:
				state = FINISH;
				printf("check_gc\n");
				if (gc_tried == true) {
					printf("######\n");
					state = STATE_ERROR;
				}
				if (gc_forced == true) {
					state = GET_PAGE;
					gc_tried = true;
				}
				argc = 1;
				break;
			case STATE_ERROR:
				printf("error\n");
				return 0;
			case FINISH:
				printf("finish\n");
				return 0;
		}
	}

}
