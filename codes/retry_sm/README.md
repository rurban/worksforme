# Retry state machine

This code is to have a state machine where some states can retry some operations
before returning error.

In this case, `gc_forced` is a flag to indicate a forced operation.
The ´gc_tried` indicates that the ´gc_forced` was forced, and tried.
So, if none of them succeed, we should finish.
