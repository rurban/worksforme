# Testing for loop

Check if the variable in the for is static

Why this?

Because I was talking to a friend and we could not answer straight
if the comparison in the foor loop could change, that is, if the control
variables could change within the loop.

Turns out it can, so, the values in the comparison are not static by the
compiler.
