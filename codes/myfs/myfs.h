#ifndef MYFS_H_
#define MYFS_H_

static int
myfs_create(struct inode *dir, struct dentry *dentry, umode_t mode, bool excl);

struct inode *myfs_get_inode(struct super_block *sb,
        const struct inode *dir, umode_t mode, dev_t dev);

static int
myfs_mknod(struct inode *dir, struct dentry *dentry, umode_t mode, dev_t dev);

static int
myfs_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode);

static int
myfs_symlink(struct inode *dir, struct dentry *dentry, const char *symname);

static int myfs_fill_super(struct super_block *sb, void *data, int silent);

static struct dentry *myfs_mount(struct file_system_type *fs_type,
        int flags, const char *dev_name, void *data);

#endif
