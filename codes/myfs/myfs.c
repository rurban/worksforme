#include <linux/module.h>

#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/highmem.h>
#include <linux/time.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/backing-dev.h>
//#include <linux/sched.h>
#include <linux/parser.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/mm.h>
#include <linux/splice.h>
#include <linux/buffer_head.h>

#include "myfs.h"

#ifndef FS_NAME
#define FS_NAME ("myfs")
#endif

#ifndef AUTHOR_NAME
#define AUTHOR_NAME ("Cassiano Campes")
#endif

#ifndef FS_MAGIC_NUMBER
#define FS_MAGIC_NUMBER (0xcaca)
#endif

static const struct super_operations myfs_ops;
static const struct inode_operations myfs_dir_inode_operations;
static const struct file_operations myfs_file_operations;
static const struct address_space_operations myfs_aops;
static const struct inode_operations myfs_file_inode_operations;
static struct file_system_type myfs_fs_type;

//---------------------------------------------------------

static struct file_system_type myfs_fs_type = {
    .name = "myfs",
    .mount = myfs_mount,
    .kill_sb = kill_block_super,
    .fs_flags = FS_USERNS_MOUNT,
};

static const struct super_operations myfs_ops = {
    .statfs = simple_statfs,
    .drop_inode = generic_delete_inode,
    .show_options = generic_show_options,
};

static const struct inode_operations myfs_dir_inode_operations = {
    .create = myfs_create,
    .lookup = simple_lookup,
    .link = simple_link,
    .unlink = simple_unlink,
    .symlink = myfs_symlink,
    .mkdir = myfs_mkdir,
    .rmdir = simple_rmdir,
    .mknod = myfs_mknod,
    .rename = simple_rename,
};

static const struct file_operations myfs_file_operations = {
    .read_iter      = generic_file_read_iter,
    .write_iter     = generic_file_write_iter,
    .mmap           = generic_file_mmap,
    .fsync          = noop_fsync,
    .splice_read    = generic_file_splice_read,
    .splice_write   = iter_file_splice_write,
    .llseek         = generic_file_llseek,
};

static const struct address_space_operations myfs_aops = {
    .readpage = simple_readpage,
    .write_begin = simple_write_begin,
    .write_end = simple_write_end,
    //.set_page_dirty = __set_page_dirty_no_writeback,
};



static const struct inode_operations myfs_file_inode_operations = {
    .setattr = simple_setattr,
    .getattr = simple_getattr,
};

//---------------------------------------------------------

struct inode *myfs_get_inode(struct super_block *sb,
        const struct inode *dir, umode_t mode, dev_t dev)
{
    struct inode *inode = new_inode(sb);

    if (inode) {
        inode->i_ino = get_next_ino();
        inode_init_owner(inode, dir, mode);
        inode->i_mapping->a_ops = &myfs_aops;
        mapping_set_gfp_mask(inode->i_mapping, GFP_HIGHUSER);
        mapping_set_unevictable(inode->i_mapping);
        inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;

        switch(mode & S_IFMT) {
            default:
                init_special_inode(inode, mode, dev);
                break;
            case S_IFREG:
                inode->i_op = &myfs_file_inode_operations;
                inode->i_fop = &myfs_file_operations;
                break;
            case S_IFDIR:
                inode->i_op = &myfs_dir_inode_operations;
                inode->i_fop = &simple_dir_operations;

                inc_nlink(inode);
                break;
            case S_IFLNK:
                inode->i_op= &page_symlink_inode_operations;
                break;
        }
    }
    return inode;
}

static int
myfs_mknod(struct inode *dir, struct dentry *dentry, umode_t mode, dev_t dev)
{
    struct inode *inode = myfs_get_inode(dir->i_sb, dir, mode, dev);
    int error = -ENOSPC;

    if(inode) {
        /* Fill in inode information for a dentry */
        d_instantiate(dentry, inode);
        dget(dentry);
        error = 0;
        dir->i_mtime = dir->i_ctime = CURRENT_TIME;
    }
    return error;
}

static int
myfs_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode)
{
    int retval = myfs_mknod(dir, dentry, mode | S_IFDIR, 0);
    if(!retval)
        inc_nlink(dir);

    return retval;
}

static int
myfs_create(struct inode *dir, struct dentry *dentry, umode_t mode, bool excl)
{
    return myfs_mknod(dir, dentry, mode | S_IFREG, 0);
}

static int
myfs_symlink(struct inode *dir, struct dentry *dentry, const char *symname)
{
    struct inode *inode;
    int error = -ENOSPC;

    inode = myfs_get_inode(dir->i_sb, dir, S_IFLNK | S_IRWXUGO, 0);

    if(inode) {
        int l = strlen(symname)+1;
        error = page_symlink(inode, symname, l);
        if(!error) {
            d_instantiate(dentry, inode);
            dget(dentry);
            dir->i_mtime = dir->i_ctime = CURRENT_TIME;
        } else
            iput(inode);
    }

    return error;
}

static int myfs_fill_super(struct super_block *sb, void *data, int silent)
{
    sb->s_maxbytes = MAX_LFS_FILESIZE;
    sb->s_blocksize = PAGE_CACHE_SIZE;
    sb->s_blocksize_bits = PAGE_CACHE_SHIFT;
    sb->s_magic = FS_MAGIC_NUMBER;
    sb->s_op = &myfs_ops;
    sb->s_time_gran = 1;

    return 0;
}

static struct dentry *myfs_mount(struct file_system_type *fs_type,
        int flags, const char *dev_name, void *data)
{
    return mount_bdev(fs_type, flags, dev_name,
            data, myfs_fill_super);
}

static int __init init_myfs(void)
{
    static unsigned long once;
    int ret = 0;

    /* Atomic version */
    if(test_and_set_bit(0, &once)) {
        printk(KERN_INFO "%s is already loaded\n", FS_NAME);
        return 0;
    }
    ret = register_filesystem(&myfs_fs_type);

    if (ret)
        printk(KERN_INFO "%s got some problem while registering\n", FS_NAME);
    else {
        printk(KERN_INFO "%s loaded\n", FS_NAME);
        pr_debug("%s !!! loaded\n", FS_NAME);

    }

    return ret;
}

static void __exit fini_myfs(void)
{
    unregister_filesystem(&myfs_fs_type);
    printk(KERN_INFO "%s unloaded\n", FS_NAME);
}

module_init(init_myfs);
module_exit(fini_myfs);

MODULE_LICENSE("GPL");
