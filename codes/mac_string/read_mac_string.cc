#include <iostream>
#include <string>
#include <fstream>
#include <boost/format.hpp>


int main()
{

    std::string mac_file_path = "inband-mac-address";

    boost::format fmt("%02X:%02X:%02X:%02X:%02X:%02X\n");

    std::ifstream mac_file;
    std::string sys_mac_addr_;

    mac_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        mac_file.open(mac_file_path.c_str(), std::ifstream::out);
        for (int i = 0; !mac_file.eof(); i++) {
            fmt % static_cast<unsigned int>(mac_file.get());
            std::cout << "i " << i  << " format " <<  fmt << std::endl;
        }
        std::cout << fmt << std::endl;

        mac_file.close();
    }
    catch (std::ifstream::failure e) {
        std::cout << "Exception opening/reading/closing file\n";
    }
    printf("sys_mac = %s\n", sys_mac_addr_.c_str());
}
