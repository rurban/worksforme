#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#define HAVE_ARCH_STRUCT_FLOCK
#include <linux/fcntl.h>

// If needed
#define RWH_WRITE_LIFE_MEDIUM (3)
#define F_SET_RW_HINT (F_LINUX_SPECIFIC_BASE + 12)

static char const filename[] = "./file.txt";
static char const message[] = "/mnt/pool/my_file\n";

int main(void)
{
    int fp;
    int cnt = 0;
    uint64_t type = RWH_WRITE_LIFE_MEDIUM;

    fp = open(filename, O_WRONLY | O_CREAT);
    if (fp == -1)
        return 1;

    fcntl(fp, F_SET_RW_HINT, &type);
    cnt = write(fp, message, sizeof(message));

    if(cnt == -1)
        return 1;

    close(fp);
    return 0;
}
