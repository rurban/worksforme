#include <iostream>
#include <unistd.h>
#include <chrono>
#include <thread>

using namespace std;

int main() {
    cout << "sleeping 1 second" << endl;
    auto wait_for_one_second = []() { std::this_thread::sleep_for(std::chrono::seconds(1)); };
    wait_for_one_second();
    cout << "Awake" << endl;

    return 0;
}
