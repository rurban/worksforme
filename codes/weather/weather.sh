#!/bin/sh

#####################################################################################
#
# Mainteainer:
#       Cassiano Campes
#       cassianocampes@gmail.com
#
# Description:
#       Weather script used to get information about static
#       cities that is of my interest and the currency from USD to BRL.
#
#####################################################################################

# URLs used to get information about the weather
URL_PORTO='http://www.accuweather.com/en/br/porto-alegre/45561/weather-forecast/45561'
URL_NOIAH='http://www.accuweather.com/en/br/novo-hamburgo/35731/weather-forecast/35731'
URL_ELDOR='http://www.accuweather.com/en/br/eldorado-do-sul/2309298/weather-forecast/2309298'
URL_TAQUA='http://www.accuweather.com/en/br/taquari/40928/weather-forecast/40928'

# Parse the HTML page to the interested fields
echo "----------------------------------------------------------------------"
wget -q -O- "$URL_PORTO" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print ""$14" in Porto alegre. Temperature is "$10"°C and feels is "$12"°C" }'| head -1
echo "----------------------------------------------------------------------"
wget -q -O- "$URL_NOIAH" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print ""$14" in Novo Hamburgo. Temperature is "$10"°C and feels is "$12"°C" }'| head -1
echo "----------------------------------------------------------------------"
wget -q -O- "$URL_ELDOR" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print ""$14" in Eldorado do Sul. Temperature is "$10"°C and feels is "$12"°C" }'| head -1
echo "----------------------------------------------------------------------"
wget -q -O- "$URL_TAQUA" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print ""$14" in Taquari. Temperature is "$10"°C and feels is "$12"°C" }'| head -1
echo "----------------------------------------------------------------------"
echo "Dollar to Real:"
 wget -q -O- http://br.investing.com/currencies/usd-brl | grep "last_last" | sed 's/\s<*.*["]>//g' | sed 's/<\/span>//g'
