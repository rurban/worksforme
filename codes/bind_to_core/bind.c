#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define FILENAME ("/mnt/disk/myfile.txt")

pthread_t threads[2];
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct param {
    int cpu;
} param_t;

void *write_to_file(void *p)
{
    FILE *fp;
    cpu_set_t cpuset;
    int i, ret, cpu = ((param_t*)p)->cpu;

    CPU_ZERO(&cpuset);
    CPU_SET(cpu, &cpuset);

    sched_setaffinity(0, sizeof(cpuset), &cpuset);

    printf("CPU %d\n", sched_getcpu());

    if (pthread_self() == threads[sched_getcpu()]) {
        pthread_mutex_lock(&mutex);
        fp = fopen(FILENAME, "a+");
        if(!fp) {
            printf("Could not open file\n");
        } else {
            if (cpu)
                ret = fputc('a', fp);
            else
                ret = fputc('b', fp);
            ret = fputc('\n', fp);

            fsync(fileno(fp));
            fclose(fp);
        }
        pthread_mutex_unlock(&mutex);
    }
}

int main(int argc, char *argv[]) {

    param_t params0 = {.cpu = atoi(argv[1]) };
    param_t params1 = {.cpu = atoi(argv[2]) };
    pthread_create(&threads[0], NULL, write_to_file, &params0);
    pthread_create(&threads[1], NULL, write_to_file, &params1);

    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);

    return 0;
}
