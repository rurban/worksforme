#define _GNU_SOURCE

//XXX: REFERENCE https://stackoverflow.com/questions/10921210/cpu-tsc-fetch-operation-especially-in-multicore-multi-processor-environment

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

#include <sched.h>
#include <pthread.h>
#include <stdlib.h>
#include <sys/stat.h>

#define ERR_EXIT(msg) \
    do { \
        printf("%s\n", msg); \
        exit(-1); \
    } while (0)

typedef struct thread_args {
    int cpu_id;
    int thread_id;
} thread_args_t;

unsigned long tacc_rdtscp(int *chip, int *core)
{
    unsigned a, d, c;
    __asm__ volatile("rdtscp" : "=a" (a), "=d" (d), "=c" (c));

    *chip = (c & 0xFFF000)>>12;
    *core = c & 0xFFF;

    return ((unsigned long)a) | (((unsigned long)d) << 32);
}

void *thread_func(void *arg)
{
    cpu_set_t set;

    unsigned long counter = 0;
    int chip = 0, core = 0;
    thread_args_t *ta = arg;
    int cpu = ta->cpu_id;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);

    if(sched_setaffinity(0, sizeof(set), &set) == -1) {
        ERR_EXIT("sched_setaffinity");
    }

    counter = tacc_rdtscp(&chip, &core);
    printf("%d, %d,\t%lu\n", chip, core, counter);
}


int main() {
    int core = 0;
    int chip = 0;
    int i = 0;
    unsigned long counter[100];
    unsigned long diff[100];
    pthread_t *threads;
    thread_args_t *thread_args;

    long num_cores = sysconf(_SC_NPROCESSORS_ONLN);

    threads = (pthread_t*) calloc(num_cores*8, sizeof(pthread_t));
    thread_args = (thread_args_t*) calloc(num_cores*8, sizeof(thread_args_t));

    for (i = 0; i < num_cores; i++) {
        thread_args[i].cpu_id = i;
        thread_args[i].thread_id = i;
        pthread_create(&threads[i], NULL, thread_func, &thread_args[i]);
    }

    for(i = 0; i < num_cores; i++) {
        pthread_join(threads[i], NULL);
    }

    free(threads);
    free(thread_args);
    return 0;
}
