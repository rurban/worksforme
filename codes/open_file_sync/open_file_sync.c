#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

static char const filename[] = "/home/cassiano/Develop/BlackOps/codes/my_file";
static char const message[] = "/mnt/pool/my_file\n";

int main()
{
    int fp;
    FILE *fp1;
    int cnt = 0;
    errno = 0;

    //fp = open(filename, O_SYNC | O_APPEND | O_NONBLOCK);
    fp1 = fopen(filename, "a+");
    if (fp1 == NULL) { // < 0) {
        printf("could not open file\n");
        printf("file: %s\n", filename);
        printf("[errno]: %s\n", strerror(errno));
        return -1;
    }
    fwrite(message, 1, strlen(message), fp1);

    fsync(fileno(fp1));

    return 0;

    printf("fp = %d\n", fp);

    errno = 0;
    cnt = write(fp, message, sizeof(message));

    if(cnt < 0) {
        printf("Some error ocurred while writting to file\n");
        printf("errno: %s\n", strerror(errno));
        return -1;
    }

    close(fp);
    return 0;
}
