#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <malloc.h>
#include <pthread.h>

#include <sched.h>

#include <stdlib.h>

#define ERR_EXIT(msg) \
    do { \
        printf("%s\n", msg); \
        exit(-1); \
    } while (0);

typedef struct align_cnt {
    uint64_t v;

#ifdef ALIGNED
    uint64_t padding[7];

} align_cnt_t __attribute__ ((aligned (64)));
#else
} align_cnt_t;
#endif

typedef struct thread_arg {
#ifdef ALIGNED
    uint64_t cpuid;
    uint64_t chip;
    uint64_t padding0[6];

    uint64_t core;
    uint64_t padding1[7];

    uint64_t *ptr1;
    uint64_t *ptr2;
    uint64_t padding2[6];
} thread_arg_t __attribute__ ((aligned (64)));
#else
    int cpuid;
    uint64_t chip, core;
    uint64_t *ptr1;
    uint64_t *ptr2;
} thread_arg_t;
#endif

uint64_t rdtscp(uint64_t *chip, uint64_t *core)
{
    uint32_t a, d, c;

    __asm__ volatile("rdtscp" : "=a" (a),"=d" (d), "=c" (c));
    *chip = (c & 0xFFF000) >> 12;
    *core = c & 0xFFF;

    return ((uint64_t) a) | (((uint64_t) d) << 32);
}

void *f_thread(void *arg)
{
    thread_arg_t *t = arg;
    cpu_set_t cpuset;

    CPU_ZERO(&cpuset);

    CPU_SET(t->cpuid, &cpuset);

    if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset)) {
        ERR_EXIT("sched_setaffinity");
    }

#if PERFORMANCE
    int cnt = 100000;

    while(cnt--) {
#endif

        __asm__ volatile("" : : : "memory");
        *t->ptr1 = rdtscp(&t->chip, &t->core);
        __asm__ volatile("" : : : "memory");
        *t->ptr2 = rdtscp(&t->chip, &t->core);
#if PERFORMANCE
    }
#endif
}

int main() {
    int i, j;
    align_cnt_t *align;
    pthread_t *threads;
    thread_arg_t *threads_arg;
    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);

#ifndef PERFORMANCE
    printf("Num cores = %d\n", num_cores);
#endif

#ifdef ALINED
    align = (align_cnt_t*) memalign(64, 2* num_cores * sizeof(align_cnt_t));
#else
    align = (align_cnt_t*) calloc(2 * num_cores, sizeof(align_cnt_t));
#endif

    threads = (pthread_t*) calloc(num_cores, sizeof(pthread_t));
    threads_arg = (thread_arg_t*) calloc(num_cores, sizeof(thread_arg_t));

    for (i = 0, j = 0; i < num_cores; i++, j+=2) {
        threads_arg[i].cpuid = i;
        threads_arg[i].ptr1 = &align[j].v;
        threads_arg[i].ptr2 = &align[j+1].v;
        pthread_create(&threads[i], NULL, f_thread, &threads_arg[i]);
    }


    for (i = 0; i < num_cores; i++) {
        pthread_join(threads[i], NULL);
    }

#ifndef PERFORMANCE
    for (i = 0; i < num_cores; i++) {
        uint64_t a = *threads_arg[i].ptr1;
        uint64_t b = *threads_arg[i].ptr2;
        printf("Socket [ %lu ]\tCore [ %lu ]\tTSC1 = [ %lu ]\tTSC2 = [ %lu ]\t Diff [ %lu ]\n", threads_arg[i].chip, threads_arg[i].core, a, b, (b - a));
    }
#endif

    return 0;
}
