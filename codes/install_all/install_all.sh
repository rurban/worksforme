#!/bin/bash

sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo apt-get -f install
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo apt-get -y install git
sudo apt-get -y install terminator
sudo apt-get -y install dh-autoreconf
sudo apt-get -y install htop
sudo apt-get -y install tree
sudo apt-get -y install vim-gnome
sudo apt-get -y install flex
sudo apt-get -y install bison
sudo apt-get -y install libaio-dev
sudo apt-get -y install shutter
sudo apt-get -y install libssl-dev

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim


cd ~/
mkdir Develop
cd Develop
git clone https://github.com/redswaqz/bashrc.git
git clone https://github.com/redswaqz/vimrc.git
git clone https://github.com/redswaqz/WorksForMe.git

sudo apt-get -y install meld

#{ Install linux kernel ------------------------------->
cd ~/Develop/
wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.9.43.tar.xz
tar -vxf linux-4.9.43.tar.xz
cd linux-4.9.43
sudo apt-get install -y xmlto
sudo apt-get install -y sphinx-doc
sudo apt-get install -y libncurses5-dev libncursesw5-dev
make mandocs
make installmandocs

# <-----------------end install linux kernel }

# install the jnlp java runtime
sudo apt-get -y install icedtea-netx

