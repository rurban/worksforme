#include <linux/types.h>

#define COMBINE_TSC_AND_COREID(tsc, core) ((uint64_t)(((tsc) << 8) | ((core) & 0xFF )))
#define GET_COREID_FROM_COMBTSC(cmbtsc) ((uint64_t) ((cmbtsc) & 0xFF))
#define GET_TSC_ONLY(tsc) ((uint64_t)(tsc) >> 8)
#define GET_COREID_ONLY(coreid) ((uint64_t)(coreid) & 0xFF)

typedef struct mj_hash_table {
    uint64_t hash;
    uint64_t wlock;
    void *entry;
} mj_hash_table_t;

typedef struct mj_entry {
    uint64_t key;
    uint64_t value;
    void *next;
} mj_entry_t;

extern void mj_insert_hash(mj_hash_table_t *table, const uint64_t key, const uint64_t value);

extern void mj_delete_hash(mj_hash_table_t *table, uint64_t key);

extern mj_entry_t* mj_search_hash(mj_hash_table_t *table, const uint64_t key);

extern mj_hash_table_t* mj_create_hash_table(int size);

extern uint64_t mj_rdtscp(uint64_t *chip, uint64_t *core);
