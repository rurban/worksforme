#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>

#include <stdbool.h>
#include <linux/types.h>

#include "mj_hash_table.h"

MODULE_LICENSE("GPL");

/* Cassiano::
 * This tell us how many entries our hash support
 */
static int g_size = 50;

static mj_hash_table_t *g_table = NULL;

uint64_t mj_rdtscp(uint64_t *chip, uint64_t *core)
{
    uint32_t a, d, c;

    __asm__ volatile("rdtscp" : "=a" (a),"=d" (d), "=c" (c));
    *chip = (c & 0xFFF000) >> 12;
    *core = (c & 0xFFF);

    return (((uint64_t) a) | (((uint64_t) d) << 32));
}
EXPORT_SYMBOL(mj_rdtscp);

static void _mj_clear_hash_table(mj_hash_table_t *table, int size)
{
    int i;
    mj_entry_t *curr, *next;
    mj_hash_table_t *it = table;

    printk(KERN_DEBUG "mj_clear_hash_table\n");

    if(table == NULL)
        return;

    for(i = 0; i < size; i++) {
        curr = it->entry;
        while(curr != NULL) {
            next = curr->next;
            kfree(curr);
            curr = next;
        }
    }
}

static uint64_t _mj_hash_code(uint64_t key)
{
    //XXX: This is a very basic hash function
    uint64_t hash = (key % g_size);

    printk(KERN_DEBUG "mj_hash_code\n");

    return hash;
}

// FIXME returning NULL when fails
static mj_entry_t *__mj_create_entry(uint64_t key, uint64_t value)
{

    //XXX: Shall we use GFP_NOWAIT?
    mj_entry_t *t = kmalloc(sizeof(mj_entry_t), GFP_NOWAIT);

    printk(KERN_DEBUG "mj_create_entry\n");

    if(!t)
        return NULL;
    t->key = key;
    t->value = value;
    t->next = NULL;

    return t;
}

static void _mj_acquire_lock(uint64_t *lock)
{
    printk(KERN_DEBUG "mj_acquire_lock\n");

    BUG_ON(lock);

    //XXX: This is a very basic locking primitive (CAS)
    do {
        continue;
    } while(!__sync_bool_compare_and_swap(lock, 0, 1));
}

static void _mj_release_lock(uint64_t *lock)
{
    printk(KERN_DEBUG "mj_release_lock\n");

    BUG_ON(lock);
    //XXX: This is a very basic locking primitive (CAS)
    do {
    } while (!__sync_bool_compare_and_swap(lock, 1, 0));
}

void mj_insert_hash(mj_hash_table_t *table, const uint64_t key, const uint64_t value)
{
    int hash = _mj_hash_code(key);
    mj_entry_t *new, *it;

    printk(KERN_DEBUG "mj_insert_hash\n");

    BUG_ON(table);

    new = __mj_create_entry(key, value);

    _mj_acquire_lock(&(table[hash].wlock));

    if(!table[hash].entry) {
        table[hash].entry = new;
        goto jmp_release_lock;
    }

    it = table[hash].entry;

    while(it != NULL) {
        if(it->key == key) {
            /*Cassiano:: simply compare TSCs of the values */
            if(likely(GET_TSC_ONLY(it->value) < GET_TSC_ONLY(value)))
                it->value = value;
            /*Cassiano:: Ok, somehow the TSCs previously are the same
             * then we need to see which core ID is the lower one
             * and update the new value to whom has the bigger core ID */
            else if(unlikely(GET_TSC_ONLY(it->value) == GET_TSC_ONLY(value))) {
                if(GET_COREID_ONLY(it->value) > GET_COREID_ONLY(value))
                    it->value = value;
                else if(unlikely(GET_COREID_ONLY(it->value) == GET_COREID_ONLY(value)))
                    /*Ok, we just reached one impossible point in the source code.
                     * Let's kill ourselves and explode everything! =) */
                    BUG_ON(0);
            }
            kfree(new);
            goto jmp_release_lock;
        }
        it = it->next;
    }
    new->next = table[hash].entry;
    table[hash].entry = new;
jmp_release_lock:
    _mj_release_lock(&(table[hash].wlock));
}
EXPORT_SYMBOL(mj_insert_hash);

void mj_delete_entry(mj_hash_table_t *table, uint64_t key)
{
    mj_entry_t *entry, *next;
    uint64_t hash = _mj_hash_code(key);
    mj_hash_table_t *bucket;

    printk(KERN_DEBUG "mj_delete_entry\n");
    BUG_ON(table);

    bucket = &table[hash];

    _mj_acquire_lock(&bucket->wlock);

    entry = bucket->entry;

    if(entry == NULL)
        return;

    if(entry->key == key) {
        bucket->entry = entry->next;
        kfree(entry);
        return;
    }
    next = entry->next;

    while (next != NULL) {
        if(next->key == key) {
            entry->next = next->next;
            kfree(next);
            goto release_lock;
        }
        entry = next;
        next = next->next;
    }
release_lock:
    _mj_release_lock(&bucket->wlock);
}
EXPORT_SYMBOL(mj_delete_entry);

mj_entry_t *mj_search_hash(mj_hash_table_t *table, const uint64_t key)
{
    mj_entry_t *entry;
    uint64_t hash = _mj_hash_code(key);
    mj_hash_table_t *bucket;

    printk(KERN_DEBUG "mj_search_hash\n");

    BUG_ON(table);

    bucket = &table[hash];

    entry = bucket->entry;

    while(entry != NULL) {
        if(entry->key == key)
            return entry;
        entry = entry->next;
    }
    return NULL;
}
EXPORT_SYMBOL(mj_search_hash);

mj_hash_table_t* mj_create_hash_table(int size) {

    printk(KERN_DEBUG "mj_create_hash_table\n");

    //XXX: Shall we use GFP_NOFS?
    return kmalloc(size * sizeof(mj_hash_table_t), GFP_NOFS);
}
EXPORT_SYMBOL(mj_create_hash_table);

static int __init mj_hash_init(void)
{
    printk(KERN_INFO "Loading mj_hash_table module\n");
    g_table = mj_create_hash_table(g_size);
    return 0;
}

static void __exit mj_hash_fini(void)
{
    printk(KERN_INFO "Cleaning mj_hash_table module\n");
    _mj_clear_hash_table(g_table, g_size);
    kfree(g_table);
}

module_init(mj_hash_init);
module_exit(mj_hash_fini);
