#include<stdint.h>

typedef struct mj_hash_table {
    uint64_t hash;
    uint64_t wlock;
    void *entry;
} mj_hash_table_t;

typedef struct mj_entry {
    uint64_t key;
    uint64_t value;
    void *next;
} mj_entry_t;

void ext4mj_clear_hash_table(mj_hash_table_t *table, int size);

uint64_t ext4mj_hash_code(uint64_t key);

bool ext4mj_acquire_lock(uint64_t *lock);

bool ext4mj_release_lock(uint64_t *lock);

void ext4mj_insert_hash(mj_hash_table_t *table, const uint64_t key, const uint64_t value);

void ext4mj_delete_hash(mj_hash_table_t *table, uint64_t key);

mj_entry_t* ext4mj_search_hash(mj_hash_table_t *table, const uint64_t key);

void ext4mj_display_hash(mj_hash_table_t *table, int size);
