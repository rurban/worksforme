# Hash table

I needed to learn something about hash tables and I decided to create my own

## Features of this hash table
- Stores a <key,value> pair that are 64-bits value each
- Hash handles collisions
- Uses simple locking primitive for writes

## TODO
- Improve the locking mechanism for read/write operations
