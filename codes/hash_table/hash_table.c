#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include "hash_table.h"

int g_size = 5;

void clear_hash_table(mj_hash_table_t *table, int size) {
    int i, j;
    mj_entry_t *nxt, *curr, *next;
    mj_hash_table_t *it = table;

    for(i = 0; i < size; i++) {
        curr = it->entry;
        while(curr != NULL) {
            next = curr->next;
            free(curr);
            curr = next;
        }
    }
}

uint64_t hash_code(uint64_t key) {
    return key % g_size;
}
// FIXME returning NULL when fails
static mj_entry_t *__create_entry(uint64_t key, uint64_t value) {
    mj_entry_t *t = calloc(1, sizeof(mj_entry_t));
    if(!t)
        return NULL;
    t->key = key;
    t->value = value;
    t->next = NULL;

    return t;
}

bool acquire_lock(uint64_t *lock) {
    do {
        continue;
    } while(!__sync_bool_compare_and_swap(lock, 0, 1));
}

bool release_lock(uint64_t *lock) {
    do {
    } while (!__sync_bool_compare_and_swap(lock, 1, 0));
}

void insert_hash(mj_hash_table_t *table, const uint64_t key, const uint64_t value) {
    int hash = hash_code(key);
    mj_entry_t *new, *it;

    new = __create_entry(key, value);

    acquire_lock(&(table[hash].wlock));

    if(!table[hash].entry) {
        table[hash].entry = new;
        goto jmp_release_lock;
    }

    it = table[hash].entry;

    while(it != NULL) {
        if(it->key == key)
            goto jmp_release_lock;
        it = it->next;
    }
    new->next = table[hash].entry;
    table[hash].entry = new;
jmp_release_lock:
    release_lock(&(table[hash].wlock));
}

void delete_hash(mj_hash_table_t *table, uint64_t key) {
    mj_entry_t *entry, *next;
    uint64_t hash = hash_code(key);
    mj_hash_table_t *bucket = &table[hash];

    acquire_lock(&bucket->wlock);

    entry = bucket->entry;

    if(entry->key == key) {
        bucket->entry = entry->next;
        free(entry);
        entry = NULL;
        goto release_lock;
    }
    next = entry->next;

    while (next != NULL) {
        if(next->key == key) {
            entry->next = next->next;
            free(next);
            next = NULL;
            goto release_lock;
        }
        entry = next;
        next = next->next;
    }
release_lock:
    release_lock(&bucket->wlock);
}

mj_entry_t *search_hash(mj_hash_table_t *table, const uint64_t key) {
    mj_entry_t *entry;
    uint64_t hash = hash_code(key);
    mj_hash_table_t *bucket = &table[hash];

    entry = bucket->entry;

    while(entry != NULL) {
        if(entry->key == key)
            return entry;
        entry = entry->next;
    }
    return NULL;
}

void display_hash(mj_hash_table_t *table, int size) {
    mj_entry_t *entry;
    int i;
    for(i = 0; i < size; i++) {
        printf("bucket[%d] = ", i);
        entry = table[i].entry;
        while(entry != NULL) {
            printf("(%lu, %lu), ", entry->key, entry->value);
            entry = entry->next;
        }
        printf("\n");
    }
}

mj_hash_table_t* create_hash_table(mj_hash_table_t *table, int size) {
    return calloc(size, sizeof(mj_hash_table_t));
}

int main() {
    int i;
    mj_hash_table_t *table;
    mj_entry_t *entry;
    table = create_hash_table(table, g_size);

    insert_hash(table, 10, 100);
    insert_hash(table, 20, 1200);
    insert_hash(table, 30, 1300);
    insert_hash(table, 40, 1400);
    display_hash(table, g_size);
    delete_hash(table, 20);
    display_hash(table, g_size);
    entry = search_hash(table, 40);

    if(entry == NULL) {
        printf("NOT FOUND\n");
    } else {
        printf("Value %lu\n", entry->value);
    }

}
