#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <malloc.h>
#include <pthread.h>

#include <sched.h>

#include <stdlib.h>

#define ERR_EXIT(msg) \
    do { \
        printf("%s\n", msg); \
        exit(-1); \
    } while (0);

typedef struct align_cnt {
    uint64_t v;

    uint64_t padding[7];

} align_cnt_t __attribute__ ((aligned (64)));

typedef struct thread_arg {
    uint64_t cpuid;
    uint64_t chip;
    uint64_t padding0[6];

    uint64_t core;
    uint64_t padding1[7];

    uint64_t *ptr1;
    uint64_t *ptr2;
    uint64_t padding2[6];
} thread_arg_t __attribute__ ((aligned (64)));

uint64_t rdtscp(uint64_t *chip, uint64_t *core)
{
    uint32_t a, d, c;

    __asm__ volatile("rdtscp" : "=a" (a),"=d" (d), "=c" (c));
    *chip = (c & 0xFFF000) >> 12;
    *core = c & 0xFFF;

    return ((uint64_t) a) | (((uint64_t) d) << 32);
}

void *f_thread_server(void *arg)
{
    thread_arg_t *t = arg;
    cpu_set_t cpuset;

    CPU_ZERO(&cpuset);

    CPU_SET(t->cpuid, &cpuset);

    if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset)) {
        ERR_EXIT("sched_setaffinity");
    }

    while(*t->ptr2) {
        //__asm__ volatile("" : : : "memory");
        *t->ptr1 = rdtscp(&t->chip, &t->core);
    }
}

void *f_thread_client(void *arg)
{
    thread_arg_t *t = arg;
    cpu_set_t cpuset;

    CPU_ZERO(&cpuset);

    uint64_t read = 0;
    uint64_t other = 0;

    CPU_SET(t->cpuid, &cpuset);

    if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset)) {
        ERR_EXIT("sched_setaffinity");
    }

    while(1) {
        //__asm__ volatile("" : : : "memory");
        other = *t->ptr1;
        //__asm__ volatile("" : : : "memory");
        read = rdtscp(&t->chip, &t->core);
        //__asm__ volatile("" : : : "memory");
        if(other > read) {
            printf("ASSERT\t*t->ptr1\t> \tread\n%lu\t%lu\n", other, read);
            ERR_EXIT("ASSERT");
        } else if (other == read) {
            printf("ASSERT\t*t->ptr1\t== \tread\n%lu\t%lu\n", other, read);
            ERR_EXIT("ASSERT");
        }
        printf("%lu\t======\t%lu\n", other, read);
    }
}

uint64_t sigint = 1;
void sig_handler(int signo) {
    sigint = 0;
    ERR_EXIT("sig_handler");
}

int main(int argc, char **argv) {
    int i, j;
    align_cnt_t *align;
    pthread_t *threads;
    thread_arg_t *threads_arg;
    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
    int master_core = atoi(argv[1]);
    int slave_core = atoi(argv[2]);

    if (master_core < 0 || master_core >= num_cores) {
        ERR_EXIT("master_core");
    }
    if (slave_core < 0 || slave_core >= num_cores) {
        ERR_EXIT("slave_core");
    }

    if(signal(SIGINT, sig_handler) == SIG_ERR) {
        ERR_EXIT("signal");
    }

    align = (align_cnt_t*) memalign(64, 2* num_cores * sizeof(align_cnt_t));

    threads = (pthread_t*) calloc(num_cores, sizeof(pthread_t));
    threads_arg = (thread_arg_t*) calloc(num_cores, sizeof(thread_arg_t));

    threads_arg[0].cpuid = master_core;
    threads_arg[0].ptr1 = &align[0].v;
    threads_arg[0].ptr2 = &sigint;
    pthread_create(&threads[0], NULL, f_thread_server, &threads_arg[0]);

    threads_arg[1].cpuid = slave_core;
    threads_arg[1].ptr1 = &align[0].v;
    threads_arg[1].ptr2 = &sigint;
    pthread_create(&threads[1], NULL, f_thread_client, &threads_arg[1]);


    for (i = 0; i < 2; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}
