#include <iostream>
#include <chrono>
#include <unistd.h>
#define TIMEOUT (15000)

static void call_process()
{
    system("./teste");
}

int main()
{
    if (fork() == 0) {
        printf("child process\n");
        call_process();
    }
    else {
        std::cout << "tempo " << TIMEOUT << std::endl;
        return 0;
    }
}
