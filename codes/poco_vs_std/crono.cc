#include <iostream>
#include <chrono>
#include "Poco/Process.h"
#include "Poco/File.h"
#include "Poco/Path.h"

using namespace Poco;

int main()
{
    std::string cmd = "./teste";
    std::string args = "l2";
    auto foo = Process::launch(cmd, {}, nullptr);
    std::cout << "tempo "
              << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::seconds(15))
                     .count() << std::endl;

    return 0;
}
