#/bin/bash

# Here is listed all the softwares I install in my computer when I format it.
# To make things faster, I automated this process.


cd ~/
mkdir -v Develop

gituser="Cassiano Campes"
gitemail="cassianocampes@gmail.com"

declare -a  arr=("vim"
"terminator"
"git"
"shutter"
"curl"
"qemu-system"
"pm-utils"
"cscope"
"exuberant-ctags"
"gitk"
"flex"
"bison"
"libelf-dev"
"meld"
"pidgin"
"libssl-dev"
"libncurses5-dev"
"texlive-latex-base"
"texlive-fonts-recommended"
"texlive-fonts-extra"
"libcurl3"
"doxygen"
"apt-transport-https"
"ca-certificates"
"software-properties-common"
"python3-pip"
"libusb-1.0-0-dev"
"compizconfig-settings-manager"
"ubuntu-unity-desktop"
"gparted"
"anydesk"
"autoconf"
"automake"
"autotools-dev"
"brave-browser"
"cmake"
"discord"
"remarkable"
"skypeforlinux"
"rstudio"
"slack-desktop"
"teams"
"teamviewer"
"zoom"
)

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y

for i in "${arr[@]}"
do
    echo "[$i]======================>"
    sudo apt-get install "$i" -y
    echo "---------------------------"
done

###############################################################################
# URL of the DEB packages downloaded to install stuff
###############################################################################
cd ~/Downloads
wget http://remarkableapp.github.io/files/remarkable_1.87_all.deb
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.3.3-amd64.deb

# List of DEB packages to be installed
declare -a debpkg=("remarkable_1.87_all.deb"
"slack-desktop-3.3.3-amd64.deb"
)
for i in "${debpkg[@]}"
do
    echo "[$i]======================>"
    sudo dpkg -i "$i"
    echo "---------------------------"
done
################################################################################
# Git configs
################################################################################
git config --global user.email "$gituser"
git config --global user.name "$gitemail"

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://github.com/phyloflash/vimrc.git ~/Develop
git clone https://github.com/phyloflash/bashrc.git ~/Develop


return

################################################################################
# Docker stuff
################################################################################
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
################################################################################
